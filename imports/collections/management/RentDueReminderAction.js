import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

export const RentDueReminderAction =  new SimpleSchema({
    monthRentDue: {
        type: String,
        label: 'Month and year rent is due',
        optional: true,
        custom: rentDueReminderValidation
    },
    rentDueAmount: {
        type: Number,
        label: 'Rent due amount',
        optional: true,
        custom: rentDueReminderValidation
    },
    deadlineToPay: {
        type: Date,
        label: 'Deadline for payment',
        optional: true,
        custom: rentDueReminderValidation,
        defaultValue: new Date(new moment().add(7, 'days').valueOf())
    },
    lateFee: {
        type: Number,
        label: 'Late rent fee',
        optional: true,
        custom: rentDueReminderValidation
    }
}, { tracker: Tracker });

function rentDueReminderValidation() {
    var doc = Processes.findOne({_id: this.docId}),
        shouldBeRequired;
    if (doc && doc.management.shouldSendFirstReminder) {
        shouldBeRequired = true;
    } else {
        shouldBeRequired = false;
    }
    if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
            if (!this.isSet || this.value === null || this.value === "") return "required";
        }

        // updates
        else if (this.isSet) {
            if (this.operator === "$set" && this.value === null || this.value === "") return "required";
            if (this.operator === "$unset") return "required";
            if (this.operator === "$rename") return "required";
        }
    }
}