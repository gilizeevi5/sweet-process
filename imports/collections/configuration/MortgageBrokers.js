import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

MortgageBrokers = new Mongo.Collection('mortgageBrokers');

MortgageBrokers.allow({
    insert: function(userId) {
        return !!userId;
    },
    update: function(userId) {
        return !!userId;
    },
    remove: function(userId) {
        return !!userId;
    }
});

MortgageBrokerSchema = new SimpleSchema({
    displayName: {
        type: 'String',
        label: 'Name'
    },
    email: {
        type: 'String',
        label: 'Email'
    },
    simpleName: {
        type: 'String',
        label: 'Contact first name'
    },
    folder: {
        type: 'String',
        label: 'Folder'
    }
},{ tracker: Tracker });

MortgageBrokers.attachSchema(MortgageBrokerSchema);