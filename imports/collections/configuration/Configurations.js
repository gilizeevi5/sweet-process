import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

Configurations = new Mongo.Collection('configurations');


Configurations.allow({
    update: function(userId) {
        return !!userId;
    },
    insert: function(userId) {
        return !!userId;
    }
});

ConfigurationSchema = new SimpleSchema({
    landTax: {
        type: Number,
        label: 'Land tax',
    },
    mortgageBrokerFee: {
        type: Number,
        label: 'Mortgage broker fee',
    },
    notaryFee: {
        type: Number,
        label: 'Notary fee',
    },
    registrationFee: {
        type: Number,
        label: 'Registration fee',
    },
    escrowFee: {
        type: Number,
        label: 'Escrow management fee',
    },
    reservationDeposit: {
        type: Number,
        label: 'Reservation deposit'
    },
    serviceFee: {
        type: Number,
        label: 'Service fee',
    },
    managementFee: {
        type: Number,
        label: 'Tenant Management fee',
    },
    germanyVat: {
        type: Number,
        label: 'Germany VAT',
    },
    israelVat: {
        type: Number,
        label: 'Israel VAT',
    },
    isTesting: {
        type:  Boolean,
        label: 'Is in testing mode',
        defaultValue: true
    },
    testingEmail: {
        type: String,
        label: 'Testing email',
        defaultValue: 'sweetProcess.text@gmail.com'
    },
    bcc: {
        type: String,
        label: 'BCC1 email'
    },
    bcc2: {
        type: String,
        label: 'BCC2 email'
    },
    lateFee: {
        type: Number,
        label: 'Late rent fees',
    }
},  { tracker: Tracker });

Configurations.attachSchema(ConfigurationSchema);