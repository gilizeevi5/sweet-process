import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

Notaries = new Mongo.Collection('notaries');

Notaries.allow({
    insert: function(userId) {
        return !!userId;
    },
    update: function(userId) {
        return !!userId;
    },
    remove: function(userId) {
        return !!userId;
    }
});

NotarySchema = new SimpleSchema({
    displayName: {
        type: 'String',
        label: 'Name'
    },
    email: {
        type: 'String',
        label: 'Email'
    },
    simpleName: {
        type: 'String',
        label: 'Contact first name'
    }
});

Notaries.attachSchema(NotarySchema,  { tracker: Tracker });