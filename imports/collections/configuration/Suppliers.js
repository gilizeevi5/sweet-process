import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

export const Suppliers = new Mongo.Collection('suppliers');

Suppliers.allow({
    insert: function(userId) {
        return !!userId;
    },
    update: function(userId) {
        return !!userId;
    },
    remove: function(userId) {
        return !!userId;
    }
});

SupplierSchema = new SimpleSchema({
    displayName: {
        type: 'String',
        label: 'Name'
    },
    email: {
        type: 'String',
        label: 'Email'
    },
    simpleName: {
        type: 'String',
        label: 'Contact first name'
    }
},  { tracker: Tracker });

Suppliers.attachSchema(SupplierSchema);