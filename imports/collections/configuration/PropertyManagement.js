import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

PropertyManagementCompanies = new Mongo.Collection('propertyManagementCompanies');

PropertyManagementCompanies.allow({
    insert: function(userId) {
        return !!userId;
    },
    update: function(userId) {
        return !!userId;
    },
    remove: function(userId) {
        return !!userId;
    }
});

PropertyManagementSchema = new SimpleSchema({
    email: {
        type: String,
        label: 'Email'
    },
    name: {
        type: String,
        label: 'Name'
    },
    address: {
        type: String,
        label: 'Address'
    },
    phone: {
        type: String,
        label: 'Phone'
    },
    contact: {
        type: String,
        label: 'Contact name'
    },
    contactMobile: {
        type: String,
        label: 'Contact mobile number'
    },
    IBAN: {
        type: String,
        label: 'Payment IBAN'
    }
}, { tracker: Tracker });

PropertyManagementCompanies.attachSchema(PropertyManagementSchema);