AccountLawyers = new Mongo.Collection('accountLawyers');


AccountLawyers.allow({
    insert: function(userId) {
        return !!userId;
    },
    update: function(userId) {
        return !!userId;
    },
    remove: function(userId) {
        return !!userId;
    }
});
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

AccountLawyerSchema = new SimpleSchema({
    displayName: {
        type: 'String',
        label: 'Name'
    },
    email: {
        type: 'String',
        label: 'Email'
    },
    simpleName: {
        type: 'String',
        label: 'Contact first name'
    }
}, { tracker: Tracker });

AccountLawyers.attachSchema(AccountLawyerSchema);