import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

LinkedAccountingIssues = new Mongo.Collection('linkedAccountingIssues');

LinkedAccountingIssues.allow({
    insert: function(userId) {
        return !!userId;
    },
    update: function(userId) {
        return !!userId;
    },
    remove: function(userId) {
        return !!userId;
    }
});

LinkedAccountingIssuesSchema = new SimpleSchema({
    executionDate: {
        type: String,
        label: 'Execution Date',
        index: 1
    },
    valueDate: {
        type: String,
        label: 'Value Date'
    },
    payee: {
        type: String,
        label: 'Payee'
    },
    payer: {
        type: String,
        label: 'Payer'
    },
    accountNumber: {
        type: Number,
        label: 'Account Number',
        optional: true
    },
    IBAN: {
        type: String,
        label: 'IBAN',
        optional: true
    },
    BIC: {
        type: String,
        label: 'BIC',
        optional: true
    },
    BLZ: {
        type: String,
        label: 'BLZ',
        optional: true
    },
    purpose: {
        type: String,
        label: 'Purpose'
    },
    currency: {
        type: String,
        label: 'Currency'
    },
    sum: {
        type: String,
        label: 'Sum',
    },
    op: {
        type: String,
        label: 'Operation'
    },
    manualLink: {
        type: Boolean,
        label: 'Manual Link'
    },
    linkDate: {
        type: Date,
        label: 'Date link was made'
    },
    userId: {
        type: String,
        label: 'User Id'
    }
}, { tracker: Tracker });

LinkedAccountingIssues.attachSchema(LinkedAccountingIssuesSchema);