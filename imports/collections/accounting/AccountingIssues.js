import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

AccountingIssues = new Mongo.Collection('accountingIssues');

AccountingIssues.allow({
    insert: function(userId) {
        return !!userId;
    },
    update: function(userId) {
        return !!userId;
    },
    remove: function(userId) {
        return !!userId;
    }
});

AccountingIssuesSchema = new SimpleSchema({
    executionDate: {
        type: String,
        label: 'Execution Date'
    },
    valueDate: {
        type: String,
        label: 'Value Date'
    },
    payee: {
        type: String,
        label: 'Payee'
    },
    payer: {
        type: String,
        label: 'Payer'
    },
    accountNumber: {
        type: Number,
        label: 'Account Number',
        optional: true
    },
    IBAN: {
        type: String,
        label: 'IBAN',
        optional: true
    },
    BIC: {
        type: String,
        label: 'BIC',
        optional: true
    },
    BLZ: {
        type: String,
        label: 'BLZ',
        optional: true
    },
    purpose: {
        type: String,
        label: 'Purpose'
    },
    currency: {
        type: String,
        label: 'Currency'
    },
    sum: {
        type: String,
        label: 'Sum',
    },
    op: {
        type: String,
        label: 'Operation'
    }
}, { tracker: Tracker });

AccountingIssues.attachSchema(AccountingIssuesSchema);