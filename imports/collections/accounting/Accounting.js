import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import AccountingYear from './AccountingYear';

export const Accounting = new SimpleSchema({
    years: {
        type: Array,
        label: 'Accounting Years',
        autoform: {
            type: 'hidden'
        }
    },
    'years.$': {
        type: AccountingYear,
        label: 'Accounting Year',
        autoform: {
            type: 'hidden'
        }
    }
}, { tracker: Tracker });