import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

const AccountingPayment = new SimpleSchema({
    paymentType: {
        type: String,
        optional: true
    },
    period: {
        type: String,
        optional: true
    },
    date: {
        type: Date,
        optional: true
    },
    monthIdx: {
        type: Number,
        optional: true
    },
    value: {
        type: Number,
        optional: true,
    },
    isLinked: {
        type: Boolean,
        optional: true
    },
    isSplit: {
        type: Boolean,
        optional: true
    },
    note: {
        type: String,
        optional: true
    },
    bankTransaction: {
        type: Object,
        optional: true,
        blackbox: true
    }
}, { tracker: Tracker });