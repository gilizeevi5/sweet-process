import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import AccountingPayment from './AccountingPayment';

export const AccountingYear = new SimpleSchema({
    tenantPayments: {
        type: Array,
        label: 'Tenant Payments',
        optional: true
    },
    'tenantPayments.$': {
        type: Array,
        label: 'Monthly Tenant Payments',
        optional: true,
        blackbox: true
    },
    'tenantPayments.$.$': {
        type: AccountingPayment
    },
    ownerPayments: {
        type: Array,
        label: 'Owner Payments',
        optional: true
    },
    'ownerPayments.$': {
        type: Array,
        label: 'Monthly Owner Payments',
        optional: true,
        blackbox: true
    },
    'ownerPayments.$': {
        type: AccountingPayment
    },
    managementPayments: {
        type: Array,
        label: 'Management Payments',
        optional: true
    },
    'managementPayments.$': {
        type: Array,
        label: 'Monthly Management Payments',
        optional: true,
        blackbox: true
    },
    'managementPayments.$.$': {
        type: AccountingPayment
    },
    landTaxPayments: {
        type: Array,
        label: 'Land tax Payments',
        optional: true
    },
    'landTaxPayments.$': {
        type: Array,
        label: 'Monthly Land tax Payments',
        optional: true,
        blackbox: true
    },
    'landTaxPayments.$.$': {
        type: AccountingPayment
    },
}, { tracker: Tracker });