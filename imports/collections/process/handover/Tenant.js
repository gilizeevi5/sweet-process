import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import Validators from '../Validators';
import PreviousRent from './PreviousRent';

export const Tenant = new SimpleSchema({
    firstName: {
        type: String,
        label: 'First name',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    lastName: {
        type: String,
        label: 'Last name',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    phoneNumber: {
        type: String,
        label: 'Phone number',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    email: {
        type: String,
        label: 'Email',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    rentalContractSigned: {
        type: Date,
        label: 'Date contract signed',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    actualRentBrutto: {
        type: Number,
        label: 'Actual rent brutto',
        optional: true,
        custom:  Validators.inputTenantDataTenantRequireValidation
    },
    actualRentBruttoDate: {
        type: Date,
        label: 'Date rent brutto set',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    previousRentBruttos: {
        type: Array,
        label: 'Previous rent',
        optional: true
    },
    'previousRentBruttos.$': {
        type: PreviousRent
    },
    actualRentNetto: {
        type: Number,
        label: 'Actual rent netto',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    actualRentNettoDate: {
        type: Date,
        label: 'Date rent netto set',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    lastIncreaseDate: {
        type: Date,
        label: 'Last rent increase date',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    deposit: {
        type: Number,
        label: 'Deposit',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    tenantIBAN: {
        type: String,
        label: 'Tenant IBAN',
        optional: true,
        index: 1
    },
    extraIBANS: {
        type: Array,
        label: 'Extra IBAN',
        optional: true

    },
    'extraIBANS.$': String,
}, { tracker: Tracker });