import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

export const SecondaryTenant = new SimpleSchema({
    firstName: {
        type: String,
        label: 'First name',
        optional: true,
    },
    lastName: {
        type: String,
        label: 'Last name',
        optional: true,
    },
    phoneNumber: {
        type: String,
        label: 'Phone number',
        optional: true,
    },
    email: {
        type: String,
        label: 'Email',
        optional: true,
    },
    tenantIBAN: {
        type: String,
        label: 'Tenant IBAN',
        optional: true,
        index: 1
    },
    extraIBANS: {
        type: Array,
        label: 'Extra IBAN',
        optional: true

    },
    'extraIBANS.$': String,

}, { tracker: Tracker });