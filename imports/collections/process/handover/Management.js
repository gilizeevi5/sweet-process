import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import Validators from '../Validators';
import Tenant from './Tenant';
import SecondaryTenant from './SecondaryTenant';
import ManagementCompany from './ManagementCompany';
import RentDueReminderAction from '../../management/RentDueReminderAction';

export const Management = new SimpleSchema({
    contractDate: {
        type: Date,
        label: 'Contract date',
        defaultValue: new Date(),
        optional: true,
        custom: sendMgmtContractAndPOARequiredValidation
    },
    managementFee: {
        type: Number,
        label: 'Tenant Management Monthly Fee',
        autoValue: function () {
            var conf = Configurations.findOne(),
                doc = Processes.findOne({_id: this.docId});
            if (this.operator === null && !this.isSet) {
                if (conf && doc && doc.realty.town.toUpperCase() === "Berlin".toUpperCase()) {
                    return parseInt(conf.managementFee);
                } else {
                    return 0;
                }
            }
        },
        optional: true,
        custom: sendMgmtContractAndPOARequiredValidation
    },
    rentalFee: {
        type: Number,
        label: 'Rental fee',
        defaultValue: 0,
        optional: true,
    },
    managementContractPath: {
        type: String,
        label: 'Contract path',
        autoform: {
            type: 'hidden'
        },
        optional: true,
        custom: uploadMgmtContractAndPOARequiredValidation
    },
    managementContractFileName: {
        type: String,
        label: 'Contract file name',
        autoform: {
            type: 'hidden'
        },
        optional: true,
        custom: uploadMgmtContractAndPOARequiredValidation
    },
    POAPath: {
        type: String,
        label: 'POA path',
        autoform: {
            type: 'hidden'
        },
        optional: true,
        custom: uploadMgmtContractAndPOARequiredValidation
    },
    POAFileName: {
        type: String,
        label: 'POA file name',
        autoform: {
            type: 'hidden'
        },
        optional: true,
        custom: uploadMgmtContractAndPOARequiredValidation
    },
    clientIBAN: {
        type: String,
        label: 'Client IBAN',
        optional: true,
        custom: uploadMgmtContractAndPOARequiredValidation
    },
    purchaseContractSignatureDate: {
        type: Date,
        label: 'Purchase contract signature date',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    handoverDate: {
        type: Date,
        label: 'Handover date',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    paymentStartDate: {
        type: Date,
        label: 'Payment start date',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    tenant: {
        type: Tenant,
        label: 'Primary Tenant',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    secondTenant: {
        type: SecondaryTenant,
        label: 'Second Tenant',
        optional: true
    },
    isFurnished: {
        type: Boolean,
        label: 'Furnished apartment',
        optional: true,
        custom: Validators.inputTenantDataTenantRequireValidation
    },
    managementCompany: {
        type: ManagementCompany,
        label: 'Current Management ',
        optional: true,
        custom: introToManagementCompanyRequireValidation
    },
    firstRentalPaymentDate: {
        type: Date,
        label: 'First payment received date',
        optional: true,
        custom: firstRentalRequireValidation
    },
    firstRentalPaymentSum: {
        type: Number,
        label: 'First payment received sum',
        optional: true,
        custom: firstRentalRequireValidation
    },
    finanzamtName: {
        type: String,
        label: 'Finanzamt Name',
        optional: true,
        custom: informFinanzamtRequireValidation
    },
    finanzamtEmail: {
        type: String,
        label: 'Finanzamt Email',
        optional: true,
        custom: informFinanzamtRequireValidation
    },
    taxNumber: {
        type: String,
        label: 'Tax Number',
        optional: true,
        custom: confirmFinanzamtRequireValidation
    },
    quarterlyPayment: {
        type: Number,
        label: 'Quarterly Payment',
        optional: true,
        custom: confirmFinanzamtRequireValidation
    },
    shouldSendFirstReminder: {
        type: Boolean,
        defaultValue: false,
        optional: true,
        autoform: {
            type: 'hidden'
        }
    },
    firstReminder: {
        type: RentDueReminderAction,
        optional: true,
        autoform: {
            type: 'hidden'
        }
    },
    shouldSendSecondReminder: {
        type: Boolean,
        defaultValue: false,
        optional: true,
        autoform: {
            type: 'hidden'
        }
    },
    secondReminder: {
        type: RentDueReminderAction,
        optional: true,
        autoform: {
            type: 'hidden'
        }
    },
    notes: {
        type: Array,
        label: 'Notes',
        optional: true,
        autoform: {
            type: 'hidden'
        },
        blackbox: true
    },
    'notes.$': Object,
}, { tracker: Tracker });


function sendMgmtContractAndPOARequiredValidation() {
    var doc = Processes.findOne({_id: this.docId}),
        shouldBeRequired;
    if (doc && doc.step >= 40) {
        shouldBeRequired = true
    }
    if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
            if (!this.isSet || this.value === null || this.value === "") return "required";
        }

        // updates
        else if (this.isSet) {
            if (this.operator === "$set" && this.value === null || this.value === "") return "required";
            if (this.operator === "$unset") return "required";
            if (this.operator === "$rename") return "required";
        }
    }
}

function uploadMgmtContractAndPOARequiredValidation() {
    var doc = Processes.findOne({_id: this.docId}),
        shouldBeRequired;
    if (doc && doc.step >= 41) {
        shouldBeRequired = true
    }
    if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
            if (!this.isSet || this.value === null || this.value === "") return "required";
        }

        // updates
        else if (this.isSet) {
            if (this.operator === "$set" && this.value === null || this.value === "") return "required";
            if (this.operator === "$unset") return "required";
            if (this.operator === "$rename") return "required";
        }
    }
}

function introToManagementCompanyRequireValidation() {
    var doc = Processes.findOne({_id: this.docId}),
        shouldBeRequired;
    if (doc && doc.step >= 45) {
        shouldBeRequired = true;
    } else {
        shouldBeRequired = false;
    }
    if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
            if (!this.isSet || this.value === null || this.value === "") return "required";
        }

        // updates
        else if (this.isSet) {
            if (this.operator === "$set" && this.value === null || this.value === "") return "required";
            if (this.operator === "$unset") return "required";
            if (this.operator === "$rename") return "required";
        }
    }
}


function firstRentalRequireValidation() {
    var doc = Processes.findOne({_id: this.docId}),
        shouldBeRequired;
    if (doc && doc.step >= 47) {
        shouldBeRequired = true;
    } else {
        shouldBeRequired = false;
    }
    if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
            if (!this.isSet || this.value === null || this.value === "") return "required";
        }

        // updates
        else if (this.isSet) {
            if (this.operator === "$set" && this.value === null || this.value === "") return "required";
            if (this.operator === "$unset") return "required";
            if (this.operator === "$rename") return "required";
        }
    }
}

function informFinanzamtRequireValidation() {
    var doc = Processes.findOne({_id: this.docId}),
        shouldBeRequired;
    if (doc && doc.step >= 49) {
        shouldBeRequired = true;
    } else {
        shouldBeRequired = false;
    }
    if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
            if (!this.isSet || this.value === null || this.value === "") return "required";
        }

        // updates
        else if (this.isSet) {
            if (this.operator === "$set" && this.value === null || this.value === "") return "required";
            if (this.operator === "$unset") return "required";
            if (this.operator === "$rename") return "required";
        }
    }
}

function confirmFinanzamtRequireValidation() {
    var doc = Processes.findOne({_id: this.docId}),
        shouldBeRequired;
    if (doc && doc.step >= 50) {
        shouldBeRequired = true;
    } else {
        shouldBeRequired = false;
    }
    if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
            if (!this.isSet || this.value === null || this.value === "") return "required";
        }

        // updates
        else if (this.isSet) {
            if (this.operator === "$set" && this.value === null || this.value === "") return "required";
            if (this.operator === "$unset") return "required";
            if (this.operator === "$rename") return "required";
        }
    }
}