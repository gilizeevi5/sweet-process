import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

export const PreviousRent = new SimpleSchema({
    rentBrutto: {
        type: Number,
        label: 'Rent brutto',
        optional: true,
    },
    rentBruttoDate: {
        type: Date,
        label: 'Date rent brutto set',
        optional: true,
    }
}, { tracker: Tracker });