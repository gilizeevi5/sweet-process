import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

export const ManagementCompany =  new SimpleSchema({
    details: {
        type: String,
        label: 'Property Management Company',
        autoform: {
            type: 'select2',
            options: function () {
                var propertyManagementCompany =  PropertyManagementCompanies.find().map(function (option) {
                    return {'label': option.name, 'value': option._id};
                });
                return propertyManagementCompany;
            }
        },
        optional: false,
        index: 1,
        custom: inputManagementDataRequireValidation
    },
    housegeld: {
        type: Number,
        label: 'Housegeld',
        optional: true,
        custom: inputManagementDataRequireValidation
    },
    apartmentIBAN: {
        type: String,
        label: 'Apartment IBAN',
        optional: true
    }
}, { tracker: Tracker });

function inputManagementDataRequireValidation() {
    var doc = Processes.findOne({_id: this.docId}),
        shouldBeRequired;
    if (doc && doc.step >= 46) {
        shouldBeRequired = true;
    } else {
        shouldBeRequired = false;
    }
    if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
            if (!this.isSet || this.value === null || this.value === "") return "required";
        }

        // updates
        else if (this.isSet) {
            if (this.operator === "$set" && this.value === null || this.value === "") return "required";
            if (this.operator === "$unset") return "required";
            if (this.operator === "$rename") return "required";
        }
    }
}