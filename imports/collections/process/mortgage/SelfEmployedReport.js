import SimpleSchema from "simpl-schema";
import { Tracker } from 'meteor/tracker';
import Validators from '../Validators';

export const SelfEmployedReport = new SimpleSchema({
    taxYear: {
        type: String,
        label: 'Tax year',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    annualIncome: {
        type: Number,
        label: 'Annual income',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    taxableIncome: {
        type: Number,
        label: 'Taxable income',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    incomeTax: {
        type: Number,
        label: 'Income tax',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    refunds: {
        type: Number,
        label: 'Refunds',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    netTax: {
        type: Number,
        label: 'Net tax',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    advancePayment: {
        type: Number,
        label: 'Advance payments',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    balanceToPay: {
        type: Number,
        label: 'Balance left to pay',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    }
}, { tracker: Tracker });