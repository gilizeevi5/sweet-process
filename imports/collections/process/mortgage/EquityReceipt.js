import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

SimpleSchema.extendOptions(['autoform', 'index']);

const EquityReceipt = new SimpleSchema({
    receiptPath: {
        type: String,
        label: 'Receipt path',
        autoform: {
            type: 'hidden'
        },
        optional: true
    },
    receiptFileName: {
        type: String,
        label: 'Receipt file name',
        autoform: {
            type: 'hidden'
        },
        optional: true
    }
}, { tracker: Tracker });

export default EquityReceipt;