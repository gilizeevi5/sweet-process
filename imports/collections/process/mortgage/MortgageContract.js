import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

SimpleSchema.extendOptions(['autoform', 'index']);

export const MortgageContract = new SimpleSchema({
    contractPath: {
        type: String,
        label: 'Contract path',
        autoform: {
            type: 'hidden'
        },
        optional: true
    },
    contractFileName: {
        type: String,
        label: 'Contract file name',
        autoform: {
            type: 'hidden'
        },
        optional: true
    }
}, { tracker: Tracker });