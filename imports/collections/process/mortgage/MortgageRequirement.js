import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

export const MortgageRequirement = new SimpleSchema({
    requiresMortgage: {
        type: Boolean,
        label: 'Requires mortgage',
        defaultValue: true
    },
    includeClient1: {
        type: Boolean,
        label: 'Include client 1 in mortgage',
        optional: true,
        defaultValue: true
    },
    includeClient2: {
        type: Boolean,
        label: 'Include client 2 in mortgage',
        optional: true,
        defaultValue: false
    },
    includeClient3: {
        type: Boolean,
        label: 'Include client 3 in mortgage',
        optional: true,
        defaultValue: false
    }
}, { tracker: Tracker });