import SimpleSchema from "simpl-schema";
import { Tracker } from 'meteor/tracker';
import Validators from '../Validators';

export const Form106 = new SimpleSchema({
    sameEmployer: {
        type: Boolean,
        label: 'Same employer as previous year',
        optional: true,
        autoform: {
            afFieldInput: {
                class: "js-sameEmployer"
            }
        }
    },
    year: {
        type: String,
        label: 'Year',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    employerName: {
        type: String,
        label: 'Employer name',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    employerAddress: {
        type: String,
        label: 'Employer address',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    employerTaxNumber: {
        type: String,
        label: 'Employer tax number',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    salaryBrutto: {
        type: Number,
        label: 'Salary brutto',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    taxableSalary: {
        type: Number,
        label: 'Taxable salary',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    incomeTax: {
        type: Number,
        label: 'Income tax',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    nationalPension: {
        type: Number,
        label: 'National pension',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    healthInsurance: {
        type: Number,
        label: 'Health insurance',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    voluntaryPension: {
        type: Number,
        label: 'Voluntary insurance',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    lifeInsurance: {
        type: Number,
        label: 'Life insurance',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    refunds: {
        type: Number,
        label: 'Refunds',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    }
}, { tracker: Tracker });