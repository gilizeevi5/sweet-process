import SimpleSchema from "simpl-schema";
import { Tracker } from 'meteor/tracker';
import Validators from '../Validators';

export const IncomeAndExpenses= new SimpleSchema({
    numberOfDependantChildren: {
        type: Number,
        label: 'Number of dependant children',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    worthOfOwnedRealestate: {
        type: Number,
        label: 'Worth of owned real-estate',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    bankAndSavingsDeposits: {
        type: Number,
        label: 'Bank and savings deposits',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    otherAssets: {
        type: Number,
        label: 'Other assets',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    annualNetIncome: {
        type: Number,
        label: 'Annual income',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    annualChildAllowance: {
        type: Number,
        label: 'Annual child allowance',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    annualCapitalIncome: {
        type: Number,
        label: 'Annual capital income',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    annualRentalIncome: {
        type: Number,
        label: 'Annual rental income',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    annualLivingExpenses: {
        type: Number,
        label: 'Annual living expenses',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    annualAccommodationExpenses: {
        type: Number,
        label: 'Annual accommodation expenses',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    otherAnnualExpenses: {
        type: Number,
        label: 'Other annual expenses',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    annualPremiumsForPrivateHealthInsurance: {
        type: Number,
        label: 'Annual premium for private health insurance',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    }
}, { tracker: Tracker });