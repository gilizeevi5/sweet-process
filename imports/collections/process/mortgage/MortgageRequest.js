import SimpleSchema from 'simpl-schema';
import MortgageContract from './MortgageContract';
import EquityReceipt from './EquityReceipt';
import { Tracker } from 'meteor/tracker';
import Validators from '../Validators'

export const MortgageRequest = new SimpleSchema({
    mortgageBroker: {
        type: String,
        label: 'Mortgage broker',
        autoform: {
            type: 'select2',
            options: function () {
                return MortgageBrokers.find().map(function (c) {
                    return {label: c.displayName, value: c._id};
                });
            }
        },
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(7, this.docId);
        }
    },
    iban: {
        type: String,
        label: 'IBAN',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(8, this.docId);
        }
    },
    bankAccountName: {
        type: String,
        label: 'Bank account holder name',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(8, this.docId);
        }
    },
    bankNameAddress: {
        type: String,
        label: 'Bank name and address',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(8, this.docId);
        }
    },
    mortgageApprovedSum: {
        type: Number,
        label: 'Approved mortgage sum',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(9, this.docId);
        }
    },
    clientSignatureAppointment: {
        type: 'datetime',
        label: 'Mortgage signature appointment',
        optional: true,
        autoform: {
            afFieldInput: {
                type: "bootstrap-datetimepicker",
                dateTimePickerOptions: {
                    format: "DD/MM/YYYY, HH:mm"
                }
            }
        },
        custom() {
            return Validators.isRequiredStepAndMortgage(10, this.docId);
        }
    },
    requiresSeparateNotary: {
        type: Boolean,
        label: 'Requires separate notary meeting',
        custom() {
            return Validators.isRequiredStepAndMortgage(15, this.docId);
        }
    },
    finalDate: {
        type: 'datetime',
        label: 'Selected date for mortgage notarization',
        optional: true,
        autoform: {
            afFieldInput: {
                type: "bootstrap-datetimepicker",
                timezoneId: "Europe/Berlin",
                dateTimePickerOptions: {
                    format: "DD/MM/YYYY, HH:mm"
                }
            }
        },
        custom() {
            return Validators.isRequiredStepAndMortgage(17, this.docId);
        }
    },
    proposedDate1: {
        type: 'datetime',
        label: 'Suggested date for mortgage notarization #1',
        optional: true,
        autoform: {
            afFieldInput: {
                type: "bootstrap-datetimepicker",
                timezoneId: "Europe/Berlin",
                dateTimePickerOptions: {
                    format: "DD/MM/YYYY, HH:mm"
                }
            }
        },
        custom() {
            return Validators.isRequiredStepAndMortgage(16, this.docId);
        }
    },
    proposedDate2: {
        type: 'datetime',
        label: 'Suggested date for mortgage notarization #1',
        optional: true,
        autoform: {
            afFieldInput: {
                type: "bootstrap-datetimepicker",
                timezoneId: "Europe/Berlin",
                dateTimePickerOptions: {
                    format: "DD/MM/YYYY, HH:mm"
                }
            }
        },
        custom() {
            return Validators.isRequiredStepAndMortgage(16, this.docId);
        }
    },
    proposedDate3: {
        type: 'datetime',
        label: 'Suggested date for mortgage notarization #1',
        optional: true,
        autoform: {
            afFieldInput: {
                type: "bootstrap-datetimepicker",
                timezoneId: "Europe/Berlin",
                dateTimePickerOptions: {
                    format: "DD/MM/YYYY, HH:mm"
                }
            }
        },
        custom() {
            return Validators.isRequiredStepAndMortgage(16, this.docId);
        }
    },
    contract: {
        type: MortgageContract,
        label: 'Mortgage Contract',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(11, this.docId);
        }
    },
    equityReceipt: {
        type: EquityReceipt,
        label: 'Equity Receipt',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(19, this.docId);
        }
    }
}, { tracker: Tracker });
