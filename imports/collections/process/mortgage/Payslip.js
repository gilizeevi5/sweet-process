import SimpleSchema from "simpl-schema";
import { Tracker } from 'meteor/tracker';
import Validators from '../Validators';

export const Payslip = new SimpleSchema({
    sameEmployer: {
        type: Boolean,
        label: 'Same employer as previous month',
        optional: true,
        autoform: {
            afFieldInput: {
                class: "js-sameEmployer"
            }
        }
    },
    employerName: {
        type: String,
        label: 'Employer name',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    employerAddress: {
        type: String,
        label: 'Employer address',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    employerTaxNumber: {
        type: String,
        label: 'Employer tax number',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    monthAndYear: {
        type: String,
        label: 'Month and year',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    employedSince: {
        type: Date,
        label: 'Employed since',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    salaryType: {
        type: String,
        label: 'Salary type',
        autoValue: function (){
            return 'Monatlich';
        },
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    salaryBrutto: {
        type: Number,
        label: 'Salary Brutto',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    incomeTax: {
        type: Number,
        label: 'Income tax',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    nationalPension: {
        type: Number,
        label: 'National pension',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    healthInsurance: {
        type: Number,
        label: 'Health insurance',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    voluntaryPension: {
        type: Number,
        label: 'Voluntary pension',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    otherExpenses: {
        type: String,
        label: 'Other expenses',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    salaryNetto: {
        type: Number,
        label: 'Salary Netto',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
}, { tracker: Tracker });