import SimpleSchema from "simpl-schema";
import { Tracker } from 'meteor/tracker';
import { Mongo } from 'meteor/mongo';
import { Processes } from './Processes';

SimpleSchema.extendOptions(['autoform', 'index']);

export const Realty = new SimpleSchema({
    referenceNumber: {
        type: String,
        label: 'Reference number',
        optional: true,
        index: 1,
        autoform: {
            type: 'hidden'
        }
    },
    propertyType: {
        type: String,
        label: 'Property type',
        defaultValue: 'Apartment',
        autoform: {
            type: 'select',
            options: function () {
                return [
                    {'label': 'Apartment', 'value': 'Apartment'},
                    {'label': 'Building', 'value': 'Building'}
                ]
            },
        },
        optional: false,
        index: 1
    },
    supplier: {
        type: String,
        label: 'Supplier',
        autoform: {
            type: 'select2',
            options: function () {
                var suppliers =  Suppliers.find().map(function (option) {
                    return {'label': option.displayName, 'value': option._id};
                });
                return suppliers;
            },
        },
        optional: false
    },
    notary: {
        type: String,
        label: 'Notary',
        autoform: {
            type: 'select2',
            options: function () {
                var notaries =  Notaries.find().map(function (option) {
                    return {'label': option.displayName, 'value': option._id};
                });
                return notaries;
            }
        },
        optional: false
    },
    addressStreet: {
        type: String,
        label: 'Address street',
        optional: false
    },
    addressNumber: {
        type: String,
        label: 'Address number',
        optional: false
    },
    apartmentNumber: {
        type: String,
        label: 'Apartment number',
        optional: false
    },
    floor: {
        type: String,
        label: 'Floor',
        optional: false
    },
    location: {
        type: String,
        label: 'Location',
        optional: false
    },
    town: {
        type: String,
        label: 'Town',
        optional: false
    },
    country: {
        type: String,
        label: 'Country',
        defaultValue: function() {
            // Todo: fetch from configurations
            return 'Germany';
        },
        optional: false
    },
    postcode: {
        type: Number,
        label: 'Postcode',
        optional: false
    },
    size: {
        type: String,
        label: 'Size',
        optional: false,
    },
    price: {
        type: Number,
        label: 'Price',
        optional: false,
    },
    status: {
        type: String,
        label: 'Status',
        autoform: {
            type: "select2",
            options: function () {
                return [
                    {label: "Vermietet", value: 'vermietet'},
                    {label: "Leer", value: 'leer'}
                ];
            }
        },
        optional: false
    },
    landTax: {
        type: Number,
        label: 'Land tax',
        defaultValue: function () {
            var doc = Configurations.findOne();
            if (doc) {
                return doc.landTax;
            } else {
                return 0;
            }
        },
        optional: false,
    },
    mortgageBrokerFee: {
        type: Number,
        label: 'Mortgage broker fee',
        defaultValue: function () {
            var doc = Configurations.findOne();
            if (doc) {
                return doc.mortgageBrokerFee;
            } else {
                return 0;
            }
        },
        optional: false,
    },
    notaryFee: {
        type: Number,
        label: 'Notary fee',
        defaultValue: function () {
            var doc = Configurations.findOne();
            if (doc) {
                return doc.notaryFee;
            } else {
                return 0;
            }
        },
        optional: false,
    },
    registrationFee: {
        type: Number,
        label: 'Registration fee',
        defaultValue: function () {
            var doc = Configurations.findOne();
            if (doc) {
                return doc.registrationFee;
            } else {
                return 0;
            }
        },
        optional: false,
    },
    escrowFee: {
        type: Number,
        label: 'Escrow management fee',
        defaultValue: function () {
            var doc = Configurations.findOne();
            if (doc) {
                return doc.escrowFee;
            } else {
                return 0;
            }
        },
        optional: false,
    },
    reservationDeposit: {
        type: Number,
        label: 'Reservation deposit',
        defaultValue: function () {
            var doc = Configurations.findOne();
            if (doc) {
                return doc.reservationDeposit;
            } else {
                return 0;
            }
        },
        optional: false,
    },
    serviceFee: {
        type: Number,
        label: 'Service fee',
        defaultValue: function () {
            var doc = Configurations.findOne();
            if (doc) {
                return doc.serviceFee;
            } else {
                return 0;
            }
        },
        optional: false,
    },
    rentInClientsOffer: {
        type: Number,
        label: 'Rent in clients offer',
        optional: true,
        custom: inputTenantDataRequireValidation

    },
    buildingManagementFee: {
        type: Number,
        label: 'Property Management Fee',
        optional: true,
        custom: inputTenantDataRequireValidation
    },
    reserveFund: {
        type: Number,
        label: 'Reserve fund',
        optional: true,
        custom: inputTenantDataRequireValidation

    }
}, { tracker: Tracker });

function inputTenantDataRequireValidation() {
    var doc = Processes.findOne({_id: this.docId}),
        shouldBeRequired;
    if (doc && doc.step >= 42) {
        shouldBeRequired = true;
    } else {
        shouldBeRequired = false;
    }
    if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
            if (!this.isSet || this.value === null || this.value === "") return "required";
        }

        // updates
        else if (this.isSet) {
            if (this.operator === "$set" && this.value === null || this.value === "") return "required";
            if (this.operator === "$unset") return "required";
            if (this.operator === "$rename") return "required";
        }
    }
}