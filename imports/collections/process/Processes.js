import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import Client from './Clients';
import Realty from './Realty';
import { Mongo } from 'meteor/mongo';
import SupplierReservation from './reservation/SupplierReservation';
import MortgageRequirement from './mortgage/MortgageRequirement';
import MortgageRequest from './mortgage/MortgageRequest';
import Notary from './notary/Notary';
import Management from './handover/Management';
import Accounting from '../accounting/Accounting';

SimpleSchema.extendOptions(['autoform', 'index']);

export const Processes = new Mongo.Collection('processes');

Processes.allow({
    insert: function(userId) {
        return !!userId;
    },
    update: function(userId) {
        return !!userId;
    },
    remove: function(userId) {
        return !!userId;
    }
});

const ProcessSchema = new SimpleSchema({
    createdAt: {
        type: Date,
        label: 'Created at',
        defaultValue: new Date(),
        autoform: {
            type: 'hidden'
        }
    },
    lastStepAAdvancedAt: {
        type: Date,
        label: 'Last step A was advanced',
        defaultValue: new Date(),
        autoform: {
            type: 'hidden'
        }
    },
    lastStepBAdvancedAt: {
        type: Date,
        label: 'Last step B was advanced',
        defaultValue: new Date(),
        autoform: {
            type: 'hidden'
        }
    },
    isOnline: {
        type: Boolean,
        label: 'Process is Online',
        defaultValue: true,
        autoform: {
            type: 'hidden'
        }
    },
    isArchived: {
        type: Boolean,
        label: 'Process is Archived',
        defaultValue: false,
        autoform: {
            type: 'hidden'
        },
        index: 1
    },
    isAOverdue: {
        type: Boolean,
        label: 'Process step A is overdue',
        defaultValue: false,
        autoform: {
            type: 'hidden'
        }
    },
    isBOverdue: {
        type: Boolean,
        label: 'Process step B is overdue',
        defaultValue: false,
        autoform: {
            type: 'hidden'
        }
    },
    realty: {
        type: Realty
    },
    primaryClient: {
        type: Client,
        label: 'Primary client',

    },
    includeClient2: {
        type: Boolean,
        label: 'Include second client',
        optional: true
    },
    client2: {
        type: Client,
        label: 'Second client',
        optional: true,
    },
    includeClient3: {
        type: Boolean,
        label: 'Include third client',
        optional: true
    },
    client3: {
        type: Client,
        label: 'Third client',
        optional: true,
    },
    mortgageRequirement: {
        type: MortgageRequirement,
        optional: true
    },
    supplierReservation: {
        type: SupplierReservation,
        optional: true
    },
    mortgageRequest: {
        type: MortgageRequest,
        optional: true
    },
    notary: {
        type: Notary,
        optional: true
    },
    management: {
        type: Management,
        optional: true
    },
    accounting: {
        type: Accounting,
        optional: true,
    },
    log: {
        type: Array,
        label: 'Log',
        optional: true,
        autoform: {
            type: 'hidden'
        },
        blackbox: true
    },
    'log.$': Object,
    notes: {
        type: Object,
        label: 'Notes',
        optional: true,
        autoform: {
            type: 'hidden'
        },
        blackbox: true
    },
    'notes.$': Object,
    phase: {
        type: Number,
        label: 'Phase',
        defaultValue: 0,
        autoform: {
            type: 'hidden'
        },
        index: 1
    },
    step: {
        type: Number,
        label: 'Step',
        defaultValue: 0,
        autoform: {
            type: 'hidden'
        },
        index: -1
    },
    phaseB: {
        type: Number,
        label: 'Phase',
        defaultValue: 0,
        autoform: {
            type: 'hidden'
        },
        optional: true
    },
    stepB: {
        type: Number,
        label: 'Step',
        defaultValue: 0,
        autoform: {
            type: 'hidden'
        },
        optional: true
    },
    isAccountingReady: {
        type: Boolean,
        label: 'Accounting ready',
        optional: true
    }
}, { tracker: Tracker });


Processes.attachSchema(ProcessSchema);

Processes.helpers({
    getFullName() {
        var doc = Processes.findOne({_id: this._id});
        return doc.primaryClient.firstName + ' ' + doc.primaryClient.lastName;
    },
    getProcessStep() {
        var doc = Processes.findOne({_id: this._id}),
            steps;
        steps = Constants.getProcessStep(doc.step)
        if (doc.stepB) {
            steps + ', ' + Constants.getProcessStep(doc.stepB);
        }
        return steps;
    },
    getPropertyAddress() {
        var doc = Processes.findOne({_id: this._id});
        return doc.realty.addressStreet + ' ' +doc.realty.addressNumber;
    },
    getLastUpdateDate() {
        var doc = Processes.findOne({_id: this._id}),
            date;
        date = moment(doc.log[doc.step]).format('D MMMM YYYY, HH:mm');
        if (doc.stepB) {
            date + ', ' + moment(doc.log[doc.stepB]).format('D MMMM YYYY, HH:mm');
        }
        return date;
    }
});
