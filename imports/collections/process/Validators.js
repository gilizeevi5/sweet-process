import { Processes } from './Processes';

export const Validators = {
    isRequiredStepAndMortgage(step) {
        var doc = Processes.findOne({_id: this.docId}),
            shouldBeRequired;
        if (doc && doc.step >= step && doc.mortgageRequirement.requiresMortgage) {
            shouldBeRequired = true
        }
        if (shouldBeRequired) {
            // inserts
            if (!this.operator) {
                if (!this.isSet || this.value === null || this.value === "") return "required";
            }

            // updates
            else if (this.isSet) {
                if (this.operator === "$set" && this.value === null || this.value === "") return "required";
                if (this.operator === "$unset") return "required";
                if (this.operator === "$rename") return "required";
            }
        }
    },
    inputTenantDataTenantRequireValidation() {
        return false;
        var doc = Processes.findOne({_id: this.docId}),
            shouldBeRequired;
        if (doc && doc.step >= 42 && doc.realty.status === 'vermietet') {
            shouldBeRequired = true;
        } else {
            shouldBeRequired = false;
        }
        if (shouldBeRequired) {
            // inserts
            if (!this.operator) {
                if (!this.isSet || this.value === null || this.value === "") return "required";
            }

            // updates
            else if (this.isSet) {
                if (this.operator === "$set" && this.value === null || this.value === "") return "required";
                if (this.operator === "$unset") return "required";
                if (this.operator === "$rename") return "required";
            }
        }
    },
    mortgageRequiredValidation() {
        var doc = Processes.findOne({_id: this.docId}),
            shouldBeRequired;
        if (doc && doc.step >= 6 && doc.mortgageRequirement.requiresMortgage) {
            if (this.key.includes('selfEmployed') && (doc.primaryClient.professionType === 'selfEmployed') ||
                doc.client2.professionType === 'selfEmployed' || doc.client3.professionType === 'selfEmployed') {
                shouldBeRequired = true;
            }
            if ((this.key.includes('payslip') || this.key.includes('106')) && (doc.primaryClient.professionType === 'employed' ||
                    doc.client2.professionType === 'employed' || doc.client3.professionType === 'employed')) {
                shouldBeRequired = true
            }
        }
        if (shouldBeRequired) {
            // inserts
            if (!this.operator) {
                if (!this.isSet || this.value === null || this.value === "") return "required";
            }

            // updates
            else if (this.isSet) {
                if (this.operator === "$set" && this.value === null || this.value === "") return "required";
                if (this.operator === "$unset") return "required";
                if (this.operator === "$rename") return "required";
            }
        }
    },
    mortgageCreateRequiredValidation() {
        var doc = Processes.findOne({_id: this.docId}),
            shouldBeRequired;
        if (doc && doc.step >= 7 && doc.mortgageRequirement.requiresMortgage) {
            if (this.key.includes('incomeAndExpenses')) {
                if (this.key.includes('primary')) {
                    shouldBeRequired = doc.mortgageRequirement.includeClient1;
                } else if (this.key.includes('client2')) {
                    shouldBeRequired = doc.mortgageRequirement.includeClient2;
                } else if (this.key.includes('client3')) {
                    shouldBeRequired = doc.mortgageRequirement.includeClient3;
                }
            }
        }
        if (shouldBeRequired) {
            // inserts
            if (!this.operator) {
                if (!this.isSet || this.value === null || this.value === "") return "required";
            }

            // updates
            else if (this.isSet) {
                if (this.operator === "$set" && this.value === null || this.value === "") return "required";
                if (this.operator === "$unset") return "required";
                if (this.operator === "$rename") return "required";
            }
        }
    }
};