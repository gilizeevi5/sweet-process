import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import Notary from './Notary';

export const PaymentAccount = new SimpleSchema({
    IBAN: {
        type: String,
        label: 'Account IBAN',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(31, this.docId);
        }
    },
    accountHolderName: {
        type: String,
        label: 'Account holder full name',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(31, this.docId);
        }
    },
    bank: {
        type: String,
        label: 'Account bank',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(31, this.docId);
        }
    },
    BIC: {
        type: String,
        label: 'Account BIC',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(31, this.docId);
        }
    },
    reference: {
        type: String,
        label: 'Reference',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(31, this.docId);
        }
    },
    value: {
        type:Number,
        label: 'Value to pay',
        optional: true
    }
}, { tracker: Tracker });