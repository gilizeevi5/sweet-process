import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import PaymentAccount from './PaymentAccount';
import Notary from './Notary';

export const Payment = new SimpleSchema({
    paymentAccount1: {
        type: PaymentAccount,
        label: 'Payable account #1',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(31, this.docId);
        }
    },
    paymentAccount2: {
        type: PaymentAccount,
        label: 'Payable account #2',
        optional: true
    },
    payableToTwoAccounts: {
        type: Boolean,
        label: 'Add a second account',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(31, this.docId);
        }
    },
    includeClient1: {
        type: Boolean,
        label: 'Include client 1 in instruction',
        optional: true,
        defaultValue: true,
    },
    includeClient2: {
        type: Boolean,
        label: 'Include client 2 in instruction',
        optional: true,
        defaultValue: false
    },
    includeClient3: {
        type: Boolean,
        label: 'Include client 3 in instruction',
        optional: true,
        defaultValue: false
    }
}, { tracker: Tracker });