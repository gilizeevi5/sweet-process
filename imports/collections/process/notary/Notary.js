import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import Payment from './Payment';

export const Notary = new SimpleSchema({
    proposedDate1: {
        type: 'datetime',
        label: 'Suggested date for notarization #1',
        optional: true,
        autoform: {
            afFieldInput: {
                type: "bootstrap-datetimepicker",
                timezoneId: "Europe/Berlin",
                dateTimePickerOptions: {
                    format: "DD/MM/YYYY, HH:mm"
                }
            }
        },
        custom() {
            return Validators.isRequiredStepAndMortgage(20, this.docId);
        }
    },
    proposedDate2: {
        type: 'datetime',
        label: 'Suggested date for notarization #2',
        optional: true,
        autoform: {
            afFieldInput: {
                type: "bootstrap-datetimepicker",
                timezoneId: "Europe/Berlin",
                dateTimePickerOptions: {
                    format: "DD/MM/YYYY, HH:mm"
                }
            }
        },
        custom() {
            return Validators.isRequiredStepAndMortgage(20, this.docId);
        }
    },
    proposedDate3: {
        type: 'datetime',
        label: 'Suggested date for notarization #3',
        optional: true,
        autoform: {
            afFieldInput: {
                type: "bootstrap-datetimepicker",
                timezoneId: "Europe/Berlin",
                dateTimePickerOptions: {
                    format: "DD/MM/YYYY, HH:mm"
                }
            }
        },
        custom() {
            return Validators.isRequiredStepAndMortgage(20, this.docId);
        }
    },
    finalDate: {
        type: 'datetime',
        label: 'Selected date for notarization',
        optional: true,
        autoform: {
            afFieldInput: {
                type: "bootstrap-datetimepicker",
                timezoneId: "Europe/Berlin",
                dateTimePickerOptions: {
                    format: "DD/MM/YYYY, HH:mm"
                }
            }
        },
        custom() {
            return Validators.isRequiredStepAndMortgage(21, this.docId);
        }
    },
    conversionRate: {
        type: Number,
        label: 'Service fee conversion rate',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(23, this.docId);
        }
    },
    escrowIBAN: {
        type: String,
        label: 'Account IBAN',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(28, this.docId);
        }
    },
    escrowBIC: {
        type: String,
        label: 'Account BIC',
        optional: true,
        custom() {
            return Validators.isRequiredStepAndMortgage(28, this.docId);
        }
    },
    payment: {
        type: Payment,
        label: 'Payment'
    }
}, { tracker: Tracker });
