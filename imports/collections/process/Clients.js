import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import { Processes } from './Processes';
import Identification from './reservation/Identification';
import Payslip from './mortgage/Payslip';
import Form106 from './mortgage/Form106';
import SelfEmployedReport from './mortgage/SelfEmployedReport';
import IncomeAndExpenses from './mortgage/IncomeAndExpenses';
import Validators from './Validators';

SimpleSchema.extendOptions(['autoform', 'index']);

export const Client = new SimpleSchema({
    firstName: {
        type: String,
        label: 'First Name',
        optional: true,
        custom: multipleClientRequiredValidation
    },
    lastName: {
        type: String,
        label: 'Last Name',
        optional: true,
        custom: multipleClientRequiredValidation
    },
    hebrewName: {
        type: String,
        label: 'Hebrew name',
        optional: true,
        custom: multipleClientRequiredValidation
    },
    birthDate: {
        type: Date,
        label: 'Birth date',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    mainNationality: {
        type: String,
        label: 'Main Nationality',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    addressStreet: {
        type: String,
        label: 'Address street',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    addressNumber: {
        type: String,
        label: 'Address number',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    town: {
        type: String,
        label: 'Town',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    postcode: {
        type: Number,
        label: 'Postcode',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    country: {
        type: String,
        label: 'Country',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    phone: {
        type: String,
        label: 'Phone',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    email: {
        type: String,
        label: 'Email',
        optional: true,
        custom: multipleClientRequiredValidation
    },
    idNumber: {
        type: String,
        label: 'Identification number',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    maritalStatus: {
        type: String,
        label: 'Marital status',
        autoform: {
            type: "select2",
            options: function () {
                return [
                    {label: 'Single', value: 'single'},
                    {label: 'Married', value: 'married'},
                    {label: 'Civil union', value: 'civilUnion'},
                    {label: 'Divorced', value: 'divorced'},
                    {label: 'Widowed', value: 'widowed'}
                ];
            }
        },
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    profession: {
        type: String,
        label: 'Profession',
        optional: true
    },
    professionType: {
        type: String,
        label: 'Employment type',
        autoform: {
            type: "select2",
            options: function () {
                return [
                    {label: 'Employed', value: 'employed'},
                    {label: 'Self employed / independent', value: 'selfEmployed'}
                ];
            }
        },
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    identification: {
        type: Identification,
        label: 'Identification',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    },
    payslip1: {
        type: Payslip,
        label: 'Month #1 payslip',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    payslip2: {
        type: Payslip,
        label: 'Month #2 payslip',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    payslip3: {
        type: Payslip,
        label: 'Month #3 payslip',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    form106Year1: {
        type: Form106,
        label: 'Year #1 106',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    form106Year2: {
        type: Form106,
        label: 'Year #2 106',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    selfEmployed1: {
        type: SelfEmployedReport,
        label: 'Self employed year #1 report',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    selfEmployed2: {
        type: SelfEmployedReport,
        label: 'Self employed year #2 report',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    selfEmployedSince: {
        type: Date,
        label: 'Self employed since',
        optional: true,
        custom: Validators.mortgageRequiredValidation
    },
    incomeAndExpenses: {
        type: IncomeAndExpenses,
        label: 'Income and expenses',
        optional: true,
        custom: Validators.mortgageCreateRequiredValidation
    }
}, { tracker: Tracker });

function multipleClientRequiredValidation() {
    var doc = Processes.findOne({_id: this.docId}),
        shouldBeRequired;
    if (this.key.includes('primary')) {
        shouldBeRequired = true;
    } else if (this.key.includes('client2')) {
        shouldBeRequired = this.field('includeClient2').value;
    } else if (this.key.includes('client3')) {
        shouldBeRequired = this.field('includeClient3').value;
    }
    if (shouldBeRequired) {
        // inserts
        if (!this.operator) {
            if (!this.isSet || this.value === null || this.value === "") return "required";
        }

        // updates
        else if (this.isSet) {
            if (this.operator === "$set" && this.value === null || this.value === "") return "required";
            if (this.operator === "$unset") return "required";
            if (this.operator === "$rename") return "required";
        }
    }
}
