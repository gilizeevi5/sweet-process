import SimpleSchema from "simpl-schema";
import { Tracker } from 'meteor/tracker';

SimpleSchema.extendOptions(['autoform', 'index']);

export const Identification = new SimpleSchema({
    passportNumber: {
        type: String,
        label: 'Passport number',
        optional: true
    },
    signaturePath: {
        type: String,
        label: 'Signature example path',
        autoform: {
            type: 'hidden'
        },
        optional: true
    },
    signatureFileName: {
        type: String,
        label: 'Signature example file name',
        autoform: {
            type: 'hidden'
        },
        optional: true
    },
    passportScanPath: {
        type: String,
        label: 'Passport scan path',
        autoform: {
            type: 'hidden'
        },
        optional: true
    },
    passportScanFileName: {
        type: String,
        label: 'Passport file name',
        autoform: {
            type: 'hidden'
        },
        optional: true
    }
}, { tracker: Tracker });