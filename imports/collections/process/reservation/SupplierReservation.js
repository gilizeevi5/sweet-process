import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

export const SupplierReservation = new SimpleSchema({
    includeClient1: {
        type: Boolean,
        label: 'Include client 1 in reservation',
        optional: true,
        defaultValue: true
    },
    includeClient2: {
        type: Boolean,
        label: 'Include client 2 in reservation',
        optional: true,
        defaultValue: false
    },
    includeClient3: {
        type: Boolean,
        label: 'Include client 3 in reservation',
        optional: true,
        defaultValue: false
    }
}, { tracker: Tracker });