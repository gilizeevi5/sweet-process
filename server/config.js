Accounts.config({
    // forbidClientAccountCreation : true
});

Accounts.emailTemplates.siteName = 'sweet-process';
Accounts.emailTemplates.from = 'sweet-process Admin <sweet-process@sweet-home.co.il>';

Accounts.emailTemplates.resetPassword.from = () => {
    // Overrides the value set in `Accounts.emailTemplates.from` when resetting
    // passwords.
    return 'sweet-process Password Reset <sweet-process@sweet-home.co.il>';
};