import { Meteor } from 'meteor/meteor';
import { Processes } from '../imports/collections/process/Processes';
import _ from 'lodash';

Meteor.startup(() => {
  // code to run on server at startup
    SyncedCron.start();
    updateOverdueProcess();
    // Meteor.call('clearAllAccountingRelatedData');
    // SyncedCron.stop();
    // findDocByPaymentValueAndIBAN(225.28, 'DE54500105175419578470')
});

SyncedCron.add({
    name: 'Check Overdue Processes',
    schedule: function(parser) {
        // parser is a later.parse object
        return parser.text('every 3 hours');
    },
    job: function() {
        updateOverdueProcess();
    }
});

SyncedCron.add({
    name: 'Notify Daily on Overdue Processes',
    schedule: function(parser) {
        // parser is a later.parse object
        return parser.text('at 7:05 PM');
    },
    job: function() {
        notifyOnOverdueProcess();
    }
});

updateOverdueProcess = function() {
    var processes = Processes.find({}).fetch();
    _.each(processes, function isOverdue(process){
        var isPassDeadline,
            stepADeadline,
            stepAAllowedDuration,
            stepBAllowedDuration,
            stepBDeadline;

        if (Constants.steps[process.step] && Constants.steps[process.step].deadline) {
            stepAAllowedDuration = Constants.steps[process.step].deadline;
        } else {
            return;
        }

        if (!process.lastStepAAdvancedAt) {
            Processes.update(process._id, {
                $set: {
                    lastStepAAdvancedAt: new Date()
                }
            });
        }

        stepADeadline = moment(process.lastStepAAdvancedAt).add(stepAAllowedDuration,'days');

        if (process.stepB) {
            if (!process.lastStepBAdvancedAt) {
                Processes.update(process._id, {
                    $set: {
                        lastStepBAdvancedAt: new Date()
                    }
                });
            }
            stepBAllowedDuration = Constants.steps[process.stepB].deadline;
            stepBDeadline = moment(process.lastStepBAdvancedAt).add(stepBAllowedDuration,'days');
        }

        if (stepADeadline.isBefore(moment.now())) {
            Processes.update(process._id, {
                $set: {
                    isAOverdue: true
                }
            });
        }

        if (stepBDeadline && stepBDeadline.isBefore(moment.now())) {
            Processes.update(process._id, {
                $set: {
                    isBOverdue: true
                }
            });
        }

    });
};

function notifyOnOverdueProcess() {
    var recipients = [{"Email": 'gilizeevi@gmail.com'}],
        subject = 'Daily notification on overdue processes',
        html,
        overdueReservations,
        overdueNotarys,
        overdueMortgages,
        overdueHandovers;

    overdueReservations = Processes.find({
        phase: 0,
        isAOverdue: true
    }).fetch();

    overdueNotarys = Processes.find({
        phase: 1,
        isAOverdue: true
    }).fetch();

    overdueMortgages = Processes.find({
        phase: 2,
        isBOverdue: true
    }).fetch();

    overdueHandovers = Processes.find({
        phase: 3,
        isAOverdue: true
    }).fetch();

    SSR.compileTemplate('OverdueProcessItem', '<li>{{process.primaryClient.lastName}} {{process.primaryClient.firstName}} - {{process.realty.addressStreet}} {{process.realty.addressNumber}} - {{process.realty.apartmentNumber}} - <a href="https://sweet-process.scalingo.io/process/{{process._id}}">link</a></li>');
    SSR.compileTemplate('NotifyOnOverdueProcess', Assets.getText('emails/NotifyOnOverdueProcess.html'));
    html = SSR.render("NotifyOnOverdueProcess", {
        overdueReservations: overdueReservations,
        overdueReservationsCount: overdueReservations.length,
        overdueNotarys: overdueNotarys ,
        overdueNotarysCount: overdueNotarys.length,
        overdueMortgages: overdueMortgages,
        overdueMortgagesCount: overdueMortgages.length,
        overdueHandovers: overdueHandovers,
        overdueHandoversCount: overdueHandovers.length
    });

    Meteor.call('sendEmail', recipients, Constants.senders.system, subject, html, true);
};

function findDocByPaymentValueAndIBAN(value, IBAN) {
    const processes = Processes.find({accounting: {$exists: true}}).fetch();
    let matchedProcesses = [];
    processes.forEach(process => {
        if (process.accounting.years[0].ownerPayments) {
            process.accounting.years[0].ownerPayments.forEach(month => {
                if (month) {
                    month.forEach(payment => {
                        if (payment.value === value && payment.bankTransaction.IBAN === IBAN) {
                            matchedProcesses.push(process);
                        }
                    });
                }
            });
        }
        if (process.accounting.years[0].tenantPayments) {
            process.accounting.years[0].tenantPayments.forEach(month => {
                if (month) {
                    month.forEach(payment => {
                        if (payment.value === value && payment.bankTransaction.IBAN === IBAN) {
                            matchedProcesses.push(process);
                        }
                    });
                }
            });
        }
        if (process.accounting.years[0].managementPayments) {
            process.accounting.years[0].managementPayments.forEach(month => {
                if (month) {
                    month.forEach(payment => {
                        if (payment.value === value && payment.bankTransaction.IBAN === IBAN) {
                            matchedProcesses.push(process);
                        }
                    });
                }
            });
        }
        if (process.accounting.years[0].landTaxPayments) {
            process.accounting.years[0].landTaxPayments.forEach(month => {
                if (month) {
                    month.forEach(payment => {
                        if (payment.value === value && payment.bankTransaction.IBAN === IBAN) {
                            matchedProcesses.push(process);
                        }
                    });
                }
            });
        }
    });
    if (matchedProcesses.length > 0) {
        console.log('findDocByPaymentValueAndIBAN, matchedProcesses = ' + JSON.stringify(matchedProcesses));
    } else {
        console.log('findDocByPaymentValueAndIBAN nothing found');
    }
}
