/**
 * Created by gilizeevi on 06/08/16.
 */
import { Processes } from '../imports/collections/process/Processes';

Meteor.publish('mini-processes', function(){
    return Processes.find({'isArchived':{$ne: true}}, {
        sort: { step: 1 },
        fields: {
            'primaryClient.firstName': true,
            'primaryClient.lastName': true,
            'realty.addressStreet': true,
            'realty.addressNumber': true,
            'realty.apartmentNumber': true
        }
    });
});
Meteor.publish('processesWithAccounting', function(){
    return Processes.find({ 'step':{$gte: 50, }, 'realty.propertyType': {$not: /Building/}, 'isArchived': {$ne: true} }, {
        sort: { step: 1 },
        fields: {
            log: false
        }
    });
});
Meteor.publish('processes', function(){
    return Processes.find({ 'isArchived': {$ne: true}}, {
        sort: { step: 1 },
        fields: {
            accounting: false,
            log: false
        }
    });
});
Meteor.publish('archivedProcesses', function(){
    return Processes.find({ 'isArchived': true}, {
        sort: { step: 1 },
        fields: {
            accounting: false,
            log: false
        }
    });
});
Meteor.publish('users', function(){
    return Meteor.users.find();
});
Meteor.publish('singleProcess', function (processId) {
    return Processes.find({_id: processId});
});
Meteor.publish('configurations', function(){
    return Configurations.find();
});
Meteor.publish('suppliers', function(){
    return Suppliers.find();
});
Meteor.publish('notaries', function(){
    return Notaries.find();
});
Meteor.publish('mortgageBrokers', function(){
    return MortgageBrokers.find();
});
Meteor.publish('accountLawyers', function(){
    return AccountLawyers.find();
});
Meteor.publish('propertyManagementCompanies', function(){
    return PropertyManagementCompanies.find();
});
Meteor.publish('accountingIssues', function(){
    return AccountingIssues.find();
});
Meteor.publish('ignoredAccountingIssues', function(){
    return IgnoredAccountingIssues.find();
});
Meteor.publish('linkedAccountingIssues', function(){
    return LinkedAccountingIssues.find();
});

