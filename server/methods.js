import { Random } from 'meteor/random';
import _ from 'lodash';
import latinize from 'latinize';
import { Processes } from '../imports/collections/process/Processes';

Meteor.methods({
    addRefToRealty: function(id) {
        var doc = Processes.findOne({_id: id}),
            ref = generateRefNumber(doc);

        function pad(num, size) {
            var s = "000000000" + num;
            return s.substring(s.length - size, s.length);
        };

        Processes.update(id, {
            $set: {
                'realty.referenceNumber': ref
            },
        });
    },
    addRefToRealties: function() {
        var ref,
            processes = Processes.find({}).fetch();

        function pad(num, size) {
            var s = "000000000" + num;
            return s.substring(s.length - size, s.length);
        };

        _.forEach(processes, function(process) {
            ref = generateRefNumber(process);

            Processes.update(process._id, {
                $set: {
                    'realty.referenceNumber': ref
                },
            });
        });
    },
    clearAllAccountingRelatedData: function() {
        var processes = Processes.find({ 'step':{$gte: 50, }, 'realty.propertyType': {$not: /Building/} }).fetch();
        _.forEach(processes, function(process) {
            Processes.update(process._id, {
                $unset: {
                    'accounting': []
                }
            });
        });
        LinkedAccountingIssues.remove({});
        IgnoredAccountingIssues.remove({});
        AccountingIssues.remove({});
    },
    deleteAccountingById: function(id) {
        try {
            var doc = Processes.findOne({_id: id}),
                payments = [],
                linkedAccountingIssues,
                linkedAccountingIssuesIds,
                ignoredAccountingIssues,
                ignoredAccountingIssuesIds;

            if (!doc.accounting || !doc.accounting.years) {
                console.log('nothing to delete');
                return;
            }

            _.each(doc.accounting.years, function(year) {
                _.each(year.tenantPayments, function(month) {
                    _.each(month, function(payment) {
                        payments.push(payment.bankTransaction);
                    });
                });
                _.each(year.ownerPayments, function(month) {
                    _.each(month, function(payment) {
                        payments.push(payment.bankTransaction);
                    });
                });
                _.each(year.managementPayments, function(month) {
                    _.each(month, function(payment) {
                        payments.push(payment.bankTransaction);
                    });
                });
                _.each(year.landTaxPayments, function(month) {
                    _.each(month, function(payment) {
                        payments.push(payment.bankTransaction);
                    });
                });
            });
            console.log('payments = ' + JSON.stringify(payments));
            linkedAccountingIssues = _.intersectionWith(LinkedAccountingIssues.find({}).fetch(), payments, function(issue, payment) {
                var isSame;
                if (issue.IBAN === payment.IBAN &&
                    issue.sum === payment.sum &&
                    issue.executionDate === payment.executionDate &&
                    issue.payee === payment.payee &&
                    issue.payer === payment.payer
                ) {
                    isSame = true;
                }
                return isSame;
            });
            linkedAccountingIssuesIds = _.map(linkedAccountingIssues, '_id');
            console.log('linkedAccountingIssuesIds = ' + JSON.stringify(linkedAccountingIssuesIds));
            ignoredAccountingIssues = _.intersectionWith(IgnoredAccountingIssues.find({}).fetch(), payments, function(issue, payment) {
                var isSame;
                if (issue.IBAN === payment.IBAN &&
                    issue.sum === payment.sum &&
                    issue.executionDate === payment.executionDate &&
                    issue.payee === payment.payee &&
                    issue.payer === payment.payer
                ) {
                    isSame = true;
                }
                return isSame;
            });
            ignoredAccountingIssuesIds = _.map(ignoredAccountingIssues, '_id');
            console.log('ignoredAccountingIssuesIds = ' + JSON.stringify(ignoredAccountingIssuesIds));

            if (linkedAccountingIssuesIds.length > 1) {
                LinkedAccountingIssues.remove({'_id':{'$in':linkedAccountingIssuesIds}});
            }
            if (ignoredAccountingIssuesIds.length > 1) {
                IgnoredAccountingIssues.remove({'_id':{'$in':ignoredAccountingIssuesIds}});
            }
            Processes.update(id, {
                $unset: {
                    'accounting': []
                }
            });
        } catch (error) {
            throw new Meteor.Error('delete-accounting-error', 'error when trying to delete accounting data. error' + error);
        }
    },
    assignUserRole: function(userId) {
        if (userId) {
            var email = Meteor.users.findOne({'_id': userId}).emails[0].address;
            if (email && (email === 'israel@sweet-home.co.il' || email ==='rina@sweet-home.co.il' || email === 'gilizeevi@gmail.com')) {
                Roles.addUsersToRoles(userId, ['admin']);
                console.log('Assigned ' + Meteor.user().emails[0].address + ' to the admin Role');
            }
        }
    },
    createNewUserAccount: function(email, password) {
        try {
            Accounts.createUser({username: email, email, email, password: password});
        } catch(error) {
            throw new Meteor.Error('create-New-user-error', 'error when trying to create a new user. error' + error);
        }
    },
    promoteUser: function(id) {
        try {
            Roles.addUsersToRoles(id, ['admin']);
        } catch(error) {
            throw new Meteor.Error('promote-user-error', 'error when trying to promote a user. error' + error);
        }
    },
    demoteUser: function(id) {
        try {
            Roles.removeUsersFromRoles(id, ['admin']);
        } catch(error) {
            throw new Meteor.Error('demote-user-error', 'error when trying to demote a user. error' + error);
        }
    },
    deleteUser: function(id) {
        try {
            Meteor.users.remove(id);
        } catch(error) {
            throw new Meteor.Error('delete-user-error', 'error when trying to delete a user. error' + error);
        }
    },
    updateFolderName: function() {
        var list = Processes.find({}, {sort: {'primaryClient.firstName': 1}}).fetch();
        function updateDropboxFolderName(index) {
            var item = list[index],
                originalPath = item.primaryClient.firstName + '_' + item.primaryClient.lastName + '_' + item.realty.addressStreet + '_' + item.realty.addressNumber,
                renamedPath = originalPath + '_' + item.realty.apartmentNumber;
            console.log('item #' + index + ': ' + originalPath);
            console.log('item #' + index + ' will be renamed to ' + renamedPath);
            HTTP.post("https://api.dropboxapi.com/2/files/move", {
                headers: {
                    Authorization: "Bearer " + Meteor.settings.private.dropbox.apiKey,
                    "Content-Type": "application/json"
                },
                data: {
                    "from_path": Meteor.settings.private.dropbox.workingPath + originalPath,
                    "to_path": Meteor.settings.private.dropbox.workingPath + renamedPath
                }
            }, function(error, result) {
                if (error) {
                    console.log('error = ' + error.response.statusCode + ':' + error.response.data.error_summary);
                } else {
                    console.log('result = ' + result.statusCode + ':' + result.data.name);
                }
                if (index < list.length) {
                    updateDropboxFolderName(index+1);
                }
            });
        }
        updateDropboxFolderName(0);
        // console.log('list = ' + JSON.stringify(list));
    },
    refreshCollections: function() {
        console.log('nothing to refresh');
    },
    advanceProcess: function(id, originStep, userId) {
        console.log('userId = ' + userId);
        var doc = Processes.findOne({_id: id}),
            log = doc.log;
        if (log) {
            log.push({'index': originStep, 'date': new Date(), 'userId': userId});
        } else {
            log = [];
            log.push({'index': originStep, 'date': new Date(), 'userId': userId});
        }
        try {
            Meteor.call('playStep', id, originStep);
            Meteor.call('updateStepAndPhase', id, doc, log, originStep);
        } catch (error) {
            console.log('error when trying to advance the process. Error = ' + error);
            throw new Meteor.Error('advance-error', 'error when trying to advance the process.');
        }
    },
    playStep: function(id, step) {
        switch (step) {
            case 0:
                Meteor.call('sendClientReservation', id);
                break;
            case 1:
                Meteor.call('requestClientIdentification', id);
                break;
            case 2:
                Meteor.call('verifyClientIdentity', id);
                break;
            case 4:
                Meteor.call('sendSupplierReservation', id);
                break;
            case 5:
                Meteor.call('askForMortgageInfo', id);
                break;
            case 6:
                Meteor.call('createPayslipsAndReports', id);
                break;
            case 8:
                Meteor.call('sendMortgageApplication', id);
                break;
            case 9:
                Meteor.call('approveMortgageSum', id);
                break;
            case 10:
                Meteor.call('scheduleMortgageSignature', id);
                break;
            case 11:
                Meteor.call('sendMortgageToNotary', id);
                break;
            case 12:
                Meteor.call('sendMortgageBrokerInvoice', id);
                break;
            case 15:
                Meteor.call('assertSeparateNotaryRequirement', id);
                break;
            case 16:
                Meteor.call('suggestMortgageNotaryDates', id);
                break;
            case 17:
                Meteor.call('selectMortgageNotaryDate', id);
                break;
            case 19:
                Meteor.call('uploadEquityReceipt', id);
                break;
            case 22:
                Meteor.call('confirmReceiptOfPOA', id);
                break;
            case 24:
                Meteor.call('suggestNotaryDates', id);
                break;
            case 25:
                Meteor.call('confirmNotaryDates', id);
                break;
            case 27:
                Meteor.call('sendBrokerInvoice', id);
                break;
            case 28 :
                Meteor.call('sendEscrowDocs', id);
                break;
            case 35:
                Meteor.call('sendPaymentInstructions', id);
                break;
            case 36:
                Meteor.call('requestSupplierConfirmEquity', id);
                break;
            case 40:
                Meteor.call('sendMgmtContractAndPOA', id);
                break;
            case 41:
                Meteor.call('uploadMgmtContractAndPOA', id);
                break;
            case 43:
                Meteor.call('notifyOnOwnershipChange', id);
                break;
            case 45:
                Meteor.call('introToManagementCompany', id);
                break;
            case 48:
                Meteor.call('notifyOnRentalsToBePayed', id);
                break;
            case 49:
                Meteor.call('informFinanzamt', id);
                break;
        }
    },
    updateStepAndPhase: function(id, doc, log, originStep) {
        var nextStep, currentPhase;
        console.log('originStep = ' + originStep);
        nextStep = parseInt(originStep) + 1;
        if (originStep > Constants.phaseCap[0] && originStep < Constants.phaseCap[1]) {
            if (nextStep === Constants.phaseCap[1]) {
                currentPhase = doc.phaseB;
                Processes.update(id, {
                    $set: {
                        stepB: 52,
                        phaseB: currentPhase.toString(),
                        log: log,
                        lastStepBAdvancedAt: new Date(),
                        isBOverdue: false
                    }
                });
            } else {
                currentPhase = doc.phaseB;
                Processes.update(id, {
                    $set: {
                        stepB: nextStep.toString(),
                        phaseB: currentPhase.toString(),
                        log: log,
                        lastStepBAdvancedAt: new Date(),
                        isBOverdue: false
                    }
                });
            }
        } else {
            currentPhase = doc.phase;
            if (nextStep > Constants.phaseCap[currentPhase]) {
                currentPhase = parseInt(currentPhase)+1;
            }
            Processes.update(id, {
                $set: {
                    step: nextStep.toString(),
                    phase: currentPhase.toString(),
                    log: log,
                    lastStepAAdvancedAt: new Date(),
                    isAOverdue: false
                }
            });
        }
    },
    sendClientReservation: function(id) {
        console.log('sendClientReservation');
        var vat = Configurations.findOne().germanyVat,
            ref,
            doc = Processes.findOne({_id: id}),
            config = Configurations.findOne();
            client1Address = doc.primaryClient.addressStreet + ' ' + doc.primaryClient.addressNumber + ', ' + doc.primaryClient.postcode + ' ' + doc.primaryClient.town + ', ' + doc.primaryClient.country + ' ',
            jsondata = {
                Include_client_1_in_reservation: '1',
                client1_full_name: doc.primaryClient.firstName + ' ' + doc.primaryClient.lastName,
                client1_id_number: doc.primaryClient.idNumber,
                client1_birthdate: doc.primaryClient.birthDate,
                client1_address: client1Address,
                client1_phone: doc.primaryClient.phone,
                first_name1: doc.primaryClient.firstName,
                last_name1: doc.primaryClient.lastName,
                email1: doc.primaryClient.email,
                hebrew_full_name_1: doc.primaryClient.hebrewName,
                property_address_street: doc.realty.addressStreet,
                property_address_number: doc.realty.addressNumber,
                property_apartment_number: doc.realty.apartmentNumber,
                property_address: doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ', ' + doc.realty.postcode + ' ' + doc.realty.town,
                property_floor: doc.realty.floor,
                property_number: doc.realty.apartmentNumber,
                property_price: doc.realty.price,
                property_size: doc.realty.size,
                land_tax: doc.realty.landTax,
                notary_fee: doc.realty.notaryFee,
                registration_fee: doc.realty.registrationFee,
                escrow_fee: doc.realty.escrowFee,
                GERMANY_VAT: vat,
                mortgage_broker_fee: doc.realty.mortgageBrokerFee,
                broker_fee: doc.realty.serviceFee,
                reservation_deposit: doc.realty.reservationDeposit,
                procces_is_online: '1',
                Number_of_clients: 1
            };
        if (doc.isOnline) {
            jsondata.procces_is_online = '1'
        } else {
            jsondata.procces_is_online = '0'
        }
        if (doc.includeClient2) {
            _.extend(jsondata, {
                Include_client_2_in_reservation: '1',
                client2_full_name: doc.client2.firstName + ' ' + doc.client2.lastName,
                client2_id_number: doc.client2.idNumber,
                client2_birthdate: doc.client2.birthDate,
                client2_address: doc.client2.addressStreet + ' ' + doc.client2.addressNumber + ', ' + doc.client2.postcode + ' ' + doc.client2.town + ', ' + doc.client2.country + ' ',
                client2_phone: doc.client2.phone,
                first_name2: doc.client2.firstName,
                last_name2: doc.client2.lastName,
                email2: doc.client2.email,
                hebrew_full_name_2: doc.client2.hebrewName,
                Number_of_clients: 2
            });
        }
        if (doc.includeClient3) {
            _.extend(jsondata, {
                Include_client_3_in_reservation: '1',
                client3_full_name: doc.client3.firstName + ' ' + doc.client2.lastName,
                client3_id_number: doc.client3.idNumber,
                client3_birthdate: doc.client3.birthDate,
                client3_address: doc.client3.addressStreet + ' ' + doc.client3.addressNumber + ', ' + doc.client3.postcode + ' ' + doc.client3.town + ', ' + doc.client3.country + ' ',
                client3_phone: doc.client3.phone,
                first_name3: doc.client3.firstName,
                last_name3: doc.client3.lastName,
                email3: doc.client3.email,
                hebrew_full_name_3: doc.client3.hebrewName,
                Number_of_clients: 3
            });
        }
        // console.log('jsondata = ' + JSON.stringify(jsondata));
        if (config.isTesting) {
            jsondata.email1 = config.testingEmail;
            jsondata.email2 = config.testingEmail;
            jsondata,email3 = config.testingEmail;
            jsondata.isTesting = '1';
        }
        if (doc.isOnline) {
            HTTP.post(Meteor.settings.private.webmergeDocPath.clientReservation, {
                headers: {
                    Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                },
                data: jsondata
            });
        } else {
            HTTP.post(Meteor.settings.private.webmergeDocPath.clientReservation + '?test=1', {
                headers: {
                    Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                },
                data: jsondata
            });
        }
        ref = generateRefNumber(doc);

        Processes.update(doc._id, {
            $set: {
                'realty.referenceNumber': ref
            },
        });
    },
    requestClientIdentification: function(id) {
        console.log('requestClientIdentification');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            recipients = [{"Email": doc.primaryClient.email}];
        if (doc.includeClient2) {
            recipients.push({"Email": doc.client2.email});
        }
        if (doc.includeClient3) {
            recipients.push({"Email": doc.client3.email});
        }
        subject = Constants.emails[1].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        SSR.compileTemplate('Step1Email', Assets.getText('emails/Step1Email.html'));
        if (doc.includeClient2 && doc.includeClient3) {
            html = SSR.render("Step1Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                client2HebrewName: doc.client2.hebrewName,
                client3HebrewName: doc.client3.hebrewName,
                includeClient2: true,
                includeClient3: true
            });
        } else if (doc.includeClient2) {
            html = SSR.render("Step1Email", {
            client1HebrewName: doc.primaryClient.hebrewName,
            client2HebrewName: doc.client2.hebrewName,
            includeClient2: true,
        });
        } else {
            html = SSR.render("Step1Email", {
                client1HebrewName: doc.primaryClient.hebrewName
            });
        }
        Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);

    },
    sendEmail: function(recipients, sender, subject, body, isOnline) {
        var config = Configurations.findOne();
        if (!isOnline) {
            console.log('this process is offline, not sending any emails');
            return;
        } else if (config.isTesting) {
            recipients = [];
            recipients.push({"Email":config.testingEmail});
            console.log('Testing, testingEmail2 = ' + JSON.stringify(recipients));
        }
        if (config.bcc) {
            recipients.push({"Email":config.bcc});
        }
        if (config.bcc2) {
            recipients.push({"Email":config.bcc2});
        }
        HTTP.post(Meteor.settings.private.mailjet.sendAPI, {
            auth: Meteor.settings.private.mailjet.key + ':' + Meteor.settings.private.mailjet.secret,
            data: {
                "FromEmail": sender.email,
                "FromName": sender.name,
                "Subject": subject,
                "Html-part": body,
                "Recipients": recipients
            }
        },function(result, error){
            if (error) {
                console.log(error);
            } else {
                console.log(result);
            }
        });
    },
    saveUrlToDropbox: function(data) {
        var interval,
            idx = 0,
            config = Configurations.findOne();
        if (config.isTesting) {
            console.log('saving to dropbox is not possible while in testing mode');
        }
            HTTP.post("https://api.dropboxapi.com/2/files/save_url", {
            headers: {
                Authorization: "Bearer " + Meteor.settings.private.dropbox.apiKey,
                "Content-Type": "application/json"
            },
            data: {
                "path": data.path,
                "url": data.url
            }
        }, function(error, result){
            // console.log('result = ' + JSON.stringify(result));
            if (error) {
                console.log('dropbox upload error = ' + error);
                Meteor.call('saveUrlToDropbox', data);
            } else {
                interval = Meteor.setInterval(function(){
                    HTTP.post("https://api.dropboxapi.com/2/files/save_url/check_job_status", {
                        headers: {
                            Authorization: "Bearer " + Meteor.settings.private.dropbox.apiKey,
                            "Content-Type": "application/json"
                        },
                        data: {
                            "async_job_id": result.data.async_job_id
                        }
                    }, function(error, result){
                        if (error) {
                            console.log('dropbox upload error = ' + error);
                            Meteor.clearInterval(interval);
                        } else {
                            if (result && result.data['.tag'] === 'complete') {
                                console.log('upload is done');
                                Meteor.clearInterval(interval);
                                if (data.successCallback) {
                                    // console.log('result = ' + JSON.stringify(result));
                                    data.successCallback();
                                }
                            } else if (idx === 180) {
                                console.log('too long, ' + JSON.stringify(result));
                                Meteor.clearInterval(interval);
                            } else {
                                idx += 1;
                                console.log('idx = ' + idx);
                            }
                        }
                    });
                }, 1000);
            }
        });
    },
    getDropboxSharedLink: function(data) {
        console.log('getDropboxSharedLink, data = ' + JSON.stringify(data));
        var settings;
        if (data.password) {
            settings = {
                "requested_visibility":{".tag":"password"},
                "link_password": data.password
            };
        } else {
            settings = {
                "requested_visibility":"public"
            };
        }
        HTTP.post('https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings', {
            headers: {
                Authorization: "Bearer " + Meteor.settings.private.dropbox.apiKey,
                "Content-Type": "application/json"
            },
            data: {
                "path": data.path,
                "settings": settings
            }
        }, function(error, result) {
            if (error) {
                // console.log('error.response.data.error_summary = ' + error.response.data.error_summary);
                if (error.response.data.error_summary.includes('shared_link_already_exists')) {
                    HTTP.post('https://api.dropboxapi.com/2/sharing/list_shared_links', {
                        headers: {
                            Authorization: "Bearer " + Meteor.settings.private.dropbox.apiKey,
                            "Content-Type": "application/json"
                        },
                        data: {
                            "path": data.path,
                            "direct_only": true
                        }
                    }, function(error, result) {
                        if (error) {
                            console.error('getDropboxSharedLink, list_shared_links, ERROR: ' + JSON.stringify(error));
                        } else if (result) {
                            // console.log('getDropboxSharedLink, result: ' + JSON.stringify(result));
                            if (result.data.links[0].url) {
                                HTTP.post('https://api.dropboxapi.com/2/sharing/modify_shared_link_settings', {
                                        headers: {
                                            Authorization: "Bearer " + Meteor.settings.private.dropbox.apiKey,
                                                "Content-Type": "application/json"
                                        },
                                        data: {
                                            "url": result.data.links[0].url,
                                            "settings": settings
                                        }
                                    }, function(error, result) {
                                    if (error) {
                                        console.error('getDropboxSharedLink, modify_shared_link_settings, ERROR: ' + JSON.stringify(error));
                                    } else {
                                        if (result.data.url) {
                                            data.callback(result.data.url);
                                        } else {
                                            console.warn('On getDropboxSharedLink, expecting result.data.url but instead got = ' + JSON.stringify(result));
                                        }
                                    }
                                });
                            } else {
                                console.warn('On getDropboxSharedLink, expecting result.data.links[0].url but instead got = ' + JSON.stringify(result));
                            }                        }
                    });
                } else {
                    console.error('getDropboxSharedLink, create_shared_link_with_settings, ERROR: ' + JSON.stringify(error));
                }
            } else if (result){
                if (result.data.url) {
                    data.callback(result.data.url);
                } else {
                    console.warn('On getDropboxSharedLink, expecting result.data.url but instead got = ' + JSON.stringify(result));
                }
            }
        });
    },
    verifyClientIdentity: function(id) {
        console.log('verifyClientIdentify');
        var doc = Processes.findOne({_id: id}),
            path = Meteor.settings.private.dropbox.workingPath + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' +doc.realty.apartmentNumber + '/';
        Meteor.call('saveUrlToDropbox', {
            url: doc.primaryClient.identification.passportScanPath,
            path: path + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.primaryClient.identification.passportScanFileName,
            id: id
        });
        console.log('saveUrlToDropbox ' + doc.primaryClient.identification.passportScanFileName);
        Meteor.call('saveUrlToDropbox', {
            url: doc.primaryClient.identification.signaturePath,
            path: path + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.primaryClient.identification.signatureFileName,
            id: id
        });
        console.log('saveUrlToDropbox ' + doc.primaryClient.identification.signaturePath);
        if (doc.includeClient2) {
            Meteor.call('saveUrlToDropbox', {
                url: doc.client2.identification.passportScanPath,
                path: path + doc.client2.firstName + '_' + doc.client2.lastName + '_' + doc.client2.identification.passportScanFileName,
                id: id
            });
            console.log('saveUrlToDropbox ' + doc.client2.identification.passportScanFileName);
            Meteor.call('saveUrlToDropbox', {
                url: doc.client2.identification.signaturePath,
                path: path + doc.client2.firstName + '_' +  doc.client2.lastName + '_' + doc.client2.identification.signatureFileName,
                id: id
            });
            console.log('saveUrlToDropbox ' + doc.client2.identification.signatureFileName);
        }
        if (doc.includeClient3) {
            Meteor.call('saveUrlToDropbox', {
                url: doc.client3.identification.passportScanPath,
                path: path + doc.client3.firstName + '_' + doc.client3.lastName + '_' + doc.client3.identification.passportScanFileName,
                id: id
            });
            console.log('saveUrlToDropbox ' + doc.client3.identification.passportScanFileName);
            Meteor.call('saveUrlToDropbox', {
                url: doc.client3.identification.signaturePath,
                path: path + doc.client3.firstName + '_' + doc.client3.lastName + '_' + doc.client3.identification.signatureFileName,
                id: id
            });
            console.log('saveUrlToDropbox ' + doc.client3.identification.signatureFileName);
        }
    },
    sendSupplierReservation: function(id) {
        console.log('sendSupplierReservation');
        var doc = Processes.findOne({_id: id}),
            config = Configurations.findOne(),
            paths = [],
            client1Data,
            client2Data,
            client3Data,
            date = new Date(),
            jsondata = {
                password: Random.hexString(5),
                property_apt_number: doc.realty.apartmentNumber,
                property_location: doc.realty.location,
                property_status: doc.realty.status,
                property_price: doc.realty.price,
                property_size: doc.realty.size,
                date: date.toDateString(),
                property_full_address: doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ' ' + doc.realty.postcode + ' ' + doc.realty.town,
                procces_is_online: '1',
                property_address_street: doc.realty.addressStreet,
                property_address_number: doc.realty.addressNumber,
                property_apartment_number: doc.realty.apartmentNumber,
                property_address_street_number: doc.realty.addressNumber,
                property_number: doc.realty.apartmentNumber,
                notary_email: getNotaryEmail(doc.realty.notary),
                supplier_email: getSupplierEmail(doc.realty.supplier),
                first_name1: doc.primaryClient.firstName,
                last_name1: doc.primaryClient.lastName
            };
        if (doc.supplierReservation) {
            if (doc.supplierReservation.includeClient1) {
                paths.push({
                            id: 1,
                            path: Meteor.settings.private.dropbox.workingPath + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' + doc.realty.apartmentNumber + '/' + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.primaryClient.identification.passportScanFileName
                        });
                _.extend(jsondata, {
                    Include_client_1_in_supplier_reservation: '1',
                    client_1_full_name: doc.primaryClient.firstName + ' ' + doc.primaryClient.lastName,
                    client_1_birthdate: doc.primaryClient.birthDate,
                    client_1_birthplace: doc.primaryClient.mainNationality,
                    client_1_address_street: doc.primaryClient.addressStreet,
                    client_1_address_number: doc.primaryClient.addressNumber,
                    client_1_address_country: doc.primaryClient.country,
                    client_1_address_postcode: doc.primaryClient.postcode,
                    client_1_address_city: doc.primaryClient.town,
                    client_1_passport_number: doc.primaryClient.identification.passportNumber,
                    client_1_phone: doc.primaryClient.phone,
                    client_1_email: doc.primaryClient.email,
                });
            }
            if (doc.includeClient2 && doc.supplierReservation.includeClient2) {
                paths.push({
                            id: 2,
                            path: Meteor.settings.private.dropbox.workingPath + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' + doc.realty.apartmentNumber + '/' + doc.client2.firstName + '_' + doc.client2.lastName + '_' + doc.client2.identification.passportScanFileName
                        });
                _.extend(jsondata, {
                    Include_client_2_in_supplier_reservation: '1',
                    client_2_full_name: doc.client2.firstName + ' ' + doc.client2.lastName,
                    client_2_birthdate: doc.client2.birthDate,
                    client_2_birthplace: doc.client2.mainNationality,
                    client_2_address_street: doc.client2.addressStreet,
                    client_2_address_number: doc.client2.addressNumber,
                    client_2_address_country: doc.client2.country,
                    client_2_address_postcode: doc.client2.postcode,
                    client_2_address_city: doc.client2.town,
                    client_2_passport_number: doc.client2.identification.passportNumber,
                    client_2_phone: doc.client2.phone,
                    client_2_email: doc.client2.email,
                });
            }
            if (doc.includeClient3 && doc.supplierReservation.includeClient3) {
                paths.push({id: 3,
                            path:Meteor.settings.private.dropbox.workingPath + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' + doc.realty.apartmentNumber + '/' + doc.client3.firstName + '_' + doc.client3.lastName + '_' + doc.client3.identification.passportScanFileName
                        });
                _.extend(jsondata, {
                    Include_client_3_in_supplier_reservation: '1',
                    client_3_full_name: doc.client3.firstName + ' ' + doc.client3.lastName,
                    client_3_birthdate: doc.client3.birthDate,
                    client_3_birthplace: doc.client3.mainNationality,
                    client_3_address_street: doc.client3.addressStreet,
                    client_3_address_number: doc.client3.addressNumber,
                    client_3_address_country: doc.client3.country,
                    client_3_address_postcode: doc.client3.postcode,
                    client_3_address_city: doc.client3.town,
                    client_3_passport_number: doc.client3.identification.passportNumber,
                    client_3_phone: doc.client3.phone,
                    client_3_email: doc.client3.email,
                });
            }
        }
        if (doc.isOnline) {
            jsondata.procces_is_online = '1'
        } else {
            jsondata.procces_is_online = '0'
        }
        if (config.isTesting) {
            jsondata.email1 = config.testingEmail;
            jsondata.email2 = config.testingEmail;
            jsondata.email3 = config.testingEmail;
            jsondata.isTesting = '1';
        }

        if (doc.supplierReservation.includeClient1) {
            client1Data = {
                path: _.find(paths, function(path) {
                    return path.id === 1;
                }).path,
                password: jsondata.password,
                callback: function(url){
                    jsondata['client_1_dropboxURL'] = url;
                    paths.pop();
                    if (paths.length === 0) {
                        Meteor.call('mergeSupplierReservation', jsondata)
                    }
                }
            };
            Meteor.call('getDropboxSharedLink', client1Data);
        }
        if (doc.supplierReservation.includeClient2) {
            client2Data = {
                path: _.find(paths, function(path) {
                    return path.id === 2;
                }).path,
                password: jsondata.password,
                callback: function(url){
                    jsondata['client_2_dropboxURL'] = url;
                    paths.pop();
                    if (paths.length === 0) {
                        Meteor.call('mergeSupplierReservation', jsondata)
                    }
                }
            };
            Meteor.call('getDropboxSharedLink', client2Data);
        }
        if (doc.supplierReservation.includeClient3) {
            client3Data = {
                path: _.find(paths, function(path) {
                    return path.id === 3;
                }).path,
                password: jsondata.password,
                callback: function(url){
                    jsondata['client_3_dropboxURL'] = url;
                    paths.pop();
                    if (paths.length === 0) {
                        Meteor.call('mergeSupplierReservation', jsondata)
                    }
                }
            };
            Meteor.call('getDropboxSharedLink', client3Data);
        }
    },

    mergeSupplierReservation(jsondata) {
        console.log('mergeSupplierReservation');
        if (jsondata.procces_is_online === '1') {
            HTTP.post(Meteor.settings.private.webmergeDocPath.supplierReservation, {
                headers: {
                    Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                },
                data: jsondata
            });
        } else {
            HTTP.post(Meteor.settings.private.webmergeDocPath.supplierReservation + '?test=1', {
                headers: {
                    Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                },
                data: jsondata
            });
        }
    },
    askForMortgageInfo: function(id) {
        console.log('askForMortgageInfo');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            recipients = [{"Email": doc.primaryClient.email}];
        subject = Constants.emails[2].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        if (doc.mortgageRequirement.includeClient1) {
            if (doc.primaryClient.professionType === 'employed') {
                SSR.compileTemplate('Step5Email', Assets.getText('emails/Step5Email_employee.html'));
            } else {
                SSR.compileTemplate('Step5Email', Assets.getText('emails/Step5Email_indie.html'));
            }
            html = SSR.render("Step5Email", {
                hebrewName: doc.primaryClient.hebrewName
            });
            Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
        }
        if (doc.mortgageRequirement.includeClient2) {
            recipients = [{"Email": doc.client2.email}];
            if (doc.client2.professionType === 'employed') {
                SSR.compileTemplate('Step5Email', Assets.getText('emails/Step5Email_employee.html'));
            } else {
                SSR.compileTemplate('Step5Email', Assets.getText('emails/Step5Email_indie.html'));
            }
            html = SSR.render("Step5Email", {
                hebrewName: doc.client2.hebrewName
            });
            Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
        }
        if (doc.mortgageRequirement.includeClient3) {
            recipients = [{"Email": doc.client3.email}];
            if (doc.client3.professionType === 'employed') {
                SSR.compileTemplate('Step5Email', Assets.getText('emails/Step5Email_employee.html'));
            } else {
                SSR.compileTemplate('Step5Email', Assets.getText('emails/Step5Email_indie.html'));
            }
            html = SSR.render("Step5Email", {
                hebrewName: doc.client3.hebrewName
            });
            Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
        }
    },
    createPayslipsAndReports: function(id) {
        console.log('createPayslipsAndReports');
        var doc = Processes.findOne({_id: id}),
            clientFolder = doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' + doc.realty.apartmentNumber,
            brokerId = doc.mortgageRequest.mortgageBroker,
            config = Configurations.findOne();
        if (doc.mortgageRequirement.includeClient1) {
            if (doc.primaryClient.professionType === 'employed') {
                createPayslips(doc.primaryClient, clientFolder, brokerId, doc, config.isTesting);
                create106Reports(doc.primaryClient, clientFolder, brokerId, doc, config.isTesting);
            } else {
                createSelfEmployedReport(doc.primaryClient, clientFolder, brokerId, doc, config.isTesting);
            }
        }
        if (doc.mortgageRequirement.includeClient2) {
            if (doc.client2.professionType === 'employed') {
                createPayslips(doc.client2, clientFolder, brokerId, doc, config.isTesting);
                create106Reports(doc.client2, clientFolder, brokerId, doc, config.isTesting);
            } else {
                createSelfEmployedReport(doc.client2, clientFolder, brokerId, doc, config.isTesting);
            }
        }
        if (doc.mortgageRequirement.includeClient3) {
            if (doc.client3.professionType === 'employed') {
                createPayslips(doc.client3, clientFolder, brokerId, doc, config.isTesting);
                create106Reports(doc.client3, clientFolder, brokerId, doc, config.isTesting);
            } else {
                createSelfEmployedReport(doc.client3, clientFolder, brokerId, doc, config.isTesting);
            }
        }
    },
    sendMortgageApplication: function(id) {
        console.log('sendMortgageApplication');
        var doc = Processes.findOne({_id: id}),
            clients = [],
            signatures = [],
            mortgageRequest = doc.mortgageRequest,
            propertyInfo = doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
            config = Configurations.findOne(),
            clientFolder = doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' + doc.realty.apartmentNumber;


        if (doc.mortgageRequirement.includeClient1) {
            clients.push(doc.primaryClient);
        }
        if (doc.mortgageRequirement.includeClient2) {
            clients.push(doc.client2);
        }
        if (doc.mortgageRequirement.includeClient3) {
            clients.push(doc.client3);
        }

        fetchClientSignature(clients[0], clientFolder).then(function onSuccess(result){
            signatures.push(result);
            if (clients[1]) {
                fetchClientSignature(clients[1], clientFolder).then(function onSuccess(result){
                    signatures.push(result);
                    if (clients[2]) {
                        fetchClientSignature(clients[2], clientFolder).then(function onSuccess(result){
                            signatures.push(result);
                            sendMortgageApplicationRoute(clients, signatures, mortgageRequest, clientFolder, propertyInfo, doc.isOnline, config);
                        }).catch(function onFailure(err) {
                            console.error(err);
                        });
                    } else {
                        sendMortgageApplicationRoute(clients, signatures, mortgageRequest, clientFolder, propertyInfo, doc.isOnline, config);
                    }
                }).catch(function onFailure(err) {
                    console.error(err);
                });
            } else {
                sendMortgageApplicationRoute(clients, signatures, mortgageRequest, clientFolder, propertyInfo, doc.isOnline, config);
            }
        }).catch(function onFailure(err) {
            console.error(err);
        });
    },
    approveMortgageSum: function(id) {
        console.log('approveMortgageSum');
        var doc = Processes.findOne({_id: id}),
            html,
            broker = MortgageBrokers.findOne({_id: doc.mortgageRequest.mortgageBroker}),
            recipients = [{"Email": broker.email}],
            subject = Constants.emails[3].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        SSR.compileTemplate('Step9Email', Assets.getText('emails/Step9Email.html'));
        html = SSR.render("Step9Email", {
            sum: doc.mortgageRequest.mortgageApprovedSum,
            primaryClientName: doc.primaryClient.firstName + ' ' + doc.primaryClient.lastName,
            propertAddress: doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
            brokerName: broker.displayName
        });
        Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
    },
    scheduleMortgageSignature: function(id) {
        console.log('scheduleMortgageSignature');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            time = moment(doc.mortgageRequest.clientSignatureAppointment).format('MMMM Do YYYY, h:mm:ss a'),
            recipients = [{"Email": doc.primaryClient.email}];
        if (doc.includeClient2) {
            recipients.push({"Email": doc.client2.email});
        }
        if (doc.includeClient3) {
            recipients.push({"Email": doc.client3.email});
        }
        subject = Constants.emails[4].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        SSR.compileTemplate('Step10Email', Assets.getText('emails/Step10Email.html'));
        if (doc.includeClient2 && doc.includeClient3) {
            html = SSR.render("Step10Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                client2HebrewName: doc.client2.hebrewName,
                client3HebrewName: doc.client3.hebrewName,
                includeClient2: true,
                includeClient3: true,
                date: time
            });
        } else if (doc.includeClient2) {
            html = SSR.render("Step10Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                client2HebrewName: doc.client2.hebrewName,
                includeClient2: true,
                date: time
            });
        } else {
            html = SSR.render("Step10Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                date: time
            });
        }
        Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
    },

    sendMortgageBrokerInvoice: function(id) {
        console.log('scheduleMortgageSignature');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            propertyPrice = doc.realty.price,
            brokerFee = doc.realty.mortgageBrokerFee,
            vat = Configurations.findOne().germanyVat,
            calculatedFee = ((propertyPrice * (brokerFee/100)) + ((propertyPrice * (brokerFee/100)) * (vat/100))),
            recipients = [{"Email": doc.primaryClient.email}];
        if (doc.includeClient2) {
            recipients.push({"Email": doc.client2.email});
        }
        if (doc.includeClient3) {
            recipients.push({"Email": doc.client3.email});
        }
        subject = Constants.emails[5].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        SSR.compileTemplate('Step12Email', Assets.getText('emails/Step12Email.html'));
        if (doc.includeClient2 && doc.includeClient3) {
            html = SSR.render("Step12Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                client2HebrewName: doc.client2.hebrewName,
                client3HebrewName: doc.client3.hebrewName,
                includeClient2: true,
                includeClient3: true,
                propertyPrice: Utils.formatCurrencies(propertyPrice),
                brokerFee: Utils.formatCurrencies(brokerFee),
                vat: vat,
                calculatedFee: Utils.formatCurrencies(calculatedFee)
            });
        } else if (doc.includeClient2) {
            html = SSR.render("Step12Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                client2HebrewName: doc.client2.hebrewName,
                includeClient2: true,
                propertyPrice: Utils.formatCurrencies(propertyPrice),
                brokerFee: Utils.formatCurrencies(brokerFee),
                vat: vat,
                calculatedFee: Utils.formatCurrencies(calculatedFee)
            });
        } else {
            html = SSR.render("Step12Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                propertyPrice: Utils.formatCurrencies(propertyPrice),
                brokerFee: Utils.formatCurrencies(brokerFee),
                vat: vat,
                calculatedFee: Utils.formatCurrencies(calculatedFee)
            });
        }
        Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
    },

    confirmReceiptOfPOA: function(id) {
        console.log('confirmReceiptOfPOA');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            recipients = [{"Email": doc.primaryClient.email}];
        if (doc.includeClient2) {
            recipients.push({"Email": doc.client2.email});
        }
        if (doc.includeClient3) {
            recipients.push({"Email": doc.client3.email});
        }
        subject = Constants.emails[6].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        SSR.compileTemplate('Step22Email', Assets.getText('emails/Step22Email.html'));
        if (doc.includeClient2 && doc.includeClient3) {
            html = SSR.render("Step22Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                client2HebrewName: doc.client2.hebrewName,
                client3HebrewName: doc.client3.hebrewName,
                includeClient2: true,
                includeClient3: true,
            });
        } else if (doc.includeClient2) {
            html = SSR.render("Step22Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                client2HebrewName: doc.client2.hebrewName,
                includeClient2: true,
            });
        } else {
            html = SSR.render("Step22Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
            });
        }
        Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
    },

    suggestNotaryDates: function(id) {
        console.log('suggestNotaryDates');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            recipients = [{"Email": getSupplierEmail(doc.realty.supplier)}, {"Email": getNotaryEmail(doc.realty.notary)}];
        subject = Constants.emails[7].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        SSR.compileTemplate('Step24Email', Assets.getText('emails/Step24Email.html'));
        html = SSR.render("Step24Email", {
            name: getSupplierName(doc.realty.supplier),
            clientName: doc.primaryClient.lastName + ' ' + doc.primaryClient.firstName,
            propertyField: doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
            dates: [moment(doc.notary.proposedDate1).tz("Europe/Berlin").format('dddd D MMMM YYYY, HH:mm'), moment(doc.notary.proposedDate2).tz("Europe/Berlin").format('dddd D MMMM YYYY, HH:mm'), moment(doc.notary.proposedDate3).tz("Europe/Berlin").format('dddd D MMMM YYYY, HH:mm')]
        });
        Meteor.call('sendEmail', recipients, Constants.senders.suppliers, subject, html, doc.isOnline);
    },

    confirmNotaryDates: function(id) {
        console.log('confirmNotaryDates');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            recipients = [{"Email": getSupplierEmail(doc.realty.supplier)}, {"Email": getNotaryEmail(doc.realty.notary)}];
        subject = Constants.emails[8].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        SSR.compileTemplate('Step25Email', Assets.getText('emails/Step25Email.html'));
        html = SSR.render("Step25Email", {
            name: getSupplierName(doc.realty.supplier),
            clientName: doc.primaryClient.lastName + ' ' + doc.primaryClient.firstName,
            propertyField: doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
            date: moment(doc.notary.finalDate).tz("Europe/Berlin").format('dddd D MMMM YYYY, HH:mm')
        });
        Meteor.call('sendEmail', recipients, Constants.senders.suppliers, subject, html, doc.isOnline);
    },

    sendBrokerInvoice: function(id) {
        console.log('sendBrokerInvoice');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            propertyPrice = doc.realty.price,
            brokerFee = doc.realty.serviceFee,
            vat = Configurations.findOne().israelVat,
            conversionRate = doc.notary.conversionRate,
            deposit = doc.realty.reservationDeposit,
            calculatedFee = ((propertyPrice * (brokerFee/100)) + ((propertyPrice * (brokerFee/100)) * (vat/100))),
            calculatedFeeClean = calculatedFee - deposit,
            calculatedFeeConverted = calculatedFeeClean * conversionRate,
            recipients = [{"Email": doc.primaryClient.email}];
        console.log();
        if (doc.includeClient2) {
            recipients.push({"Email": doc.client2.email});
        }
        if (doc.includeClient3) {
            recipients.push({"Email": doc.client3.email});
        }
        subject = Constants.emails[9].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        SSR.compileTemplate('Step27Email', Assets.getText('emails/Step27Email.html'));
        if (doc.includeClient2 && doc.includeClient3) {
            html = SSR.render("Step27Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                client2HebrewName: doc.client2.hebrewName,
                client3HebrewName: doc.client3.hebrewName,
                includeClient2: true,
                includeClient3: true,
                propertyPrice: Utils.formatCurrencies(propertyPrice),
                brokerFee: brokerFee,
                vat: vat,
                calculatedFee: Utils.formatCurrencies(calculatedFee),
                conversionRate: conversionRate,
                calculatedFeeConverted: Utils.formatCurrencies(calculatedFeeConverted),
                calculatedFeeClean: Utils.formatCurrencies(calculatedFeeClean),
                deposit: Utils.formatCurrencies(deposit)
            });
        } else if (doc.includeClient2) {
            html = SSR.render("Step27Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                client2HebrewName: doc.client2.hebrewName,
                includeClient2: true,
                propertyPrice: Utils.formatCurrencies(propertyPrice),
                brokerFee: brokerFee,
                vat: vat,
                calculatedFee: Utils.formatCurrencies(calculatedFee),
                conversionRate: conversionRate,
                calculatedFeeConverted: Utils.formatCurrencies(calculatedFeeConverted),
                calculatedFeeClean: Utils.formatCurrencies(calculatedFeeClean),
                deposit: Utils.formatCurrencies(deposit)

            });
        } else {
            html = SSR.render("Step27Email", {
                client1HebrewName: doc.primaryClient.hebrewName,
                propertyPrice: Utils.formatCurrencies(propertyPrice),
                brokerFee: brokerFee,
                vat: vat,
                calculatedFee: Utils.formatCurrencies(calculatedFee),
                conversionRate: conversionRate,
                calculatedFeeConverted: Utils.formatCurrencies(calculatedFeeConverted),
                calculatedFeeClean: Utils.formatCurrencies(calculatedFeeClean),
                deposit: Utils.formatCurrencies(deposit)
            });
        }
        Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
    },
    sendEscrowDocs: function(id) {
        console.log('sendEscrowDocs');
        var jsondata,
            doc = Processes.findOne({_id: id}),
            additionalCosts,
            landTaxValue,
            notaryFeeValue,
            registrationFeeValue,
            escrowFeeValue,
            contractStartDate,
            client1,
            client2,
            client3,
            registrationFee,
            escrowFee,
            configuration = Configurations.findOne(),
            propertyPrice = doc.realty.price,
            vat = configuration.germanyVat;

        if (!doc.realty.registrationFee) {
            registrationFee = configuration.registrationFee;
            escrowFee = configuration.escrowFee;
        } else {
            registrationFee = doc.realty.registrationFee;
            escrowFee = doc.realty.escrowFee;
        }


        contractStartDate = moment().format('MMMM Do YYYY');
        landTaxValue = propertyPrice * (doc.realty.landTax/100);
        notaryFeeValue = (propertyPrice * (doc.realty.notaryFee/100)) + ((propertyPrice * (doc.realty.notaryFee/100)) * (vat/100));
        registrationFeeValue = propertyPrice * (registrationFee/100);
        escrowFeeValue = (propertyPrice * (escrowFee/100)) + ((propertyPrice * (escrowFee/100)) * (vat/100));
        additionalCosts = landTaxValue + notaryFeeValue + registrationFeeValue + escrowFeeValue;

        jsondata = {
            'realty_full_address': doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ' ' + doc.realty.apartmentNumber + ' ' + doc.realty.town,
            'sale_price': propertyPrice,
            'additional_costs': additionalCosts,
            'land_tax': doc.realty.landTax,
            'land_tax_value': landTaxValue,
            'notary_fee': doc.realty.notaryFee,
            'notary_fee_value': notaryFeeValue,
            'registration_fee': registrationFee,
            'registration_fee_value': registrationFeeValue,
            'escrow_fee': escrowFee,
            'escrow_fee_value': escrowFeeValue,
            'contract_start_date': contractStartDate,
            'vat': vat,
            'first_name': doc.primaryClient.firstName,
            'last_name': doc.primaryClient.lastName,
            'property_address_street': doc.realty.addressStreet,
            'property_address_number': doc.realty.addressNumber,
            'property_apartment_number': doc.realty.apartmentNumber
        };



        if (doc.supplierReservation.includeClient1) {
            client1 = doc.primaryClient;
            jsondata.client_1_full_name = client1.firstName + ' ' + client1.lastName;
            jsondata.client_1_full_address =  client1.addressStreet + ' ' + client1.addressNumber + ', ' + client1.postcode + ' ' + client1.town + ', ' + client1.country + ' ';
            jsondata.Include_client_1_in_supplier_reservation = '1';
            if (configuration.isTesting) {
                jsondata.client_1_email = configuration.testingEmail;
            } else {
                jsondata.client_1_email = client1.email;
            }
        }
        if (doc.supplierReservation.includeClient2) {
            client2 = doc.client2;
            jsondata.client_2_full_name = client2.firstName + ' ' + client2.lastName;
            jsondata.client_2_full_address =  client2.addressStreet + ' ' + client2.addressNumber + ', ' + client2.postcode + ' ' + client2.town + ', ' + client2.country + ' ';
            jsondata.Include_client_2_in_supplier_reservation = '1';
            if (configuration.isTesting) {
                jsondata.client_2_email = configuration.testingEmail;
            } else {
                jsondata.client_2_email = client2.email;
            }
        }
        if (doc.supplierReservation.includeClient3) {
            client3 = doc.client3;
            jsondata.client_3_full_name = client3.firstName + ' ' + client3.lastName;
            jsondata.client_3_full_address =  client3.addressStreet + ' ' + client3.addressNumber + ', ' + client3.postcode + ' ' + client3.town + ', ' + client3.country + ' ';
            jsondata.Include_client_3_in_supplier_reservation = '1';
            if (configuration.isTesting) {
                jsondata.client_3_email = configuration.testingEmail;
            } else {
                jsondata.client_3_email = client3.email;
            }
        }

        if (configuration.isTesting) {
            jsondata.isTesting = '1';
        }


        if (doc.isOnline) {
            jsondata.procces_is_online = '1'
        } else {
            jsondata.procces_is_online = '0'
        }



        HTTP.post(Meteor.settings.private.webmergeDocPath.escrowDocs, {
            headers: {
                Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
            },
            data: jsondata
        });
    },
    sendPaymentInstructions: function(id) {
        console.log('sendPaymentInstructions');
        var jsondata,
            doc = Processes.findOne({_id: id}),
            subject = Constants.emails[10].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
            leftToPay,
            recipients = [],
            html;
        if (doc.notary.payment.includeClient1) {
            recipients.push({"Email": doc.primaryClient.email});
        }
        if (doc.notary.payment.includeClient2) {
            recipients.push({"Email": doc.client2.email});
        }
        if (doc.notary.payment.includeClient3) {
            recipients.push({"Email": doc.client3.email});
        }
        SSR.compileTemplate('Step35Email', Assets.getText('emails/Step35Email.html'));
        if (doc.notary.payment.payableToTwoAccounts) {
            if (doc.mortgageRequirement.requiresMortgage) {
                leftToPay = doc.realty.price - doc.mortgageRequest.mortgageApprovedSum;
                html = SSR.render("Step35Email", {
                    includeClient1: doc.notary.payment.includeClient1,
                    client1HebrewName: doc.primaryClient.hebrewName,
                    includeClient2: doc.notary.payment.includeClient2,
                    client2HebrewName: doc.client2.hebrewName,
                    includeClient3: doc.notary.payment.includeClient3,
                    client3HebrewName: doc.client3.hebrewName,
                    payableToTwoAccounts: doc.notary.payment.payableToTwoAccounts,
                    paymentAccount1IBAN: doc.notary.payment.paymentAccount1.IBAN,
                    paymentAccount1AccountHolderName: doc.notary.payment.paymentAccount1.accountHolderName,
                    paymentAccount1Bank: doc.notary.payment.paymentAccount1.bank,
                    paymentAccount1BIC: doc.notary.payment.paymentAccount1.BIC,
                    paymentAccount1Reference: doc.notary.payment.paymentAccount1.reference,
                    account1Payment: Utils.formatCurrencies(doc.notary.payment.paymentAccount1.value),
                    paymentAccount2IBAN: doc.notary.payment.paymentAccount2.IBAN,
                    paymentAccount2AccountHolderName: doc.notary.payment.paymentAccount2.accountHolderName,
                    paymentAccount2Bank: doc.notary.payment.paymentAccount2.bank,
                    paymentAccount2BIC: doc.notary.payment.paymentAccount2.BIC,
                    paymentAccount2Reference: doc.notary.payment.paymentAccount2.reference,
                    account2Payment: Utils.formatCurrencies(doc.notary.payment.paymentAccount2.value),
                    totalPayment: Utils.formatCurrencies(doc.realty.price),
                    clientMortgage: Utils.formatCurrencies(doc.mortgageRequest.mortgageApprovedSum),
                    leftToPay: Utils.formatCurrencies(leftToPay),
                });
            } else {
                leftToPay = doc.realty.price;
                html = SSR.render("Step35Email", {
                    includeClient1: doc.notary.payment.includeClient1,
                    client1HebrewName: doc.primaryClient.hebrewName,
                    includeClient2: doc.notary.payment.includeClient2,
                    client2HebrewName: doc.client2.hebrewName,
                    includeClient3: doc.notary.payment.includeClient3,
                    client3HebrewName: doc.client3.hebrewName,
                    payableToTwoAccounts: doc.notary.payment.payableToTwoAccounts,
                    paymentAccount1IBAN: doc.notary.payment.paymentAccount1.IBAN,
                    paymentAccount1AccountHolderName: doc.notary.payment.paymentAccount1.accountHolderName,
                    paymentAccount1Bank: doc.notary.payment.paymentAccount1.bank,
                    paymentAccount1BIC: doc.notary.payment.paymentAccount1.BIC,
                    paymentAccount1Reference: doc.notary.payment.paymentAccount1.reference,
                    account1Payment: Utils.formatCurrencies(doc.notary.payment.paymentAccount1.value),
                    paymentAccount2IBAN: doc.notary.payment.paymentAccount2.IBAN,
                    paymentAccount2AccountHolderName: doc.notary.payment.paymentAccount2.accountHolderName,
                    paymentAccount2Bank: doc.notary.payment.paymentAccount2.bank,
                    paymentAccount2BIC: doc.notary.payment.paymentAccount2.BIC,
                    paymentAccount2Reference: doc.notary.payment.paymentAccount2.reference,
                    account2Payment: Utils.formatCurrencies(doc.notary.payment.paymentAccount2.value),
                    totalPayment: Utils.formatCurrencies(doc.realty.price),
                    leftToPay: Utils.formatCurrencies(leftToPay)
                });
            }
        } else {
            if (doc.mortgageRequirement.requiresMortgage) {
                leftToPay = doc.realty.price - doc.mortgageRequest.mortgageApprovedSum;
                html = SSR.render("Step35Email", {
                    includeClient1: doc.notary.payment.includeClient1,
                    client1HebrewName: doc.primaryClient.hebrewName,
                    includeClient2: doc.notary.payment.includeClient2,
                    client2HebrewName: doc.client2.hebrewName,
                    includeClient3: doc.notary.payment.includeClient3,
                    client3HebrewName: doc.client3.hebrewName,
                    payableToTwoAccounts: doc.notary.payment.payableToTwoAccounts,
                    paymentAccount1IBAN: doc.notary.payment.paymentAccount1.IBAN,
                    paymentAccount1AccountHolderName: doc.notary.payment.paymentAccount1.accountHolderName,
                    paymentAccount1Bank: doc.notary.payment.paymentAccount1.bank,
                    paymentAccount1BIC: doc.notary.payment.paymentAccount1.BIC,
                    paymentAccount1Reference: doc.notary.payment.paymentAccount1.reference,
                    totalPayment: Utils.formatCurrencies(doc.realty.price),
                    clientMortgage: Utils.formatCurrencies(doc.mortgageRequest.mortgageApprovedSum),
                    leftToPay: Utils.formatCurrencies(leftToPay),
                    requiresMortgage: doc.mortgageRequirement.requiresMortgage
                });
            } else {
                leftToPay = doc.realty.price,
                html = SSR.render("Step35Email", {
                    includeClient1: doc.notary.payment.includeClient1,
                    client1HebrewName: doc.primaryClient.hebrewName,
                    includeClient2: doc.notary.payment.includeClient2,
                    client2HebrewName: doc.client2.hebrewName,
                    includeClient3: doc.notary.payment.includeClient3,
                    client3HebrewName: doc.client3.hebrewName,
                    payableToTwoAccounts: doc.notary.payment.payableToTwoAccounts,
                    paymentAccount1IBAN: doc.notary.payment.paymentAccount1.IBAN,
                    paymentAccount1AccountHolderName: doc.notary.payment.paymentAccount1.accountHolderName,
                    paymentAccount1Bank: doc.notary.payment.paymentAccount1.bank,
                    paymentAccount1BIC: doc.notary.payment.paymentAccount1.BIC,
                    paymentAccount1Reference: doc.notary.payment.paymentAccount1.reference,
                    totalPayment: Utils.formatCurrencies(doc.realty.price),
                    leftToPay: Utils.formatCurrencies(leftToPay),
                    requiresMortgage: doc.mortgageRequirement.requiresMortgage
                });
            }
        }
        Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
    },
    requestSupplierConfirmEquity: function(id) {
        console.log('requestSupplierConfirmEquity');
        var jsondata,
            doc = Processes.findOne({_id: id}),
            subject = Constants.emails[11].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
            recipients = [],
            html;
        recipients.push({"Email": getSupplierEmail(doc.realty.supplier)});
        SSR.compileTemplate('Step36Email', Assets.getText('emails/Step36Email.html'));
        html = SSR.render("Step36Email", {
            primaryClientName: doc.primaryClient.firstName + ' ' + doc.primaryClient.lastName,
            propertAddress: doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ' ' + doc.realty.apartmentNumber
        });
        Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
    },
    assertSeparateNotaryRequirement: function(id) {
        console.log('assertSeparateNotaryRequirement');
        var jsondata,
            doc = Processes.findOne({_id: id});
        if (doc.mortgageRequest.requiresSeparateNotary === false) {
            Processes.update(id, {
                $set: {
                    stepB: '18'
                }
            });
        }
    },
    suggestMortgageNotaryDates: function(id) {
        console.log('suggestMortgageNotaryDates');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            recipients;
        subject = Constants.emails[12].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        SSR.compileTemplate('Step16Email', Assets.getText('emails/Step16Email.html'));
        recipients = [{"Email": getNotaryEmail(doc.realty.notary)}];
        SSR.compileTemplate('Step16Email', Assets.getText('emails/Step16Email.html'));
        html = SSR.render("Step16Email", {
            name: getNotaryName(doc.realty.notary),
            clientName: doc.primaryClient.lastName + ' ' + doc.primaryClient.firstName,
            propertyField: doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
            dates: [moment(doc.mortgageRequest.proposedDate1).tz('Europe/Berlin').format('dddd D MMMM YYYY, HH:mm'), moment(doc.mortgageRequest.proposedDate2).tz('Europe/Berlin').format('dddd D MMMM YYYY, HH:mm'), moment(doc.mortgageRequest.proposedDate3).tz('Europe/Berlin').format('dddd D MMMM YYYY, HH:mm')]
        });
        Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
    },
    selectMortgageNotaryDate: function(id) {
        console.log('confirmMortgageNotaryDate');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            recipients;
        subject = Constants.emails[8].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
        recipients = [{"Email": getNotaryEmail(doc.realty.notary)}];
        SSR.compileTemplate('Step17Email', Assets.getText('emails/Step17Email.html'));
        html = SSR.render("Step17Email", {
            name: getNotaryName(doc.realty.notary),
            clientName: doc.primaryClient.lastName + ' ' + doc.primaryClient.firstName,
            propertyField: doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
            date: moment(doc.mortgageRequest.finalDate).tz('Europe/Berlin').format('dddd D MMMM YYYY, HH:mm')
        });
        Meteor.call('sendEmail', recipients, Constants.senders.suppliers, subject, html, doc.isOnline);
    },

    sendMortgageToNotary: function(id) {
        console.log('sendMortgageToNotary');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            recipients,
            data = {},
            password = Random.hexString(5),
            path = Meteor.settings.private.dropbox.workingPath + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' +doc.realty.apartmentNumber + '/';
        Meteor.call('saveUrlToDropbox', {
            url: doc.mortgageRequest.contract.contractPath,
            path: path + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.mortgageRequest.contract.contractFileName,
            id: id,
            successCallback: function() {
                data.path = path + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.mortgageRequest.contract.contractFileName;
                data.password = password;
                data.callback = function(url) {
                    subject = Constants.emails[13].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
                    recipients = [{"Email": getNotaryEmail(doc.realty.notary)}];
                    SSR.compileTemplate('Step11Email', Assets.getText('emails/Step11Email.html'));
                    html = SSR.render("Step11Email", {
                        name: getNotaryName(doc.realty.notary),
                        clientName: doc.primaryClient.lastName + ' ' + doc.primaryClient.firstName,
                        propertyField: doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
                        approvedSum: doc.mortgageRequest.mortgageApprovedSum,
                        filePath: url,
                        password: password
                    });
                    Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
                };
                Meteor.call('getDropboxSharedLink', data);
            }
        });
    },

    uploadEquityReceipt: function(id) {
        console.log('uploadEquityReceipt');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            data = {},
            recipients,
            password = Random.hexString(5),
            path = Meteor.settings.private.dropbox.workingPath + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' +doc.realty.apartmentNumber + '/';
        Meteor.call('saveUrlToDropbox', {
            url: doc.mortgageRequest.equityReceipt.receiptPath,
            path: path + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.mortgageRequest.equityReceipt.receiptFileName,
            id: id,
            successCallback: function() {
                data.path = path + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.mortgageRequest.equityReceipt.receiptFileName;
                data.password = password;
                data.callback = function(url) {
                    subject = Constants.emails[14].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber;
                    recipients = [{"Email": getBrokerEmail(doc.mortgageRequest.mortgageBroker)}];
                    SSR.compileTemplate('Step19Email', Assets.getText('emails/Step19Email.html'));
                    html = SSR.render("Step19Email", {
                        name: getBrokerName(doc.mortgageRequest.mortgageBroker),
                        clientName: doc.primaryClient.lastName + ' ' + doc.primaryClient.firstName,
                        propertAddress: doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
                        filePath: url,
                        password: password
                    });
                    Meteor.call('sendEmail', recipients, Constants.senders.clients, subject, html, doc.isOnline);
                };
                Meteor.call('getDropboxSharedLink', data);
            }
        });
    },

    sendMgmtContractAndPOA: function(id) {
        console.log('sendMgmtContractAndPOA');
        var jsondata,
            doc = Processes.findOne({_id: id}),
            contractStartDate = doc.management.contractDate,
            client1,
            client2,
            client3,
            configuration = Configurations.findOne(),
            managementFee = doc.management.managementFee;

        jsondata = {
            'realty_full_address': doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ' ' + doc.realty.postcode + ' ' + doc.realty.town,
            'contract_start_date': contractStartDate,
            'management_fee': managementFee,
            'realty_apartment_number': doc.realty.apartmentNumber,
            'now': moment(),
            'first_name': doc.primaryClient.firstName,
            'last_name': doc.primaryClient.lastName,
            'property_address_street': doc.realty.addressStreet,
            'property_address_number': doc.realty.addressNumber,
            'property_apartment_number': doc.realty.apartmentNumber,
            'property_postcode': doc.realty.postcode,
            'hebrew_name': doc.primaryClient.hebrewName
        };

        if (doc.supplierReservation.includeClient1) {
            client1 = doc.primaryClient;
            jsondata.client_1_full_name = client1.firstName + ' ' + client1.lastName;
            jsondata.client_1_full_address =  client1.addressStreet + ' ' + client1.addressNumber + ', ' + client1.postcode + ' ' + client1.town + ', ' + client1.country + ' ';
            jsondata.client_1_passport_number = client1.identification.passportNumber;
            jsondata.Include_client_1_in_supplier_reservation = '1';

            if (configuration.isTesting) {
                jsondata.client_1_email = configuration.testingEmail;
            } else {
                jsondata.client_1_email = client1.email;
            }
        }
        if (doc.supplierReservation.includeClient2) {
            client2 = doc.client2;
            jsondata.client_2_full_name = client2.firstName + ' ' + client2.lastName;
            jsondata.client_2_full_address =  client2.addressStreet + ' ' + client2.addressNumber + ', ' + client2.postcode + ' ' + client2.town + ', ' + client2.country + ' ';
            jsondata.client_2_passport_number = client2.identification.passportNumber;
            jsondata.Include_client_2_in_supplier_reservation = '1';
            if (configuration.isTesting) {
                jsondata.client_2_email = configuration.testingEmail;
            } else {
                jsondata.client_2_email = client2.email;
            }
        }
        if (doc.supplierReservation.includeClient3) {
            client3 = doc.client3;
            jsondata.client_3_full_name = client3.firstName + ' ' + client3.lastName;
            jsondata.client_3_full_address =  client3.addressStreet + ' ' + client3.addressNumber + ', ' + client3.postcode + ' ' + client3.town + ', ' + client3.country + ' ';
            jsondata.client_3_passport_number = client3.identification.passportNumber;
            jsondata.Include_client_3_in_supplier_reservation = '1';
            if (configuration.isTesting) {
                jsondata.client_3_email = configuration.testingEmail;
            } else {
                jsondata.client_3_email = client3.email;
            }
        }

        if (configuration.isTesting) {
            jsondata.isTesting = '1';
        }

        if (doc.isOnline) {
            jsondata.procces_is_online = '1';
            HTTP.post(Meteor.settings.private.webmergeDocPath.mgmtContractAndPOA, {
                headers: {
                    Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                },
                data: jsondata
            });
        } else {
            jsondata.procces_is_online = '0';
            HTTP.post(Meteor.settings.private.webmergeDocPath.mgmtContractAndPOA + '?test=1', {
                headers: {
                    Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                },
                data: jsondata
            });
        }
    },

    uploadMgmtContractAndPOA: function(id) {
        console.log('uploadMgmtContractAndPOA');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            path = Meteor.settings.private.dropbox.workingPath + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' +doc.realty.apartmentNumber + '/';
        Meteor.call('saveUrlToDropbox', {
            url: doc.management.managementContractPath,
            path: path + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.management.managementContractFileName,
            id: id
        });
        Meteor.call('saveUrlToDropbox', {
            url: doc.management.POAPath,
            path: path + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.management.POAFileName,
            id: id
        });
    },

    notifyOnOwnershipChange: function(id) {
        console.log('notifyOnOwnershipChange');
        var jsondata,
            doc = Processes.findOne({_id: id}),
            client = doc.primaryClient,
            configuration = Configurations.findOne(),
            dateFormat = 'DD.MM.YYYY';

        jsondata = {
            'tenant_full_name': doc.management.tenant.firstName + ' ' + doc.management.tenant.lastName,
            'property_full_address': doc.realty.addressStreet + ' ' + doc.realty.addressNumber,
            'postcode': doc.realty.postcode,
            'town': doc.realty.town,
            'date_now': moment(),
            'hand_over_date': doc.management.handoverDate,
            'payment_start_date': doc.management.paymentStartDate,
            'apartment_number': doc.realty.apartmentNumber,
            'tenant_family_name': doc.management.tenant.lastName,
            'client_email': client.email,
            'hebrew_name': client.hebrewName,
            'first_name': client.firstName,
            'last_name': client.lastName,
            'property_address_street': doc.realty.addressStreet,
            'property_address_number': doc.realty.addressNumber,
            'property_apartment_number': doc.realty.apartmentNumber,
            'management_email': Constants.senders.management.email,
            'management_name': Constants.senders.management.name
        };

        if (doc.management.secondTenant) {
            jsondata.include_second_tenant = 1;
            jsondata.second_tenant_full_name = doc.management.secondTenant.firstName + ' ' + doc.management.secondTenant.lastName;
            jsondata.second_tenant_family_name = doc.management.secondTenant.lastName;
        }

        if (configuration.isTesting) {
            jsondata.client_email = configuration.testingEmail;
            jsondata.isTesting = '1';
        }
        if (doc.isOnline) {
            jsondata.procces_is_online = '1';
            HTTP.post(Meteor.settings.private.webmergeDocPath.ownershipChange, {
                headers: {
                    Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                },
                data: jsondata
            });
        } else {
            jsondata.procces_is_online = '0';
            HTTP.post(Meteor.settings.private.webmergeDocPath.ownershipChange + '?test=1', {
                headers: {
                    Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                },
                data: jsondata
            });
        }
    },

    introToManagementCompany: function(id) {
        console.log('introToManagementCompany');
        var subject,
            html,
            doc = Processes.findOne({_id: id}),
            recipients;

        subject = Constants.emails[15].subject + ' ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ' apartment:' + doc.realty.apartmentNumber;
        recipients = [{"Email": doc.management.managementCompany.details.email},{"Email": Constants.senders.management.email}];
        SSR.compileTemplate('Step45Email', Assets.getText('emails/Step45Email.html'));
        Meteor.call('getDropboxSharedLink', {
            path: Meteor.settings.private.dropbox.workingPath + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' +doc.realty.apartmentNumber + '/' + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.management.POAFileName,
            callback: function(url){
                html = SSR.render("Step45Email", {
                    ownerName: doc.primaryClient.firstName + ' ' + doc.primaryClient.lastName,
                    handoverDate: moment(doc.management.handoverDate).format('DD.MM.YYYY'),
                    managementName: Constants.senders.management.name,
                    linkToPOA: url
                });
                Meteor.call('sendEmail', recipients, Constants.senders.management, subject, html, doc.isOnline);
            }
        });
    },

    notifyOnRentalsToBePayed: function(id) {
        console.log('notifyOnRentalsToBePayed');
        var jsondata,
            doc = Processes.findOne({_id: id}),
            client = doc.primaryClient,
            configuration = Configurations.findOne(),
            netPayment = doc.realty.rentInClientsOffer - doc.realty.buildingManagementFee - doc.realty.reserveFund - doc.management.managementFee,
            tenantAdvancePay = doc.management.managementCompany.housegeld - doc.realty.buildingManagementFee - doc.realty.reserveFund,
            actualRentPayed = doc.management.firstRentalPaymentSum - tenantAdvancePay,
            paymentDelta = doc.realty.rentInClientsOffer - actualRentPayed,
            actualOwnerPay = doc.management.firstRentalPaymentSum - doc.management.managementCompany.housegeld - doc.management.managementFee;

        jsondata = {
            'client_email': client.email,
            'hebrew_name': client.hebrewName,
            'first_name': client.firstName,
            'last_name': client.lastName,
            'property_address_street': doc.realty.addressStreet,
            'property_address_number': doc.realty.addressNumber,
            'property_apartment_number': doc.realty.apartmentNumber,
            'owner_full_name': client.firstName + ' ' + client.lastName,
            'property_address': doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ' ' + doc.realty.apartmentNumber,
            'rent_in_client_offer': doc.realty.rentInClientsOffer,
            'building_mgmt_fee': doc.realty.buildingManagementFee,
            'reserve_fund': doc.realty.reserveFund,
            'tenant_mgmt_fee': doc.management.managementFee,
            'net_payment': netPayment,
            'bruto_tenant_pay': doc.management.firstRentalPaymentSum,
            'housegeld': doc.management.managementCompany.housegeld,
            'tenant_advance_pay': tenantAdvancePay,
            'actual_rent_payed': actualRentPayed,
            'payment_delta': paymentDelta,
            'actual_owner_pay': actualOwnerPay

        };

        if (configuration.isTesting) {
            jsondata.client_email = configuration.testingEmail;
            jsondata.isTesting = '1';
        }
        if (doc.isOnline) {
            jsondata.procces_is_online = '1';
            HTTP.post(Meteor.settings.private.webmergeDocPath.rentalsToBePayed, {
                headers: {
                    Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                },
                data: jsondata
            });
        } else {
            jsondata.procces_is_online = '0';
            HTTP.post(Meteor.settings.private.webmergeDocPath.rentalsToBePayed + '?test=1', {
                headers: {
                    Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                },
                data: jsondata
            });
        }
    },

    informFinanzamt: function(id) {
        console.log('informFinanzamt');
        var doc = Processes.findOne({_id: id}),
            client = doc.primaryClient,
            subject,
            html,
            doc = Processes.findOne({_id: id}),
            recipients;

        subject = Constants.emails[16].subject + ' ' +  client.lastName + ' - ' + doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ' apartment:' + doc.realty.apartmentNumber;
        recipients = [{"Email": doc.management.finanzamtEmail}, {"Email": Constants.senders.management.email}];
        SSR.compileTemplate('Step49Email', Assets.getText('emails/Step49Email.html'));
        Meteor.call('getDropboxSharedLink', {
            path: Meteor.settings.private.dropbox.workingPath + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.realty.addressStreet + '_' + doc.realty.addressNumber + '_' +doc.realty.apartmentNumber + '/' + doc.primaryClient.firstName + '_' + doc.primaryClient.lastName + '_' + doc.management.POAFileName,
            callback: function(url){
                html = SSR.render("Step49Email", {
                    apartment: doc.realty.apartmentNumber,
                    handoverDate: moment(doc.management.handoverDate).format('DD.MM.YYYY'),
                    streetName: doc.realty.addressStreet,
                    streetNumber: doc.realty.addressNumber,
                    lastName: client.lastName,
                    managementName: Constants.senders.management.name,
                    linkToPOA: url
                });
                Meteor.call('sendEmail', recipients, Constants.senders.management, subject, html, doc.isOnline);
            }
        });
    },
    sendFirstReminder: function(id) {
        console.log('sendFirstReminder');
        try {
            var doc = Processes.findOne({_id: id}),
                client = doc.primaryClient,
                config = Configurations.findOne(),
                jsondata = {
                    'full_name': doc.management.tenant.firstName + ' ' + doc.management.tenant.lastName,
                    'last_name': doc.management.tenant.lastName,
                    'full_address': doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ' ' + doc.realty.apartmentNumber,
                    'postcode': doc.realty.postcode,
                    'town': doc.realty.town,
                    'emailTo': Constants.senders.management.email,
                    'date_now': moment(),
                    'rent_not_payed_month': doc.management.firstReminder.monthRentDue,
                    'rent_due_amount': doc.management.firstReminder.rentDueAmount,
                    'deadline_date': doc.management.firstReminder.deadlineToPay,
                    'client_first_name': client.firstName,
                    'client_last_name': client.lastName,
                    'property_address_street': doc.realty.addressStreet,
                    'property_address_number': doc.realty.addressNumber,
                    'property_apartment_number': doc.realty.apartmentNumber,
                    'rent_with_late_fee': doc.management.firstReminder.rentDueAmount + doc.management.firstReminder.lateFee
                };
            if (config.isTesting) {
                jsondata.emailTo = config.testingEmail;
                jsondata.isTesting = '1';
            }
            if (doc.isOnline) {
                jsondata.procces_is_online = '1';
                HTTP.post(Meteor.settings.private.webmergeDocPath.firstReminder, {
                    headers: {
                        Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                    },
                    data: jsondata
                });
            } else {
                jsondata.procces_is_online = '0';
                HTTP.post(Meteor.settings.private.webmergeDocPath.firstReminder + '?test=1', {
                    headers: {
                        Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                    },
                    data: jsondata
                });
            }
        } catch (error) {
            console.log('error when trying send first reminder letter. Error = ' + error);
            throw new Meteor.Error('management-action-error', 'error when trying send first reminder letter.');
        }
    },
    sendSecondReminder: function(id) {
        console.log('sendSecondReminder');
        try {
            var doc = Processes.findOne({_id: id}),
                client = doc.primaryClient,
                config = Configurations.findOne(),
                jsondata = {
                    'full_name': doc.management.tenant.firstName + ' ' + doc.management.tenant.lastName,
                    'last_name': doc.management.tenant.lastName,
                    'full_address': doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ' ' + doc.realty.apartmentNumber,
                    'postcode': doc.realty.postcode,
                    'town': doc.realty.town,
                    'emailTo': Constants.senders.management.email,
                    'date_now': moment(),
                    'rent_not_payed_month': doc.management.secondReminder.monthRentDue,
                    'rent_due_amount': doc.management.secondReminder.rentDueAmount,
                    'deadline_date': doc.management.secondReminder.deadlineToPay,
                    'client_first_name': client.firstName,
                    'client_last_name': client.lastName,
                    'property_address_street': doc.realty.addressStreet,
                    'property_address_number': doc.realty.addressNumber,
                    'property_apartment_number': doc.realty.apartmentNumber,
                    'rent_with_late_fee': doc.management.secondReminder.rentDueAmount + doc.management.secondReminder.lateFee
                };
            if (config.isTesting) {
                jsondata.emailTo = config.testingEmail;
                jsondata.isTesting = '1';
            }
            if (doc.isOnline) {
                jsondata.procces_is_online = '1';
                HTTP.post(Meteor.settings.private.webmergeDocPath.secondReminder, {
                    headers: {
                        Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                    },
                    data: jsondata
                });
            } else {
                jsondata.procces_is_online = '0';
                HTTP.post(Meteor.settings.private.webmergeDocPath.secondReminder + '?test=1', {
                    headers: {
                        Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                    },
                    data: jsondata
                });
            }
        } catch (error) {
            console.log('error when trying send second reminder letter. Error = ' + error);
            throw new Meteor.Error('management-action-error', 'error when trying send second reminder letter.');
        }
    },
    sendSelfUseLetter: function(id) {
        console.log('sendSelfUseLetter');
        try {
            var doc = Processes.findOne({_id: id}),
                client = doc.primaryClient,
                config = Configurations.findOne(),
                jsondata = {
                    'full_name': doc.management.tenant.firstName + ' ' + doc.management.tenant.lastName,
                    'last_name': doc.management.tenant.lastName,
                    'full_address': doc.realty.addressStreet + ' ' + doc.realty.addressNumber + ' ' + doc.realty.apartmentNumber,
                    'postcode': doc.realty.postcode,
                    'town': doc.realty.town,
                    'emailTo': Constants.senders.management.email,
                    'date_now': moment(),
                    'client_first_name': client.firstName,
                    'client_last_name': client.lastName,
                    'property_address_street': doc.realty.addressStreet,
                    'property_address_number': doc.realty.addressNumber,
                    'property_apartment_number': doc.realty.apartmentNumber,
                    'customer_support_name': Constants.senders.customerSupport.name,
                    'customer_support_email': Constants.senders.customerSupport.email

        };
            if (config.isTesting) {
                jsondata.emailTo = config.testingEmail;
                jsondata.isTesting = '1';
            }
            if (doc.isOnline) {
                jsondata.procces_is_online = '1';
                HTTP.post(Meteor.settings.private.webmergeDocPath.selfUseLetter, {
                    headers: {
                        Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                    },
                    data: jsondata
                });
            } else {
                jsondata.procces_is_online = '0';
                HTTP.post(Meteor.settings.private.webmergeDocPath.selfUseLetter + '?test=1', {
                    headers: {
                        Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                    },
                    data: jsondata
                });
            }
        } catch (error) {
            console.log('error when trying send self use letter. Error = ' + error);
            throw new Meteor.Error('management-action-error', 'error when trying send self use letter.');
        }
    },
    createRentIncreaseLetter: function(payload) {
        console.log('createRentIncreaseLetter');
        try {
            var doc = Processes.findOne({_id: payload.id}),
                email = payload.email,
                rentBruttos;
                jsondata = {

                };
            if (doc.management.tenant.previousRentBruttos) {
                rentBruttos = doc.management.tenant.previousRentBruttos;
            } else {
                rentBruttos = []
            }
            rentBruttos.push({
                rentBrutto: doc.management.tenant.actualRentBrutto,
                rentBruttoDate: doc.management.tenant.actualRentBruttoDate
            });
            Processes.update(payload.id, {
                $set: {
                    'management.tenant.previousRentBruttos': rentBruttos,
                    'management.tenant.actualRentBrutto': payload.rentBrutto,
                    'management.tenant.actualRentBruttoDate': new Date(payload.rentBurttoDate),
                    'management.tenant.lastIncreaseDate': new Date(payload.rentBurttoDate)
                }
            });
            console.log('payload = ' + JSON.stringify(payload));

        } catch (error) {
            console.log('error when trying to create rent increase letter. Error = ' + error);
            throw new Meteor.Error('management-action-error', 'error when trying to create rent increase letter.');
        }
    },
    updateAccountingIssues: function(issues) {
        console.log('updateAccountingIssues');
        try {
            _.forEach(issues, function(issue) {
                console.log('updateAccountingIssues, forEach');
                var accountingIssue = AccountingIssues.findOne({$and: [{op: issue.op}, {sum: issue.sum}, {IBAN: issue.IBAN}, {executionDate: issue.executionDate}, {purpose: issue.purpose}]}),
                    accountingIssueData = {
                        executionDate: issue.executionDate,
                        valueDate: issue.valueDate,
                        payee: issue.payee,
                        payer: issue.payer,
                        accountNumber: issue.accountNumber,
                        IBAN: issue.IBAN,
                        BIC: issue.BIC,
                        BLZ: issue.BLZ,
                        purpose: issue.purpose,
                        currency: issue.currency,
                        sum: issue.sum,
                        op: issue.op
                    };
                console.log('updateAccountingIssues, accountingIssueData');
                if (!accountingIssueData.payee) {
                    console.log('no payee, accountingIssueData', accountingIssueData);
                    return;
                }
                if (accountingIssue) {
                    AccountingIssues.update(accountingIssue._id, {
                        $set: accountingIssueData
                    });
                } else {
                    AccountingIssues.insert(accountingIssueData);
                }
            });
        } catch (error) {
            console.log('error when trying to update accounting issues. Error = ' + error);
            throw new Meteor.Error('update-accounting-issues-error', 'error when trying to update accounting issues.');
        }
    },
    deleteAllAccountingIssues: function() {
        console.log('deleteAllAccountingIssues');
        try {
            AccountingIssues.remove({});
        } catch (error) {
            console.log('error when trying to delete all accounting issues. Error = ' + error);
            throw new Meteor.Error('delete-all-accounting-issues-error', 'error when trying to delete all accounting issues.');
        }
    }
});

function getSupplierEmail(supplierId) {
    var doc = Suppliers.findOne({
        _id: supplierId
    });
    return doc.email;
}

function getSupplierName(supplierId) {
    var doc = Suppliers.findOne({
        _id: supplierId
    });
    return doc.simpleName;
}

function getNotaryEmail(notaryId) {
    var doc = Notaries.findOne({
        _id: notaryId
    });
    return doc.email;
}

function getNotaryName(notaryId) {
    var doc = Notaries.findOne({
        _id: notaryId
    });
    return doc.simpleName;
}

function getBrokerName(brokerId) {
    var doc = MortgageBrokers.findOne({
        _id: brokerId
    });
    return doc.simpleName;
}

function getBrokerEmail(brokerId) {
    var doc = MortgageBrokers.findOne({
        _id: brokerId
    });
    return doc.email;
}

function getBrokerFolderName(brokerId) {
    var doc = MortgageBrokers.findOne({
        _id: brokerId
    });
    return doc.folder;
}

function createPayslips(client, clientFolder, brokerId, doc, isTesting) {
    var date = new Date(),
        jsondata = {
            date: date.toDateString(),
            employer_name_1: client.payslip1.employerName,
            employer_address_1: client.payslip1.employerAddress,
            employee_name: client.firstName + ' ' + client.lastName,
            employee_address: client.addressStreet + ' ' + client.addressNumber + ', ' + client.postcode + ', ' + client.town,
            employer_id_1: client.payslip1.employerTaxNumber,
            employee_id: client.idNumber,
            martial_status: client.maritalStatus,
            salary_for_month_1: client.payslip1.monthAndYear,
            start_date_of_employment_1: client.payslip1.employedSince.toDateString(),
            salary_type_1: client.payslip1.salaryType,
            brutto_1: client.payslip1.salaryBrutto,
            income_tax_1: client.payslip1.incomeTax,
            national_pension_1: client.payslip1.nationalPension,
            health_insurance_1: client.payslip1.healthInsurance,
            voluntary_pension_1: client.payslip1.voluntaryPension,
            other_expenses_1: client.payslip1.otherExpenses,
            netto_1: client.payslip1.salaryNetto,
            employer_name_2: client.payslip2.employerName,
            employer_address_2: client.payslip2.employerAddress,
            employer_id_2: client.payslip2.employerTaxNumber,
            salary_for_month_2: client.payslip2.monthAndYear,
            start_date_of_employment_2: client.payslip2.employedSince.toDateString(),
            salary_type_2: client.payslip2.salaryType,
            brutto_2: client.payslip2.salaryBrutto,
            income_tax_2: client.payslip2.incomeTax,
            national_pension_2: client.payslip2.nationalPension,
            health_insurance_2: client.payslip2.healthInsurance,
            voluntary_pension_2: client.payslip2.voluntaryPension,
            other_expenses_2: client.payslip2.otherExpenses,
            netto_2: client.payslip2.salaryNetto,
            employer_name_3: client.payslip3.employerName,
            employer_address_3: client.payslip3.employerAddress,
            employer_id_3: client.payslip3.employerTaxNumber,
            salary_for_month_3: client.payslip3.monthAndYear,
            start_date_of_employment_3: client.payslip3.employedSince.toDateString(),
            salary_type_3: client.payslip3.salaryType,
            brutto_3: client.payslip3.salaryBrutto,
            income_tax_3: client.payslip3.incomeTax,
            national_pension_3: client.payslip3.nationalPension,
            health_insurance_3: client.payslip3.healthInsurance,
            voluntary_pension_3: client.payslip3.voluntaryPension,
            other_expenses_3: client.payslip3.otherExpenses,
            netto_3: client.payslip3.salaryNetto,
            ordering_client: clientFolder,
            folder_name: getBrokerFolderName(brokerId),
            isTesting: isTesting
        };
    if (doc.isOnline) {
        jsondata.procces_is_online = '1';
        HTTP.post(Meteor.settings.private.webmergeDocPath.payslip, {
            headers: {
                Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
            },
            data: jsondata
        });
    } else {
        jsondata.procces_is_online = '0';
        HTTP.post(Meteor.settings.private.webmergeDocPath.payslip + '?test=1', {
            headers: {
                Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
            },
            data: jsondata
        });
    }
}

function create106Reports(client, clientFolder, brokerId, doc, isTesting) {
    var date = new Date(),
        jsondata = {
            date: date.toDateString(),
            employee_id: client.idNumber,
            employee_name: client.firstName + ' ' + client.lastName,
            employee_address: client.addressStreet + ' ' + client.addressNumber + ', ' + client.postcode + ', ' + client.town,
            tax_year_1: client.form106Year1.year,
            employer_name_1: client.form106Year1.employerame,
            employer_address_1: client.form106Year1.employerAddress,
            tax_number1: client.form106Year1.employerTaxNumber,
            brutto_1: client.form106Year1.salaryBrutto,
            taxable_salary_1: client.form106Year1.taxableSalary,
            income_tax_1: client.form106Year1.incomeTax,
            national_pension_1: client.form106Year1.nationalPension,
            health_insurance_1: client.form106Year1.healthInsurance,
            voluntary_pensions_1: client.form106Year1.voluntaryPension,
            refunds_1: client.form106Year1.refunds,
            tax_year_2: client.form106Year2.year,
            employer_name_2: client.form106Year2.employerame,
            employer_address_2: client.form106Year2.employerAddress,
            tax_number2: client.form106Year2.employerTaxNumber,
            brutto2: client.form106Year2.salaryBrutto,
            taxable_salary2: client.form106Year2.taxableSalary,
            income_tax2: client.form106Year2.incomeTax,
            national_pension2: client.form106Year2.nationalPension,
            health_insurance2: client.form106Year2.healthInsurance,
            voluntary_pensions2: client.form106Year2.voluntaryPension,
            refunds2: client.form106Year2.refunds,
            ordering_client: clientFolder,
            folder_name: getBrokerFolderName(brokerId),
            isTesting: isTesting
        };
    // console.log('jsondata = ' + JSON.stringify(jsondata));
    if (doc.isOnline) {
        jsondata.procces_is_online = '1';
        HTTP.post(Meteor.settings.private.webmergeDocPath.form106, {
            headers: {
                Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
            },
            data: jsondata
        });
    } else {
        jsondata.procces_is_online = '0';
        HTTP.post(Meteor.settings.private.webmergeDocPath.form106 + '?test=1', {
            headers: {
                Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
            },
            data: jsondata
        });
    }
}

function createSelfEmployedReport(client, clientFolder, brokerId, doc, isTesting) {
    var date = new Date(),
        jsondata = {
            date: date.toDateString(),
            full_name: client.firstName + ' ' + client.lastName,
            full_address: client.addressStreet + ' ' + client.addressNumber + ', ' + client.postcode + ', ' + client.town,
            id: client.idNumber,
            year_1: client.selfEmployed1.taxYear,
            annual_income_1: client.selfEmployed1.annualIncome,
            taxable_income_1: client.selfEmployed1.taxableIncome,
            income_tax_1: client.selfEmployed1.incomeTax,
            refunds_1: client.selfEmployed1.refunds,
            net_tax_1: client.selfEmployed1.netTax,
            advance_paymants_1: client.selfEmployed1.advancePayment,
            balance_to_pay_1: client.selfEmployed1.balanceToPay,
            year_2: client.selfEmployed2.taxYear,
            annual_income_2: client.selfEmployed2.annualIncome,
            taxable_income_2: client.selfEmployed2.taxableIncome,
            income_tax_2: client.selfEmployed2.incomeTax,
            refunds_2: client.selfEmployed2.refunds,
            net_tax_2: client.selfEmployed2.netTax,
            advance_paymants_2: client.selfEmployed2.advancePayment,
            balance_to_pay_2: client.selfEmployed2.balanceToPay,
            ordering_client: clientFolder,
            folder_name: getBrokerFolderName(brokerId),
            isTesting: isTesting
        };
    if (doc.isOnline) {
        jsondata.procces_is_online = '1';
        HTTP.post(Meteor.settings.private.webmergeDocPath.selfEmployedReport, {
            headers: {
                Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
            },
            data: jsondata
        });
    } else {
        jsondata.procces_is_online = '0';
        HTTP.post(Meteor.settings.private.webmergeDocPath.selfEmployedReport + '?test=1', {
            headers: {
                Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
            },
            data: jsondata
        });
    }
}

function createMortgageApplicationCallbackClosure(jsondata) {
    return function (error, result) {
        if (error) {
            console.log('error = ' + JSON.stringify(result));
        } else {
            // console.log('result = ' + JSON.stringify(result));
            jsondata.signature = result.data.link;
            if (jsondata.procces_is_online === '1') {
                HTTP.post(Meteor.settings.private.webmergeDocPath.mortgageApplication, {
                    headers: {
                        Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                    },
                    data: jsondata
                });
            } else {
                HTTP.post(Meteor.settings.private.webmergeDocPath.mortgageApplication + '?test=1', {
                    headers: {
                        Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
                    },
                    data: jsondata
                });
            }
        }
    }
}

function createMortgageApplication(client, mortgageRequest, clientFolder, propertyInfo, isOnline) {
    var broker = MortgageBrokers.findOne({_id: mortgageRequest.mortgageBroker}),
        config = Configurations.findOne();
        date = new Date(),
        jsondata = {
            Date: date.toDateString(),
            Name: client.lastName,
            Vorname: client.firstName,
            Geburtsdatum: client.birthDate,
            Staatsnagehorigkeit: client.mainNationality,
            PLZ_und_wohnort: client.postcode + ' ' + client.town,
            Nation: client.country,
            Starse_und_hausnummer: client.addressStreet + ' ' + client.addressNumber,
            Email: client.email,
            Familienstand: client.maritalStatus,
            Anzahl_der_unterhaltspflichigen_kinder: client.incomeAndExpenses.numberOfDependantChildren,
            Auseubter_beruf: client.profession,
            Weiterer_grundbesitz: client.incomeAndExpenses.worthOfOwnedRealestate,
            Bankguthaben: client.incomeAndExpenses.bankAndSavingsDeposits,
            Sonstiges_vermogen: client.incomeAndExpenses.otherAssets,
            Nettoeinkommen: client.incomeAndExpenses.annualNetIncome,
            Kindergeld: client.incomeAndExpenses.annualChildAllowance,
            Einkunfte_aus_kapitalvermogen: client.incomeAndExpenses.annualCapitalIncome,
            Einkunfte_aus_vermietung: client.incomeAndExpenses.annualRentalIncome,
            Lebenshaltungkosten: client.incomeAndExpenses.annualLivingExpenses,
            Wohnkosten: client.incomeAndExpenses.annualAccommodationExpenses,
            Sonstige_kosten: client.incomeAndExpenses.otherAnnualExpenses,
            Beitrag_private_krankenvers: client.incomeAndExpenses.annualPremiumsForPrivateHealthInsurance,
            Unterhaltskosten: '€ 0,00',
            Kosten_fur_immobilienkredite: '€ 0,00',
            Kosten_fur_sostige_kredite: '€ 0,00',
            Kosten_fur_ratenkredite: '€ 0,00',
            Antragsteller: client.lastName + ' ' + client.firstName,
            Anschrift: client.addressStreet + ' ' + client.addressNumber + ', ' + client.postcode + ', ' + client.town + ', ' + client.country,
            account_holder_name: mortgageRequest.bankAccountName,
            IBAN: mortgageRequest.iban,
            Account_bank: mortgageRequest.bankNameAddress,
            broker_email: broker.email,
            BrokerName: broker.simpleName,
            property_info: propertyInfo,
            ordering_client: clientFolder,
            folder_name: broker.folder
        };
    if (client.professionType === 'employed') {
        var mostRecentPayslip = getMostRecentPayslip(client);
        jsondata.Angestllt_seit = mostRecentPayslip.employedSince;
        jsondata.Arbietgeber = mostRecentPayslip.employerName;
    } else {
        jsondata.Selbstandig_seit = client.selfEmployedSince;
        console.log('jsondata.Selbstandig_seit = ' + jsondata.Selbstandig_seit);
    }
    if (isOnline) {
        jsondata.procces_is_online = '1'
    } else {
        jsondata.procces_is_online = '0'
    }
    if (config.isTesting) {
        jsondata.email1 = config.testingEmail;
        jsondata.email2 = config.testingEmail;
        jsondata,email3 = config.testingEmail ;
    }
    HTTP.post("https://api.dropboxapi.com/2/files/get_temporary_link", {
        headers: {
            Authorization: "Bearer " + Meteor.settings.private.dropbox.apiKey,
            "Content-Type": "application/json"
        },
        data: {
            "path": Meteor.settings.private.dropbox.workingPath + clientFolder + '/' + client.firstName + '_' + client.lastName + '_' + client.identification.signatureFileName
        }
    }, createMortgageApplicationCallbackClosure(jsondata));
}

function getMostRecentPayslip(client) {
    var paySlips = [client.payslip1, client.payslip2, client.payslip3];
    return _.max(paySlips, function getMostRecent(payslip){
        return moment(payslip.monthAndYear, 'MMMM YYYY').valueOf();
    });
}

function createSelfAssessment(client, signature) {
    var date = new Date(),
        jsondata = {
            Date: date.toDateString(),
            Name: client.lastName,
            Vorname: client.firstName,
            Geburtsdatum: client.birthDate.toDateString(),
            Staatsnagehorigkeit: client.mainNationality,
            PLZ_und_wohnort: client.postcode + ' ' + client.town,
            Nation: client.country,
            Starse_und_hausnummer: client.addressStreet + ' ' + client.addressNumber,
            Email: client.email,
            Familienstand: client.maritalStatus,
            Anzahl_der_unterhaltspflichigen_kinder: client.incomeAndExpenses.numberOfDependantChildren,
            Auseubter_beruf: client.profession,
            Weiterer_grundbesitz: client.incomeAndExpenses.worthOfOwnedRealestate,
            Bankguthaben: client.incomeAndExpenses.bankAndSavingsDeposits,
            Sonstiges_vermogen: client.incomeAndExpenses.otherAssets,
            Nettoeinkommen: client.incomeAndExpenses.annualNetIncome,
            Kindergeld: client.incomeAndExpenses.annualChildAllowance,
            Einkunfte_aus_kapitalvermogen: client.incomeAndExpenses.annualCapitalIncome,
            Einkunfte_aus_vermietung: client.incomeAndExpenses.annualRentalIncome,
            Lebenshaltungkosten: client.incomeAndExpenses.annualLivingExpenses,
            Wohnkosten: client.incomeAndExpenses.annualAccommodationExpenses,
            Sonstige_kosten: client.incomeAndExpenses.otherAnnualExpenses,
            Beitrag_private_krankenvers: client.incomeAndExpenses.annualPremiumsForPrivateHealthInsurance,
            Unterhaltskosten: '€ 0,00',
            Kosten_fur_immobilienkredite: '€ 0,00',
            Kosten_fur_sostige_kredite: '€ 0,00',
            Kosten_fur_ratenkredite: '€ 0,00',
            signature: signature
        };

    return jsondata;
}

function createAccountCreationDoc(client, mortgageRequest, signature) {
    console.log('createAccountCreationDoc');
    var date = new Date(),
        jsondata = {
            Date: date.toDateString(),
            Name: client.lastName,
            Vorname: client.firstName,
            account_holder_name: mortgageRequest.bankAccountName,
            IBAN: mortgageRequest.iban,
            Account_bank: mortgageRequest.bankNameAddress,
            signature: signature
        };

    return jsondata;
}

function createMortgageApplicationSolo(clients, signatures) {
    console.log('createMortgageApplicationSolo');
    var date = new Date(),
        jsondata = {
            Date: date.toDateString(),
            Name: clients[0].lastName,
            Vorname: clients[0].firstName,
            Anschrift: clients[0].addressStreet + ' ' + clients[0].addressNumber + ', ' + clients[0].postcode + ', ' + clients[0].town + ', ' + clients[0].country,
        };

    if (clients.length === 3) {
        jsondata.Antragsteller = clients[0].lastName + ' ' + clients[0].firstName + ' und ' + clients[1].lastName + ' ' + clients[1].firstName + ' und ' + clients[2].lastName + ' ' + clients[2].firstName;
        jsondata.signature1 = signatures[0];
        jsondata.signature2 = signatures[1];
        jsondata.signature3 = signatures[2];
    } else if (clients.length === 2) {
        jsondata.Antragsteller = clients[0].lastName + ' ' + clients[0].firstName + ' und ' + clients[1].lastName + ' ' + clients[1].firstName;
        jsondata.signature1 = signatures[0];
        jsondata.signature2 = signatures[1];
    } else {
        jsondata.Antragsteller = clients[0].lastName + ' ' + clients[0].firstName;
        jsondata.signature1 = signatures[0];
    }
    return jsondata;
}

function sendMortgageApplicationRoute(clients, signatures, mortgageRequest, clientFolder, propertyInfo, isOnline, config) {
    console.log('sendMortgageApplicationRoute');

    var broker = MortgageBrokers.findOne({_id: mortgageRequest.mortgageBroker}),
        jsondata = {
            selfAssessments: [],
            broker_email: broker.email,
            BrokerName: broker.simpleName,
            property_info: propertyInfo,
            ordering_client: clientFolder,
            folder_name: broker.folder
        };

    _.forEach(clients, function(client, index) {
        jsondata.selfAssessments.push(createSelfAssessment(client, signatures[index]));
    });

    jsondata = _.extend(jsondata, createAccountCreationDoc(clients[0], mortgageRequest, signatures[0]));
    jsondata = _.extend(jsondata, createMortgageApplicationSolo(clients, signatures));

    if (isOnline) {
        jsondata.procces_is_online = '1'
    } else {
        jsondata.procces_is_online = '0'
    }
    if (config.isTesting) {
        jsondata.email1 = config.testingEmail;
        jsondata.email2 = config.testingEmail;
        jsondata,email3 = config.testingEmail;
    }

    HTTP.post(Meteor.settings.private.webmergeDocPath.mortgageApplicationRoute, {
        headers: {
            Authorization: Meteor.settings.private.webmerge.key + ':' + Meteor.settings.private.webmerge.secret
        },
        data: jsondata
    });
}

function fetchClientSignature(client, clientFolder) {
    return new Promise(function(resolve, reject) {
        HTTP.post("https://api.dropboxapi.com/2/files/get_temporary_link", {
            headers: {
                Authorization: "Bearer " + Meteor.settings.private.dropbox.apiKey,
                "Content-Type": "application/json"
            },
            data: {
                "path": Meteor.settings.private.dropbox.workingPath + clientFolder + '/' + client.firstName + '_' + client.lastName + '_' + client.identification.signatureFileName
            }
        }, function (err, result){
            if (!err) {
                return resolve(result.data.link);
            } else {
                return reject(err);
            }
        });
    }).catch(function onReject(err){
        console.error(err);
    });
}

function generateRefNumber(doc) {
    var ref,
        address,
        apartmentNumber,
        addressNumber;

    function pad(num, size) {
        var s = "000000000" + num;
        return s.substring(s.length - size, s.length);
    };

    address = latinize(doc.realty.addressStreet.replace(/\s/g, "")).replace(/[^\w\s]/gi, '');
    addressNumber = doc.realty.addressNumber.replace(/[^\w\s]/gi, '');
    apartmentNumber = doc.realty.apartmentNumber.replace(/\s/g, "").replace(/[^\w\s]/gi, '').replace(/WE/, '');
    if (addressNumber.length < 3) {
        addressNumber = pad(addressNumber, 3);
    }
    if (apartmentNumber.length < 2) {
        apartmentNumber = pad(apartmentNumber, 2);
    }
    return ref = (address.substring(0,3) + addressNumber.substring(0,3) + apartmentNumber.substring(0,2)).toUpperCase();
}