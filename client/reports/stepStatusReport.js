import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;
import _ from 'lodash';

var listener;
Template.StepStatusReport.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processes');
        self.subscribe('suppliers');
    });
    Session.set('SelectedStep',[-1]);
    this.location = new ReactiveVar('All');
    listener = function(data) {
        this.location.set(data.location);
    };
    Event.on('location-filter-change', Meteor.bindEnvironment(listener, null, this));
};

Template.StepStatusReport.destroyed = function() {
    Event.removeListener('location-filter-change', listener);
};

Template.StepStatusReport.helpers({
    location: function(){
        return Template.instance().location.get();
    },
    getCollection: function() {
        let collection;
        switch (Template.instance().location.get()) {
            case 'All':
                collection = Processes.find({}).fetch();
                break;
            case 'Berlin':
                collection = Processes.find({ 'realty.town': /^berlin$/i}).fetch();
                break;
            case 'NRW':
                collection = Processes.find({ 'realty.town': {$not: /^berlin$/i}}).fetch();
                break;
            default:
        }
        return collection;
    },
    getPriceSum: function() {
        var selectedSteps =  Session.get('SelectedStep'),
            processes,
            overallSum;
        switch (Template.instance().location.get()) {
            case 'All':
                processes = Processes.find({ $or: [ { 'step': { $in: selectedSteps } }, { 'stepB': { $in: selectedSteps } } ] }).fetch();
                break;
            case 'Berlin':
                processes = Processes.find({ $and:[ { 'realty.town': /^berlin$/i}, {$or: [ { 'step': { $in: selectedSteps } }, { 'stepB': { $in: selectedSteps } } ]} ]}).fetch();
                break;
            case 'NRW':
                processes = Processes.find({ $and:[ { 'realty.town': {$not: /^berlin$/i}}, {$or: [ { 'step': { $in: selectedSteps } }, { 'stepB': { $in: selectedSteps } } ]} ]}).fetch();
                break;
            default:
        }
        overallSum = _.reduce(processes, function(sum, process){
            return sum + process.realty.price;
        }, 0);
        return Utils.formatCurrencies(overallSum);

    },
    getSettings: function() {
        return {
            collection: Template.ProcessStatusReport.__helpers.get('getCollection').call(),
            rowsPerPage: 50,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            filters: ['select-filter', 'select-filterB'],
            filterOperator: "$or",
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.referenceNumber',
                    label: 'Ref #'
                },
                {
                    key: 'realty.propertyType',
                    label: 'Property Type'
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Street',
                    hidden: true
                },
                {
                    key: 'realty.addressNumber',
                    label: 'Number',
                    hidden: true
                },
                {
                    key: 'realty.town',
                    label: 'City'
                },
                {
                    key: 'realty.supplier',
                    label: 'Supplier',
                    fn: function (value, object, key) {
                        return getSupplierName(object);
                    }
                },
                {
                    key: 'stepValue',
                    label: 'Status',
                    fn: function (value, object, key) {
                        var step = Constants.getProcessStep(object.step);
                        if (object.stepB) {
                            step += ', ' + Constants.getProcessStep(object.stepB);
                        }
                        return step;
                    }
                },
                {
                    key: 'step',
                    label: 'Step',
                },
                {
                    key: 'stepB',
                    label: 'StepB',
                },
                {
                    price: 'realty.price',
                    label: 'Price',
                    fn: function (value, object, key) {
                        return Utils.formatCurrencies(object.realty.price);
                    }
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
    }
});

Template.selectFilter.created = function() {
    this.filter = new ReactiveTable.Filter('select-filter', ['step']);
    this.filterB = new ReactiveTable.Filter('select-filterB', ['stepB']);
};

Template.selectFilter.onRendered(function () {
    $('.selectpicker').selectpicker('selectAll');
    $('.selectpicker').selectpicker('refresh');
});

Template.selectFilter.helpers({
    getAllSteps: function() {
        return Constants.steps;
    }
});

Template.selectFilter.events({
    'changed.bs.select	.select-filter, hidden.bs.select .select-filter, refreshed.bs.select .select-filter': function(event, template) {
        // console.log('$(event.target).val() = ' + $(event.target).val());
        var result = $(event.target).val(),
            updatedArray = _.map(result, function(item){
                return parseInt(item);
            }),
            filterString;
        if (result === null) {
            template.filter.set('-1');
            template.filterB.set('-1');
        } else {
            filterString = result.join(" ");
            template.filter.set(filterString);
            template.filterB.set(filterString);
        }
        Session.set('SelectedStep', updatedArray);
    }
});

Template.selectFilterStep.helpers({
    getIndex: function(step) {
        return index = _.indexOf(Constants.steps, step);
    }
});


function getSupplierName(doc) {
    var supplierId = doc.realty.supplier,
        supplier = Suppliers.findOne({
            _id: supplierId
        });
    if (supplier) {
        return supplier.displayName;
    }
}