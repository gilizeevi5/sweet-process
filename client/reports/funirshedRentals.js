import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

Template.FurnishedRentals.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processes');
    });
    this.showAll=new ReactiveVar(false);
};

Template.FurnishedRentals.helpers({
    getCollection: function() {
        let collection;
        if (Template.instance().showAll.get()) {
            collection = Processes.find({ 'step':{$gte: 50, }, 'realty.propertyType': {$not: 'Building'}, 'realty.town': /berlin/i }).fetch();
        } else {
            collection = Processes.find({ 'step':{$gte: 50, }, 'realty.propertyType': {$not: 'Building'}, 'management.isFurnished': true, 'realty.town': /berlin/i }).fetch();
        }
        return collection;
    },
    showAll: function(){
        return Template.instance().showAll.get();
    },
    getSettings: function() {
        var collection = Template.FurnishedRentals.__helpers.get('getCollection').call();
        return {
            collection: collection,
            rowsPerPage: 400,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            showFilter: false,
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'management.isFurnished',
                    label: 'Is Furnished'
                },
                {
                    key: 'management.tenant.actualRentBrutto',
                    label: 'Rent brutto'
                },
                {
                    key: 'management.managementCompany.housegeld',
                    label: 'Hausgeld'
                },
                {
                    key: 'management.managementFee',
                    label: 'Tenant mgmt. fee'
                },
                {
                    key: 'management.rentalFee',
                    label: 'Rental fee'
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
    }
});
Template.FurnishedRentals.events({
    'click .js-showAll'(event, template) {
        var showAll = template.showAll.get();
        template.showAll.set(!showAll);
    }
});