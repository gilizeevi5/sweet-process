import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';

Template.Reports.created=function(){
    Tracker.autorun(function() {
    });
};

Template.Reports.events({
});

Template.Reports.helpers({
});

Template.Reports.rendered = function(){
    if ($('#reports-button').length === 0) {
        Meteor.setTimeout(function(){
            $('.active').removeClass('active');
            $('#reports-button').addClass('active');
        }, 500);
    } else {
        $('.active').removeClass('active');
        $('#reports-button').addClass('active');
    }
};