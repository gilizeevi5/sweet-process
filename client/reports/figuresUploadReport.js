import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

var listener;
Template.FiguresUploadReport.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processes');
        self.subscribe('propertyManagementCompanies');
    });
    this.location = new ReactiveVar('All');
    listener = function(data) {
        this.location.set(data.location);
    };
    Event.on('location-filter-change', Meteor.bindEnvironment(listener, null, this));
};

Template.FiguresUploadReport.destroyed = function() {
    Event.removeListener('location-filter-change', listener);
};

Template.FiguresUploadReport.helpers({
    location: function(){
        return Template.instance().location.get();
    },
    getCollection: function() {
        let collection;
        switch (Template.instance().location.get()) {
            case 'All':
                collection = Processes.find({}).fetch();
                break;
            case 'Berlin':
                collection = Processes.find({ 'realty.town': /^berlin$/i}).fetch();
                break;
            case 'NRW':
                collection = Processes.find({ 'realty.town': {$not: /^berlin$/i}}).fetch();
                break;
            default:
        }
        return collection;
    },
    getSettings: function() {
        return {
            collection: Template.ProcessStatusReport.__helpers.get('getCollection').call(),
            rowsPerPage: 400,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            filters: ['select-filter', 'select-filterB'],
            filterOperator: "$or",
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.email',
                    label: 'Email'
                },
                {
                    key: 'realty.referenceNumber',
                    label: 'Ref #'
                },
                {
                    key: 'realty.propertyType',
                    label: 'Property Type'
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'realty.town',
                    label: 'City'
                },
                {
                    key: 'stepValue',
                    label: 'Status',
                    fn: function (value, object, key) {
                        var step = Constants.getProcessStep(object.step);
                        if (object.stepB) {
                            step += ', ' + Constants.getProcessStep(object.stepB);
                        }
                        return step;
                    }
                },
                {
                    key: 'step',
                    label: 'Step',
                },
                {
                    key: 'stepB',
                    label: 'StepB',
                },
                {
                    key: 'management.managementCompany.details',
                    label: 'Property Management Company',
                    fn: function(value, object, key) {
                        if (object.management && object.management.managementCompany && object.management.managementCompany.details) {
                            var mgmtCompany = PropertyManagementCompanies.findOne({_id: object.management.managementCompany.details});
                            return mgmtCompany.name;
                        }
                    }
                },
                {
                    key: 'realty.tenant',
                    label: 'Primary tenant',
                    fn: function (value, object, key) {
                        var tenantName;
                        if (object.management.tenant) {
                            tenantName = object.management.tenant.firstName + ' ' + object.management.tenant.lastName;
                        }
                        return tenantName;
                    }
                },
                {
                    key: 'management.tenant.email',
                    label: 'Tenant email'
                },
                {
                    key: 'management.tenant.phoneNumber',
                    label: 'Tenant phone'
                },
                {
                    key: 'management.tenant.actualRentBrutto',
                    label: 'Rent brutto'
                },
                {
                    key: 'management.tenant.actualRentNetto',
                    label: 'Rent netto'
                },
                {
                    key: 'management.tenant.lastIncreaseDate',
                    label: 'Last rent increase',
                    fn: function (value, object, key) {
                        var date;
                        if (object.management.tenant) {
                            date = moment(object.management.tenant.lastIncreaseDate).format('DD/MM/YYYY');
                        }
                        return date;
                    }
                },
                {
                    key: 'management.tenant.deposit',
                    label: 'Deposit'
                },
                {
                    key: 'realty.rentInClientsOffer',
                    label: 'Rent in client offer'
                },
                {
                    key: 'management.managementCompany.housegeld',
                    label: 'Hausgeld'
                },
                {
                    key: 'realty.buildingManagementFee',
                    label: 'Property Management Fee'
                },
                {
                    key: 'realty.reserveFund',
                    label: 'Reserve fund'
                },
                {
                    key: 'management.handoverDate',
                    label: 'Handover date',
                    fn: function (value, object, key) {
                        var date;
                        if (object.management.handoverDate) {
                            date = moment(object.management.handoverDate).format('DD/MM/YYYY');
                        }
                        return date;
                    }
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Street',
                    hidden: true
                },
                {
                    key: 'realty.addressNumber',
                    label: 'Number',
                    hidden: true
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
    }
});