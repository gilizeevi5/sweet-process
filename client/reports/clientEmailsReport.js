import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

Template.ClientEmails.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processes');
    });
};

Template.ClientEmails.helpers({
    getSettings: function() {
        return {
            collection: Processes.find({}).fetch(),
            rowsPerPage: 400,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            filters: ['select-filter', 'select-filterB'],
            filterOperator: "$or",
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.email',
                    label: 'Email'
                },
                {
                    key: 'client2.firstName',
                    label: 'Second client name',
                    fn: function (value, object, key) {
                        var name = '';
                        if (object.client2.lastName) {
                            name = object.client2.lastName + ' ' + object.client2.firstName;
                        }
                        return name;
                    }
                },
                {
                    key: 'client2.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'client2.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'client2.email',
                    label: 'Email'
                },
                {
                    key: 'client3.firstName',
                    label: 'Third client name',
                    fn: function (value, object, key) {
                        var name = '';
                        if (object.client3.lastName) {
                            name = object.client3.lastName + ' ' + object.client3.firstName;
                        }
                        return name;                    }
                },
                {
                    key: 'client3.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'client3.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'client3.email',
                    label: 'Email'
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
    }
});