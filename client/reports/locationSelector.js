import { Template } from 'meteor/templating';

Template.LocationSelector.events({
    'change .js-location-all'(event, template) {
        Event.emit('location-filter-change', {
            location: 'All'
        });
    },
    'change .js-location-berlin'(event, template) {
        Event.emit('location-filter-change', {
            location: 'Berlin'
        });
    },
    'change .js-location-nrw'(event, template) {
        Event.emit('location-filter-change', {
            location: 'NRW'
        });
    }
});