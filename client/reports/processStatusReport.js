import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

var listener;
Template.ProcessStatusReport.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processes');
        self.subscribe('suppliers');
    });
    this.location = new ReactiveVar('All');
    listener = function(data) {
        this.location.set(data.location);
    };
    Event.on('location-filter-change', Meteor.bindEnvironment(listener, null, this));
};

Template.ProcessStatusReport.destroyed = function() {
    Event.removeListener('location-filter-change', listener);
};

Template.ProcessStatusReport.helpers({
    location: function(){
        return Template.instance().location.get();
    },
    getCollection: function() {
        let collection;
        switch (Template.instance().location.get()) {
            case 'All':
                collection = Processes.find({}).fetch();
                break;
            case 'Berlin':
                collection = Processes.find({ 'realty.town': /^berlin$/i}).fetch();
                break;
            case 'NRW':
                collection = Processes.find({ 'realty.town': {$not: /^berlin$/i}}).fetch();
                break;
            default:
        }
        return collection;
    },
    getSettings: function() {
        return {
            collection: Template.ProcessStatusReport.__helpers.get('getCollection').call(),
            rowsPerPage: 50,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.referenceNumber',
                    label: 'Ref #'
                },
                {
                    key: 'realty.propertyType',
                    label: 'Property Type'
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'realty.town',
                    label: 'City'
                },
                {
                    key: 'realty.supplier',
                    label: 'Supplier',
                    fn: function (value, object, key) {
                        return getSupplierName(object);
                    }
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Street',
                    hidden: true
                },
                {
                    key: 'realty.addressNumber',
                    label: 'Number',
                    hidden: true
                },
                {
                    key: 'stepValue',
                    label: 'Status',
                    fn: function (value, object, key) {
                        var step = Constants.getProcessStep(object.step);
                        if (object.stepB) {
                            step += ', ' + Constants.getProcessStep(object.stepB);
                        }
                        return step;
                    }
                },
                {
                    key: 'step',
                    label: 'step'
                },
                {
                    key: 'log.date',
                    label: 'Status last updated',
                    fn: function (value, object, key) {
                        var result,
                            stepTimeB,
                            stepTime = _.find(object.log, function (logItem) {
                                return logItem.index === object.step - 1;
                            });
                            if (stepTime) {
                                result = moment(stepTime.date).format('D MMMM YYYY, HH:mm');
                            }
                        if (object.stepB) {
                            stepBTime = _.find(object.log, function (logItem) {
                                return logItem.index === object.stepB - 1;
                            });
                            if (stepBTime) {
                                result += '; ' + moment(stepBTime.date).format('D MMMM YYYY, HH:mm');
                            }
                        }
                        return result;
                    }
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
    }
});

function getSupplierName(doc) {
    var supplierId = doc.realty.supplier,
        supplier = Suppliers.findOne({
            _id: supplierId
        });
    if (supplier) {
        return supplier.displayName;
    }
}