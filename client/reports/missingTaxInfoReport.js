import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

var listener;
Template.MissingTaxInfoReport.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processes');
        self.subscribe('propertyManagementCompanies');
    });
    this.location = new ReactiveVar('All');
    listener = function(data) {
        this.location.set(data.location);
    };
    Event.on('location-filter-change', Meteor.bindEnvironment(listener, null, this));
};

Template.MissingTaxInfoReport.destroyed = function() {
    Event.removeListener('location-filter-change', listener);
};

Template.MissingTaxInfoReport.helpers({
    location: function(){
        return Template.instance().location.get();
    },
    getCollection: function() {
        let collection;
        switch (Template.instance().location.get()) {
            case 'All':
                collection = Processes.find({ 'step':{$gte: 50, }, 'realty.propertyType': {$not: 'Building'}}).fetch();
                break;
            case 'Berlin':
                collection = Processes.find({ $and:[{ 'realty.town': /^berlin$/i}, { 'step':{$gte: 50, }, 'realty.propertyType': {$not: 'Building'} }]}).fetch();
                break;
            case 'NRW':
                collection = Processes.find({ $and:[{ 'realty.town': {$not: /^berlin$/i}}, { 'step':{$gte: 50, }, 'realty.propertyType': {$not: 'Building'} }]}).fetch();
                break;
            default:
        }
        return collection;
    },
    getSettings: function() {
        return {
            collection: Template.ProcessStatusReport.__helpers.get('getCollection').call(),
            rowsPerPage: 400,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            filters: ['select-filter', 'select-filterB'],
            filterOperator: "$or",
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'management.managementCompany.details',
                    label: 'Property Management Company',
                    fn: function(value, object, key) {
                        if (object.management && object.management.managementCompany && object.management.managementCompany.details) {
                            var mgmtCompany = PropertyManagementCompanies.findOne({_id: object.management.managementCompany.details});
                            return mgmtCompany.name;
                        }
                    }
                },
                {
                    key: 'management.taxNumber',
                    label: 'Steuernummer',
                },
                {
                    key: 'management.finanzamtName',
                    label: 'Finanzamt Name',
                },
                {
                    key: 'management.finanzamtEmail',
                    label: 'Finanzamt Email',
                },
                {
                    key: 'management.quarterlyPayment',
                    label: 'Quarterly Payment',
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
    }
});