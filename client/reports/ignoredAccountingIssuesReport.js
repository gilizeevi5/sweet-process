import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';

Template.IgnoredAccountingIssuesReport.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('ignoredAccountingIssues');
    });
    this.showAll=new ReactiveVar(false);
};

Template.IgnoredAccountingIssuesReport.helpers({
    getSettings: function() {
        var collection = IgnoredAccountingIssues.find({}).fetch();
        return {
            collection: collection,
            rowsPerPage: 400,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            showFilter: false,
            fields: [
                {
                    key: 'executionDate',
                    label: 'Execution Date'
                },
                {
                    key: 'valueDate',
                    label: 'Value Date'
                },
                {
                    key: 'payee',
                    label: 'Payee'
                },
                {
                    key: 'payer',
                    label: 'Payer'
                },
                {
                    key: 'accountNumber',
                    label: 'accountNumber'
                },
                {
                    key: 'IBAN',
                    label: 'IBAN'
                },
                {
                    key: 'purpose',
                    label: 'Purpose'
                },
                {
                    key: 'sum',
                    label: 'Sum'
                },
                {
                    key: 'op',
                    label: 'Operation'
                },
                {
                    key: '_id',
                    label: 'Undo ignore',
                    fn: function (value, object, key) {
                        return 'Undo';
                    },
                    cellClass: function(value, object) {
                        return 'js-undo-ignore';
                    }
                }
            ]
        };
    }
});

Template.IgnoredAccountingIssuesReport.events({
    'click .reactive-table tbody tr': function (event) {
        var self = this;
        event.preventDefault();
        event.stopPropagation();
        if (event.target.className.includes('js-undo-ignore')) {
            IgnoredAccountingIssues.remove({_id: self._id});
        }
    }
});