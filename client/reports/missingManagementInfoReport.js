import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

var listener;
Template.MissingManagementInfoReport.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processes');
        self.subscribe('propertyManagementCompanies');
    });
    this.location = new ReactiveVar('All');
    listener = function(data) {
        this.location.set(data.location);
    };
    Event.on('location-filter-change', Meteor.bindEnvironment(listener, null, this));
};

Template.MissingManagementInfoReport.destroyed = function() {
    Event.removeListener('location-filter-change', listener);
};

Template.MissingManagementInfoReport.helpers({
    location: function(){
        return Template.instance().location.get();
    },
    getCollection: function() {
        let collection;
        switch (Template.instance().location.get()) {
            case 'All':
                collection = Processes.find({ 'step':{$gte: 50, }, 'realty.propertyType': {$not: 'Building'}}).fetch();
                break;
            case 'Berlin':
                collection = Processes.find({ $and:[{ 'realty.town': /^berlin$/i}, { 'step':{$gte: 50, }, 'realty.propertyType': {$not: 'Building'} }]}).fetch();
                break;
            case 'NRW':
                collection = Processes.find({ $and:[{ 'realty.town': {$not: /^berlin$/i}}, { 'step':{$gte: 50, }, 'realty.propertyType': {$not: 'Building'} }]}).fetch();
                break;
            default:
        }
        return collection;
    },
    getSettings: function() {
        return {
            collection: Template.ProcessStatusReport.__helpers.get('getCollection').call(),
            rowsPerPage: 400,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            filters: ['select-filter', 'select-filterB'],
            filterOperator: "$or",
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'management.managementCompany.details',
                    label: 'Property Management Company',
                    fn: function(value, object, key) {
                        if (object.management && object.management.managementCompany && object.management.managementCompany.details) {
                            var mgmtCompany = PropertyManagementCompanies.findOne({_id: object.management.managementCompany.details});
                            return mgmtCompany.name;
                        }
                    }
                },
                {
                    key: 'realty.tenant',
                    label: 'Primary tenant',
                    fn: function (value, object, key) {
                        var tenantName;
                        if (object.management.tenant) {
                            tenantName = object.management.tenant.firstName + ' ' + object.management.tenant.lastName;
                        }
                        return tenantName;
                    }
                },
                {
                    key: 'management.tenant.email',
                    label: 'Tenant email'
                },
                {
                    key: 'management.tenant.phoneNumber',
                    label: 'Tenant phone'
                },
                {
                    key: 'management.tenant.teanantIBAN',
                    label: 'Tenant IBAN'
                },
                {
                    key: 'management.tenant.actualRentBrutto',
                    label: 'Rent brutto'
                },
                {
                    key: 'management.managementCompany.housegeld',
                    label: 'Hausgeld'
                },
                {
                    key: 'management.managementFee',
                    label: 'Tenant Management Fee'
                },
                {
                    key: 'management.rentalFee',
                    label: 'Rental fee'
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
    }
});