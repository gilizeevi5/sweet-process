import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

Template.FirstReminderAction.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
        self.subscribe('configurations');
    });
};

Template.FirstReminderAction.helpers({
    process: function () {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getDefaultMonthRentDue: function() {
        var lastMonth = moment(),
            lastMonthString = lastMonth.subtract(1, 'month').format('MMMM YYYY').toString();
        return lastMonthString;
    },
    getDefaultRentDueAmount: function() {
        var id = FlowRouter.getParam('id'),
        doc = Processes.findOne({_id: id});
        if (doc) {
            return doc.management.tenant.actualRentBrutto;
        }
    },
    getDefaultDeadlineToPay: function() {
        var sevenDaysFromNow = moment().add(7, 'days');
        return sevenDaysFromNow.format("YYYY-MM-DD").toString();
    },
    getDefaultLateFee: function() {
        var config = Configurations.findOne();
        return config.lateFee;
    }
});

Template.FirstReminderAction.events({
    'click .js-cancel':function() {
        var id = FlowRouter.getParam('id');
        FlowRouter.go('managementActions', {id: id});
    }
});

AutoForm.addHooks(['FirstReminderForm'], {
    beginSubmit: function() {
        $('#submitBtn').attr("disabled", "disabled").hide();
        $('#cancelBtn').attr("disabled", "disabled").hide();
        Alerts.defaultOptions.autoHide = false;
        Alerts.add('Preparing the letter...', 'warning');
    },
    onError: function() {
        $('#submitBtn').attr("disabled", "").show();
        $('#cancelBtn').attr("disabled", "").show();
    },
    onSuccess: function() {
        var id = FlowRouter.getParam('id');
        Meteor.call('sendFirstReminder', id, function(error, result){
            if (error) {
                Alerts.add('ERROR: ' + error.reason, 'danger');
                console.error('ERROR, error = ' + JSON.stringify(error));
            } else {
                Alerts.defaultOptions.autoHide = true;
                Alerts.defaultOptions.fadeOut = 1200;
                Alerts.add('Reminder sent successfully', 'success');
                Meteor.setTimeout(function(id){
                    FlowRouter.go('management', {id: id});
                }, 1500)
            }
        });
    }
});