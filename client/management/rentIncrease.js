import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

Template.RentIncreaseAction.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
};

Template.RentIncreaseAction.helpers({
    process: function () {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getEmail: function() {
        return Constants.senders.management.email;
    },
    getRentBrutto: function() {
        var id = FlowRouter.getParam('id'),
            process = Processes.findOne({_id: id});
        return process.management.tenant.actualRentBrutto;
    },
    getRentBruttoDate: function() {
        var id = FlowRouter.getParam('id'),
            process = Processes.findOne({_id: id});
        return moment(process.management.tenant.actualRentBruttoDate).format('YYYY-MM-DD')
    }
});

Template.RentIncreaseAction.events({
    'click .js-cancel':function() {
        var id = FlowRouter.getParam('id');
        FlowRouter.go('managementActions', {id: id});
    },
    'click .js-create':function() {
        var id = FlowRouter.getParam('id'),
            process = Processes.findOne({_id: id}),
            payload = {
                id: id,
                email: $('#emailInput').val(),
                rentBrutto: $('#rentBruttoInput').val(),
                rentBurttoDate: $('#rentBruttoDateInput').val(),
                createLetter: $('#createLetterInput').val()
            };
        if (parseFloat(payload.rentBrutto) === process.management.tenant.actualRentBrutto) {
            Alerts.add('ERROR: Updated rent brutto, has to be different then the one saved on server', 'danger');
            console.error('ERROR, error = Updated rent brutto, has to be different then the one saved on server');
            $('#createBtn').attr("disabled", false).show();
            $('#cancelBtn').attr("disabled", false).show();
        } else {
            $('#createBtn').attr("disabled", true).hide();
            $('#cancelBtn').attr("disabled", true).hide();
            Alerts.defaultOptions.autoHide = false;
            Alerts.add('Preparing the letter...', 'warning');
            Meteor.call('createRentIncreaseLetter', payload, function(error, result){
                if (error) {
                    Alerts.add('ERROR: ' + error.reason, 'danger');
                    console.error('ERROR, error = ' + JSON.stringify(error));
                    $('#createBtn').attr("disabled", false).show();
                    $('#cancelBtn').attr("disabled", false).show();
                } else {
                    Alerts.defaultOptions.autoHide = true;
                    Alerts.defaultOptions.fadeOut = 1200;
                    Alerts.add('Reminder sent successfully', 'success');
                    Meteor.setTimeout(function(id){
                        FlowRouter.go('management', {id: id});
                    }, 1500)
                }
            });
        }
    }
});