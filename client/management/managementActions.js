import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

Template.ManagementActions.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
};

Template.ManagementActions.helpers({
    process: function () {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    }
});

Template.ManagementActions.events({
    'click .js-mgmtAction-selfUseLetter':function() {
        if (window.confirm("Are you sure you want to create this letter?")) {
            var id = FlowRouter.getParam('id');
            Alerts.defaultOptions.autoHide = false;
            Alerts.add('Preparing the letter...', 'warning');
            Meteor.call('sendSelfUseLetter', id, function(error, result){
                if (error) {
                    Alerts.add('ERROR: ' + error.reason, 'danger');
                    console.error('ERROR, error = ' + JSON.stringify(error));
                } else {
                    Alerts.defaultOptions.autoHide = true;
                    Alerts.defaultOptions.fadeOut = 1200;
                    Alerts.add('Reminder sent successfully', 'success');
                    Meteor.setTimeout(function(id){
                        FlowRouter.go('management', {id: id});
                    }, 1500)
                }
            });
        };
    }
});