import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

Template.Management.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processes');
        self.subscribe('propertyManagementCompanies');
    });
};

Template.Management.helpers({
    getSettings: function() {
        return {
            collection: Processes.find({ 'step':{$gte: 50, }, 'realty.propertyType': {$not: 'Building'} }).fetch(),
            rowsPerPage: 200,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Street',
                    hidden: true
                },
                {
                    key: 'realty.addressNumber',
                    label: 'Number',
                    hidden: true
                },
                {
                    key: 'management.tenant.firstName',
                    label: 'Primary Tenant',
                    fn: function (value, object, key) {
                        return object.management.tenant.firstName + ' ' + object.management.tenant.lastName;
                    }
                },
                {
                    key: 'management.tenant.firstName',
                    label: 'Primary tenant first name',
                    hidden: true
                },
                {
                    key: 'management.tenant.lastName',
                    label: 'Primary tenant last name',
                    hidden: true
                },
                {
                    key: 'management.secondTenant.firstName',
                    label: 'Second Tenant',
                    fn: function (value, object, key) {
                        return object.management.secondTenant.firstName + ' ' + object.management.secondTenant.lastName;
                    }
                },
                {
                    key: 'management.secondTenant.firstName',
                    label: 'Secondary tenant first name',
                    hidden: true
                },
                {
                    key: 'management.secondTenant.lastName',
                    label: 'Secondary tenant last name',
                    hidden: true
                },
                {
                    key: 'management.secondTenant.lastName',
                    label: 'Secondary tenant last name',
                    hidden: true
                },
                {
                    key: 'management.managementCompany.details',
                    label: 'Property Management Company',
                    fn: function(value, object, key) {
                        if (object.management && object.management.managementCompany && object.management.managementCompany.details) {
                            var mgmtCompany = PropertyManagementCompanies.findOne({_id: object.management.managementCompany.details});
                            return mgmtCompany.name;
                        }
                    }
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                },
                {
                    key: '_id',
                    label: 'Actions',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + "/managementActions>Actions</a>");
                    }
                }
            ]
        };
    }
});

Template.Management.rendered = function(){
    if ($('#management-button').length === 0) {
        Meteor.setTimeout(function(){
            $('.active').removeClass('active');
            $('#management-button').addClass('active');
        }, 500);
    } else {
        $('.active').removeClass('active');
        $('#management-button').addClass('active');
    }
};