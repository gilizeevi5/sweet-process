import _ from 'lodash';
import { Processes } from '../../imports/collections/process/Processes';;
import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';

Template.Accounting.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processesWithAccounting', function(){
            Session.set('miniCollection', Processes.find({}).fetch());
        });
        self.subscribe('propertyManagementCompanies');
        self.subscribe('accountingIssues');
        self.subscribe('ignoredAccountingIssues');
        self.subscribe('linkedAccountingIssues');
    });
    this.uploading = new ReactiveVar(false);
    this.csvData = new ReactiveVar(false);
    this.paymentInfo = new ReactiveVar(false);
    Session.set('paymentInfo', false);
    Session.set('selectedProcessId', false);
    Session.set('accountingYear', parseInt($('#yearSelector').val()) || Constants.defaultAccountingYearIndex);
};

Template.Accounting.onDestroyed(function () {
    delete Session.keys['miniCollection'];
});

Template.Accounting.helpers({
    isUploading() {
        return Template.instance().uploading.get();
    },
    csvData() {
        return Template.instance().csvData.get();
    },
    getAccountingIssues: function() {
        return AccountingIssues.find({});
    },
    getAccountingSize: function() {
        return AccountingIssues.find({}).fetch().length;
    },
    getCollection: function() {
        return Processes.find({}).fetch()
    },
    getTenantsSettings: function() {
        var settings = {
            rowsPerPage: 400,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            fields: [
                {
                    key: 'isAccountingReady',
                    label: 'Ready?',
                    cellClass: function(value, object) {
                        var cellClass;
                        if (object.isAccountingReady) {
                            cellClass = 'isAccountingReady ready'
                        } else {
                            cellClass = 'isAccountingReady notReady'
                        }
                        return cellClass;
                    }

                },
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    },
                    sortOrder: 0
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Street',
                    hidden: true
                },
                {
                    key: 'realty.addressNumber',
                    label: 'Number',
                    hidden: true
                },
                {
                    key: 'management.tenant.firstName',
                    label: 'Primary Tenant',
                    fn: function (value, object, key) {
                        var name = " ";
                        if (object.management.tenant && object.management.tenant.firstName && object.management.tenant.lastName) {
                            name = object.management.tenant.firstName + ' ' + object.management.tenant.lastName;
                        }
                        return name;                    }
                },
                {
                    key: 'management.tenant.firstName',
                    label: 'Primary tenant first name',
                    hidden: true
                },
                {
                    key: 'management.tenant.lastName',
                    label: 'Primary tenant last name',
                    hidden: true
                },
                {
                    key: 'management.secondTenant.firstName',
                    label: 'Second Tenant',
                    fn: function (value, object, key) {
                        var name = " ";
                        if (object.management.secondTenant && object.management.secondTenant.lastName) {
                            name = object.management.secondTenant.firstName + ' ' + object.management.secondTenant.lastName;
                        }
                        return name;
                    }
                },
                {
                    key: 'management.secondTenant.firstName',
                    label: 'Secondary tenant first name',
                    hidden: true
                },
                {
                    key: 'management.secondTenant.lastName',
                    label: 'Secondary tenant last name',
                    hidden: true
                },
                {
                    key: 'accounting[getYear()].tenantPayments.0.value',
                    label: 'Jan \'18',
                    cellClass: function(value, object) {
                        return getTenantCellClass(0, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(0, object);
                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.1.value',
                    label: 'Feb',
                    cellClass: function(value, object) {
                        return getTenantCellClass(1, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(1, object);
                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.2.value',
                    label: 'Mar',
                    cellClass: function(value, object) {
                        return getTenantCellClass(2, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(2, object);
                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.3.value',
                    label: 'Apr',
                    cellClass: function(value, object) {
                        return getTenantCellClass(3, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(3, object);
                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.4.value',
                    label: 'May',
                    cellClass: function(value, object) {
                        return getTenantCellClass(4, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(4, object);
                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.5.value',
                    label: 'Jun',
                    cellClass: function(value, object) {
                        return getTenantCellClass(5, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(5, object);
                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.6.value',
                    label: 'Jul',
                    cellClass: function(value, object) {
                        return getTenantCellClass(6, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(6, object);

                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.7.value',
                    label: 'Aug',
                    cellClass: function(value, object) {
                        return getTenantCellClass(7, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(7, object);

                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.8.value',
                    label: 'Sep',
                    cellClass: function(value, object) {
                        return getTenantCellClass(8, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(8, object);
                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.9.value',
                    label: 'Oct',
                    cellClass: function(value, object) {
                        return getTenantCellClass(9, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(9, object);
                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.10.value',
                    label: 'Nov',
                    cellClass: function(value, object) {
                        return getTenantCellClass(10, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(10, object);
                    }
                },
                {
                    key: 'accounting[getYear()].tenantPayments.11.value',
                    label: 'Dec',
                    cellClass: function(value, object) {
                        return getTenantCellClass(11, value, object);
                    },
                    fn: function(value, object, key) {
                        return getTenantCellValue(11, object);
                    }
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
        settings.fields.splice(12, 0, getReusableSettings()[0], getReusableSettings()[1], getReusableSettings()[2], getReusableSettings()[3], getReusableSettings()[4]);
        return settings;
    },
    getOwnersSettings: function() {
        var settings;
        settings =  {
            rowsPerPage: 400,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Street',
                    hidden: true
                },
                {
                    key: 'realty.addressNumber',
                    label: 'Number',
                    hidden: true
                },
                {
                    key: 'accounting.years.$.ownerPayments.0.value',
                    label: 'Jan \'18',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(0, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(0, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.1.value',
                    label: 'Feb',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(1, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(1, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.2.value',
                    label: 'Mar',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(2, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(2, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.3.value',
                    label: 'Apr',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(3, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(3, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.4.value',
                    label: 'May',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(4, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(4, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.5.value',
                    label: 'Jun',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(5, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(5, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.6.value',
                    label: 'Jul',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(6, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(6, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.7.value',
                    label: 'Aug',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(7, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(7, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.8.value',
                    label: 'Sep',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(8, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(8, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.9.value',
                    label: 'Oct',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(9, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(9, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.10.value',
                    label: 'Nov',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(10, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(10, object);
                    }
                },
                {
                    key: 'accounting.years.$.ownerPayments.11.value',
                    label: 'Dec',
                    cellClass: function(value, object) {
                        return getOwnerCellClass(11, value, object);
                    },
                    fn: function(value, object, key) {
                        return getOwnerCellValue(11, object);
                    }
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
        settings.fields.splice(6, 0, getReusableSettings()[0], getReusableSettings()[1], getReusableSettings()[2], getReusableSettings()[3], getReusableSettings()[4]);
        return settings;
    },
    getManagementSettings: function() {
        var settings;
        settings =  {
            rowsPerPage: 400,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Street',
                    hidden: true
                },
                {
                    key: 'realty.addressNumber',
                    label: 'Number',
                    hidden: true
                },
                {
                    key: 'management.managementCompany.details',
                    label: 'Property Management Company',
                    fn: function(value, object, key) {
                        if (object.management && object.management.managementCompany && object.management.managementCompany.details) {
                            var mgmtCompany = PropertyManagementCompanies.findOne({_id: object.management.managementCompany.details});
                            if (mgmtCompany) {
                                return mgmtCompany.name;
                            }
                        }
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.0.value',
                    label: 'Jan \'18',
                    cellClass: function(value, object) {
                        return getManagementCellClass(0, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(0, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.1.value',
                    label: 'Feb',
                    cellClass: function(value, object) {
                        return getManagementCellClass(1, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(1, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.2.value',
                    label: 'Mar',
                    cellClass: function(value, object) {
                        return getManagementCellClass(2, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(2, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.3.value',
                    label: 'Apr',
                    cellClass: function(value, object) {
                        return getManagementCellClass(3, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(3, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.4.value',
                    label: 'May',
                    cellClass: function(value, object) {
                        return getManagementCellClass(4, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(4, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.5.value',
                    label: 'Jun',
                    cellClass: function(value, object) {
                        return getManagementCellClass(5, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(5, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.6.value',
                    label: 'Jul',
                    cellClass: function(value, object) {
                        return getManagementCellClass(6, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(6, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.7.value',
                    label: 'Aug',
                    cellClass: function(value, object) {
                        return getManagementCellClass(7, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(7, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.8.value',
                    label: 'Sep',
                    cellClass: function(value, object) {
                        return getManagementCellClass(8, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(8, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.9.value',
                    label: 'Oct',
                    cellClass: function(value, object) {
                        return getManagementCellClass(9, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(9, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.10.value',
                    label: 'Nov',
                    cellClass: function(value, object) {
                        return getManagementCellClass(10, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(10, object);
                    }
                },
                {
                    key: 'accounting.years.$.managementPayments.11.value',
                    label: 'Dec',
                    cellClass: function(value, object) {
                        return getManagementCellClass(11, value, object);
                    },
                    fn: function(value, object, key) {
                        return getManagementCellValue(11, object);
                    }
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
        settings.fields.splice(7, 0, getReusableSettings()[0], getReusableSettings()[1], getReusableSettings()[2], getReusableSettings()[3], getReusableSettings()[4]);
        return settings;
    },
    getLandTaxSettings: function() {
        var settings;
        settings =  {
            rowsPerPage: 400,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Street',
                    hidden: true
                },
                {
                    key: 'realty.addressNumber',
                    label: 'Number',
                    hidden: true
                },
                {
                    key: 'management.taxNumber',
                    label: 'Steuernummer'
                },
                {
                    key: 'management.finanzamtName',
                    label: 'Finanzamt Name'
                },
                {
                    key: 'management.quarterlyPayment',
                    label: 'Quarterly payment',
                    cellClass: function(value, object) {
                        var cellClass;
                        if (object.management && object.management.finanzamtName && object.management.quarterlyPayment) {
                            cellClass = 'editable management.tenant.quarterlyPayment';
                        } else {
                            cellClass = 'management.tenant.quarterlyPayment';
                        }
                        return cellClass;
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.0.value',
                    label: 'Jan \'18',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(0, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(0, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.1.value',
                    label: 'Feb',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(1, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(1, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.2.value',
                    label: 'Mar',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(2, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(2, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.3.value',
                    label: 'Apr',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(3, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(3, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.4.value',
                    label: 'May',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(4, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(4, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.5.value',
                    label: 'Jun',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(5, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(5, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.6.value',
                    label: 'Jul',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(6, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(6, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.7.value',
                    label: 'Aug',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(7, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(7, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.8.value',
                    label: 'Sep',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(8, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(8, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.9.value',
                    label: 'Oct',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(9, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(9, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.10.value',
                    label: 'Nov',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(10, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(10, object);
                    }
                },
                {
                    key: 'accounting.years.$.landTaxPayments.11.value',
                    label: 'Dec',
                    cellClass: function(value, object) {
                        return getLandtaxCellClass(11, value, object);
                    },
                    fn: function(value, object, key) {
                        return getLandTaxCellValue(11, object);
                    }
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return new Spacebars.SafeString("<a href=/process/" + value + ">View</a>");
                    }
                }
            ]
        };
        return settings;
    },
    getGrossRentSum: function() {
        var processes = Processes.find({ 'step':{$gte: 50} }).fetch(),
            overallSum = _.reduce(processes, function(sum, process){
                var val;
                if (process.management && process.management.tenant && process.management.tenant.actualRentBrutto) {
                    val = sum + process.management.tenant.actualRentBrutto;
                } else {
                    val = sum + 0;
                }
                return val;
            }, 0);
        return Utils.formatCurrencies(overallSum);
    },
    getOwnerPaySum: function() {
        var processes = Processes.find({ 'step':{$gte: 50} }).fetch(),
            overallSum = _.reduce(processes, function(sum, process){
                var factor = Math.pow(10, 2),
                    val;
                if (process.management && process.management.managementCompany && process.management.managementCompany.housegeld && process.management.tenant && process.management.tenant.actualRentBrutto) {
                    val = sum + Math.round((process.management.tenant.actualRentBrutto - process.management.managementCompany.housegeld - process.management.managementFee - (process.management.rentalFee || 0)) * factor) / factor;
                } else {
                    val = sum + 0;
                }
                return val;
            }, 0);
        return Utils.formatCurrencies(overallSum);
    },
    getHausgeldSum: function() {
        var processes = Processes.find({ 'step':{$gte: 50} }).fetch(),
            overallSum = _.reduce(processes, function(sum, process){
                var val;
                if (process.management && process.management.managementCompany && process.management.managementCompany.housegeld) {
                    val = sum + process.management.managementCompany.housegeld;
                } else {
                    val = sum + 0;
                }
                return val;
            }, 0);
        return Utils.formatCurrencies(overallSum);
    },
    getTenantMgmtFeeSum: function() {
        var processes = Processes.find({ 'step':{$gte: 50} }).fetch(),
            overallSum = _.reduce(processes, function(sum, process){
                var val;
                if (process.management && process.management.managementFee) {
                    val = sum + process.management.managementFee;
                } else {
                    val = sum + 0;
                }
                return val;
            }, 0);
        return Utils.formatCurrencies(overallSum);
    },
    getRentalFeeSum: function() {
        var processes = Processes.find({ 'step':{$gte: 50} }).fetch(),
            overallSum = _.reduce(processes, function(sum, process){
                var val;
                if (process.management && process.management.rentalFee) {
                    val = sum + process.management.rentalFee;
                } else {
                    val = sum + 0;
                }
                return val;
            }, 0);
        return Utils.formatCurrencies(overallSum);
    }
});

Template.Accounting.events({
    'change #yearSelector' (event, template) {
        Session.set('accountingYear', parseInt($('#yearSelector').val()) || Constants.defaultAccountingYearIndex);
    },
    'click #delete-all-issues' (event, template) {
        Meteor.call('deleteAllAccountingIssues', function(error, result) {
            if (error) {
                Alerts.add('ERROR: ' + error.reason, 'danger');
                console.error('ERROR, error = ' + JSON.stringify(error));
            } else {
                Alerts.defaultOptions.autoHide = true;
                Alerts.defaultOptions.fadeOut = 1200;
                Alerts.add('Issues deleted', 'success');
            }
        });
    },
    'change [name="uploadCSV"]' (event, template) {
        var file = event.target.files[0],
            fr = new FileReader(),
            csvRaw,
            template = Template.instance(),
            issues, quoteChar, delimiter;
        template.uploading.set(true);
        fr.onload = function onTextLoaded() {
            var csvStart,
                csvEnd;
            csvRaw = fr.result;
            if (csvRaw.charAt(0)==="\"") {
                console.log('NEW FORMAT CSV');

                csvStart = csvRaw.indexOf('\"Buchungstag\";\"Valuta\";\"');
                csvEnd = csvRaw.search(/"\d\d.\d\d.\d\d\d\d";;;;;;;;;"Anfangssaldo";/);
                csvRaw = csvRaw.slice(csvStart, csvEnd);
                csvRaw = csvRaw.replace(/(\n,)/, '');
                csvRaw = csvRaw.replace(/\r?\n|\r/g, '');
                csvRaw = csvRaw.replace(/;"H"/g, ';"H"\n');
                csvRaw = csvRaw.replace(/;"S"/g, ';"S"\n');
                csvRaw = csvRaw.replace(/GUTSCHRIFT/g, 'GUTSCHRIFT ');
                csvRaw = csvRaw.replace(/DAUERAUFTRAG/g, 'DAUERAUFTRAG ');
                csvRaw = csvRaw.replace('"Umsatz";', '"Umsatz";op;\n');
                csvRaw = csvRaw.replace('Buchungstag', 'executionDate');
                csvRaw = csvRaw.replace('Valuta', 'valueDate');
                csvRaw = csvRaw.replace(/Auftraggeber\/Zahlungsempf.nger/g, 'payee');
                csvRaw = csvRaw.replace(/Empf.nger\/Zahlungspflichtiger/g, 'payer');
                csvRaw = csvRaw.replace('Konto-Nr.', 'accountNumber');
                csvRaw = csvRaw.replace('Vorgang/Verwendungszweck', 'purpose');
                csvRaw = csvRaw.replace('Kundenreferenz', 'reference');
                csvRaw = csvRaw.replace(/W.hrung/g, 'currency');
                csvRaw = csvRaw.replace('Umsatz', 'sum');
                csvRaw = csvRaw.replace(/ (?=\n)/, '');
            } else {
                console.log('OLD FORMAT CSV');

                csvStart = csvRaw.indexOf('\"Buchungstag;\"\"Valuta\"\";');
                csvEnd = csvRaw.search(/"\d\d.\d\d.\d\d\d\d;;;;;;;;;""Anfangssaldo"";/);
                csvRaw = csvRaw.slice(csvStart, csvEnd);
                csvRaw = csvRaw.replace(/(\n,)/, '');
                csvRaw = csvRaw.replace(/\r?\n|\r/g, '&^');
                csvRaw = csvRaw.replace(/""",,&\^/g, '\n');
                csvRaw = csvRaw.replace(/""",&\^/g, '\n');
                csvRaw = csvRaw.replace(/"""&\^/g, '\n');
                csvRaw = csvRaw.replace(/",,,&\^/g, ' ');
                csvRaw = csvRaw.replace(/,,,&\^/g, '');
                csvRaw = csvRaw.replace(/",,&\^/g, ' ');
                csvRaw = csvRaw.replace(/,,&\^/g, '');
                csvRaw = csvRaw.replace(/",&\^/g, ' ');
                csvRaw = csvRaw.replace(/,&\^/g, '');
                csvRaw = csvRaw.replace(/&\^/g, '');
                csvRaw = csvRaw.replace(/['"]/g, '');
                csvRaw = csvRaw.replace('Umsatz;', 'Umsatz;op;');
                csvRaw = csvRaw.replace('Buchungstag', 'executionDate');
                csvRaw = csvRaw.replace('Valuta', 'valueDate');
                csvRaw = csvRaw.replace(/Auftraggeber\/Zahlungsempf.nger/g, 'payee');
                csvRaw = csvRaw.replace(/Empf.nger\/Zahlungspflichtiger/g, 'payer');
                csvRaw = csvRaw.replace('Konto-Nr.', 'accountNumber');
                csvRaw = csvRaw.replace('Vorgang/Verwendungszweck', 'purpose');
                csvRaw = csvRaw.replace('Kundenreferenz', 'reference');
                csvRaw = csvRaw.replace(/W.hrung/g, 'currency');
                csvRaw = csvRaw.replace('Umsatz', 'sum');
                csvRaw = csvRaw.replace(/ (?=\n)/, '');
            }
            // console.log('csvRaw = ' + csvRaw);template.uploading.set(false);return;
            Papa.parse( csvRaw, {
                header: true,
                delimiter: ';',
                encoding: 'ISO-8859-1',
                quoteChar: '"',
                skipEmptyLines: true,
                complete: function(results, file) {
                    // console.log('results = ' + JSON.stringify(results));template.uploading.set(false);return;
                    _.forEach(results.data, function(item, index){
                        if (item.purpose) {
                            var IBANField = item.purpose.match(/(IBAN:[^\s]+|IBAN: [^\s]+)/),
                                BICField = item.purpose.match(/(BIC: [^\s]+)/);
                            if (IBANField) {
                                if (IBANField[0].split(": ")[1]) {
                                    item.IBAN = IBANField[0].split(": ")[1].replace('BIC:','');
                                } else {
                                    item.IBAN = IBANField[0].split(":")[1].replace('BIC:','');
                                }
                            }
                            if (BICField) {
                                item.BIC = BICField[0].split(": ")[1];
                            }
                        }
                    });
                    issues = updateAccounting(results.data);
                    Meteor.call('updateAccountingIssues', issues, function(error, result) {
                        template.uploading.set(false);
                        if (error) {
                            Alerts.add('ERROR: ' + error.reason, 'danger');
                            console.error('ERROR, error = ' + JSON.stringify(error));
                        } else {
                            Alerts.defaultOptions.autoHide = true;
                            Alerts.defaultOptions.fadeOut = 1200;
                            Alerts.add('Accounting updated successfully', 'success');
                            $('#accounting-top-tabs a[href="#issues"]').tab('show');
                        }
                    });
                }
            });
        };
        fr.readAsText(file);
    },
    'click .reactive-table.accounting tbody tr': function (event) {
        var self = this,
            filter = $('input.reactive-table-input').val(),
            $el = $(event.target),
            val = Number($el.text());
        event.preventDefault();
        event.stopPropagation();
        if (event.target.className.includes('editable')) {
            $el.html('<input type=number value=' + val + '><button class="js-save">Save</button><button class="js-cancel">Cancel</button>');
            $el.removeClass('editable');
            $('button.js-cancel', $el).click(function(event){
                $el.addClass('editable');
                $el.html(val);
            });
            $('button.js-save', $el).click(function(event){
                event.preventDefault();
                event.stopPropagation();
                Session.set( 'filter', filter);
                var newValue = Number($('input', $el).val()),
                    className = $el[0].className,
                    update = {};
                update[className] = newValue;
                Processes.update(self._id, {
                    $set: update
                });
                if (val === newValue) {
                    $el.html(val);
                }
                $('input', $el).remove();
                $('button', $el).remove();
                $el.addClass('editable');
            });
            $(window).click(function() {
                $el.addClass('editable');
                $el.html(val);
            });
        } else if ((!isNaN(val) && val !== 0) || event.target.className.includes('note')) {
            var monthIdx = parseInt($el.attr('class').match(/(?:month)([^\s]+)/gm)[0].replace('month',''));
            switch($('#accounting-bottom-tabs li.active a').text()) {
                case 'Tenants':
                    Session.set('paymentInfo', this.accounting.years[Session.get('accountingYear')].tenantPayments[monthIdx]);
                    break;
                case 'Owners':
                    Session.set('paymentInfo', this.accounting.years[Session.get('accountingYear')].ownerPayments[monthIdx]);
                    break;
                case 'Management':
                    Session.set('paymentInfo', this.accounting.years[Session.get('accountingYear')].managementPayments[monthIdx]);
                    break;
                case 'Land Tax':
                    Session.set('paymentInfo', this.accounting.years[Session.get('accountingYear')].landTaxPayments[monthIdx]);
                    break;
            }
            Session.set('selectedProcessId', this._id);
            $('#paymentInfoModal').modal('show');
        }  else {
            if (event.target.className.includes('notReady')) {
                var $el = $(event.target);
                $el.removeClass('notReady');
                $el.addClass('ready');
                Processes.update(self._id, {
                    $set: {
                        isAccountingReady: true
                    }
                });
            } else if (event.target.className.includes('ready')) {
                var $el = $(event.target);
                $el.removeClass('ready');
                $el.addClass('notReady').text('');
                Processes.update(self._id, {
                    $set: {
                        isAccountingReady: false
                    }
                });
            }
        }
    }
});

Template.Accounting.rendered = function(){
    if ($('#accounting-button').length === 0) {
        Meteor.setTimeout(function(){
            $('.active').removeClass('active');
            $('#accounting-button').addClass('active');
        }, 500);
    } else {
        $('.active').removeClass('active');
        $('#accounting-button').addClass('active');
    }
    if (Session.get('filter')) {
        $('input.reactive-table-input').val(Session.get('filter'));
        delete Session.keys['filter'];
    }
};

Template.reactiveTable.onRendered = function() {
    console.log('rendered');
};

getReusableSettings = function(object) {
    var cellClass = '',
        settings;
    settings = [
        {
            key: 'management.tenant.actualRentBrutto',
            label: 'Rent brutto',
            cellClass: function(value, object) {
                var cellClass;
                if (object.management && object.management.managementCompany && object.management.managementCompany.housegeld !== null && object.management.tenant) {
                    cellClass = 'editable management.tenant.actualRentBrutto';
                } else {
                    cellClass = 'management.tenant.actualRentBrutto';
                }
                return cellClass;
            }
        },
        {
            key: 'management.tenant.actualRentBrutto - management.managementCompany.housegeld - management.managementFee - management.rentalFee',
            label: 'Pay to owner',
            fn: function (value, object, key) {
                var factor = Math.pow(10, 2),
                    value;
                if (object.management && object.management.managementCompany && object.management.managementCompany.housegeld !== null && object.management.tenant) {
                    value = Math.round((object.management.tenant.actualRentBrutto - object.management.managementCompany.housegeld - object.management.managementFee - (object.management.rentalFee || 0)) * factor) / factor;
                }
                return value;
            }
        },
        {
            key: 'management.managementCompany.housegeld',
            label: 'Hausgeld',
            cellClass: function(value, object) {
                var cellClass;
                if (object.management && object.management.managementCompany && object.management.managementCompany.housegeld !== null && object.management.tenant) {
                    cellClass = 'editable management.managementCompany.housegeld';
                } else {
                    cellClass = 'management.managementCompany.housegeld';
                }
                return cellClass;
            }
        },
        {
            key: 'management.managementFee',
            label: 'Tenant mgmt. fee',
            cellClass: function(value, object) {
                var cellClass;
                if (object.management && object.management.managementCompany && object.management.managementCompany.housegeld !== null && object.management.tenant) {
                    cellClass = 'editable management.managementFee';
                } else {
                    cellClass = 'management.managementFee';
                }
                return cellClass;
            }
        },
        {
            key: 'management.rentalFee',
            label: 'Rental fee',
            cellClass: function(value, object) {
                var cellClass;
                if (object.management && object.management.managementCompany && object.management.managementCompany.housegeld !== null && object.management.tenant) {
                    cellClass = 'editable management.rentalFee';
                } else {
                    cellClass = 'management.rentalFee';
                }
                return cellClass;
            },
            fn: function (value, object, key) {
                return object.management.rentalFee || 0;
            }
        }
    ];
    return settings;
};

updateAccounting = function(results) {
    var ignoredIssues = IgnoredAccountingIssues.find({}).fetch(),
        linkedIssues = LinkedAccountingIssues.find({}).fetch();
        deposits = _.filter(results, function(result){
            var isLinked,
                isIgnored = _.find(ignoredIssues, function(ignoredIssue) {
                    return ignoredIssue.payee === result.payee &&
                            ignoredIssue.payer === result.payer &&
                            ignoredIssue.op === result.op &&
                            ignoredIssue.IBAN === result.IBAN &&
                            ignoredIssue.sum === result.sum;
                });
            if (!isIgnored) {
                isLinked = _.find(linkedIssues, function(linkedIssue){
                    return linkedIssue.payee === result.payee &&
                        linkedIssue.payer === result.payer &&
                        linkedIssue.op === result.op &&
                        linkedIssue.IBAN === result.IBAN &&
                        linkedIssue.sum === result.sum &&
                        linkedIssue.executionDate === result.executionDate &&
                        linkedIssue.valueDate === result.valueDate;
                });
            }
            return result.op === 'H' && !isIgnored && !isLinked;
        }),
        withdrawals = _.filter(results, function(result){
            var isLinked,
                isIgnored = _.find(ignoredIssues, function(ignoredIssue) {
                    return ignoredIssue.payee === result.payee &&
                        ignoredIssue.payer === result.payer &&
                        ignoredIssue.op === result.op &&
                        ignoredIssue.IBAN === result.IBAN &&
                        ignoredIssue.sum === result.sum
                });
            if (!isIgnored) {
                isLinked = _.find(linkedIssues, function(linkedIssue){
                    return linkedIssue.payee === result.payee &&
                        linkedIssue.payer === result.payer &&
                        linkedIssue.op === result.op &&
                        linkedIssue.IBAN === result.IBAN &&
                        linkedIssue.sum === result.sum &&
                        linkedIssue.executionDate === result.executionDate &&
                        linkedIssue.valueDate === result.valueDate;
                });
            }
            return result.op === 'S' && !isIgnored && !isLinked;
        }),
        processes = Processes.find({ 'step':{$gte: 50, }, 'realty.propertyType': {$not: 'Building'} }).fetch(),
        depositIssues = [],
        withdrawalIssues = [],
        allIssues = [];
    depositIssues = updateDeposits(deposits, processes);
    withdrawalIssues =  updateWithdrawals(withdrawals, processes);
    allIssues = depositIssues.concat(withdrawalIssues);
    return allIssues;
};
updateDeposits = function(deposits, processes) {
    var issues = [],
        monthIdx,
        tenantPayments,
        payment,
        idx = 0,
        process,
        yearVar;
    _.forEach(deposits, function(deposit) {
        yearVar = parseInt(moment(deposit.executionDate, 'DD.MM.YYYY').year()) - Constants.defaultAccountingYear;
        payment = {
            date: moment(deposit.executionDate, 'DD.MM.YYYY').toISOString(),
            value: Utils.roundWithDecimals(deposit.sum, 2)
        };
        process = undefined;
        process = matchDepositToTenant(deposit, processes);
        if (process) {
            monthIdx = getTransactionMonthAsIndex(deposit.executionDate);
            payment.paymentType = 'PaymentFromTenant';
            payment.period = 'Monthly';
            payment.bankTransaction = deposit;
            payment.monthIdx = monthIdx;
            if (!process.accounting) {
                process.accounting = {};
                process.accounting.years = [];
            }
            if (!process.accounting.years[yearVar]) {
                process.accounting.years[yearVar] = {};
            }
            if (!process.accounting.years[yearVar].tenantPayments) {
                process.accounting.years[yearVar].tenantPayments = [];
            }
            if (!process.accounting.years[yearVar].tenantPayments[monthIdx]) {
                process.accounting.years[yearVar].tenantPayments[monthIdx] = [];
            }
            process.accounting.years[yearVar].tenantPayments[monthIdx].push(payment);
            Processes.update(process._id, {
                $set: {
                    'accounting': process.accounting
                }
            });
            deposit.manualLink = false;
            deposit.linkDate = Date.now();
            deposit.userId = Meteor.userId();
            LinkedAccountingIssues.insert(deposit);
            idx += 1;
        } else {
            issues.push(deposit);
            idx += 1;
        }
    });
    // console.log(issues.length  + ' deposits ISSUES = ' + JSON.stringify(issues));
    return issues;
};

updateWithdrawals = function(withdrawals, processes) {
    var issues = [],
        monthIdx,
        payment,
        idx = 0,
        process,
        yearVar;
    _.forEach(withdrawals, function(withdrawl) {
        yearVar = parseInt(moment(withdrawl.executionDate, 'DD.MM.YYYY').year()) - Constants.defaultAccountingYear;
        payment = {
            date: moment(withdrawl.executionDate, 'DD.MM.YYYY').toISOString(),
            value: Utils.roundWithDecimals(withdrawl.sum, 2)
        };
        payment.bankTransaction = withdrawl;
        process = undefined;
        process = matchWithdrawalToOwner(withdrawl, processes);
        if (process) {
            monthIdx = getTransactionMonthAsIndex(withdrawl.executionDate);
            payment.paymentType ='PaymentToOwner';
            payment.period = 'Monthly';
            payment.monthIdx = monthIdx;
            if (!process.accounting) {
                process.accounting = {};
                process.accounting.years = [];
            }
            if (!process.accounting.years[yearVar]) {
                process.accounting.years[yearVar] = {};
            }
            if (!process.accounting.years[yearVar].ownerPayments) {
                process.accounting.years[yearVar].ownerPayments = [];
            }
            if (!process.accounting.years[yearVar].ownerPayments[monthIdx]) {
                process.accounting.years[yearVar].ownerPayments[monthIdx] = [];
            }
            process.accounting.years[yearVar].ownerPayments[monthIdx].push(payment);
            Processes.update(process._id, {
                $set: {
                    'accounting': process.accounting
                }
            });
            withdrawl.manualLink = false;
            withdrawl.linkDate = Date.now();
            withdrawl.userId = Meteor.userId();
            LinkedAccountingIssues.insert(withdrawl);
            idx += 1;
        } else {
            process = matchWithdrawalToManagementCompany(withdrawl, processes);
            if (process) {
                monthIdx = getTransactionMonthAsIndex(withdrawl.executionDate);
                payment.paymentType ='PaymentToMgmtCompany';
                payment.period = 'Monthly';
                payment.monthIdx = monthIdx;
                if (!process.accounting) {
                    process.accounting = {};
                    process.accounting.years = [];
                }
                if (!process.accounting.years[yearVar]) {
                    process.accounting.years[yearVar] = {};
                }
                if (!process.accounting.years[yearVar].managementPayments) {
                    process.accounting.years[yearVar].managementPayments = [];
                }
                if (!process.accounting.years[yearVar].managementPayments[monthIdx]) {
                    process.accounting.years[yearVar].managementPayments[monthIdx] = [];
                }
                process.accounting.years[yearVar].managementPayments[monthIdx].push(payment);
                Processes.update(process._id, {
                    $set: {
                        'accounting': process.accounting
                    }
                });
                withdrawl.manualLink = false;
                withdrawl.linkDate = Date.now();
                withdrawl.userId = Meteor.userId();
                LinkedAccountingIssues.insert(withdrawl);
                idx += 1;
            } else {
                process = matchWithdrawalToFinanzamt(withdrawl, processes);
                if (process) {
                    monthIdx = getTransactionMonthAsIndex(withdrawl.executionDate);
                    payment.paymentType = 'PaymentToFinanzamt';
                    payment.period = 'Quarterly';
                    payment.monthIdx = monthIdx;
                    if (!process.accounting) {
                        process.accounting = {};
                        process.accounting.years = [];
                    }
                    if (!process.accounting.years[yearVar]) {
                        process.accounting.years[yearVar] = {};
                    }
                    if (!process.accounting.years[yearVar].landTaxPayments) {
                        process.accounting.years[yearVar].landTaxPayments = [];
                    }
                    if (!process.accounting.years[yearVar].landTaxPayments[monthIdx]) {
                        process.accounting.years[yearVar].landTaxPayments[monthIdx] = [];
                    }
                    process.accounting.years[yearVar].landTaxPayments[monthIdx].push(payment);
                    Processes.update(process._id, {
                        $set: {
                            'accounting': process.accounting
                        }
                    });
                    withdrawl.manualLink = false;
                    withdrawl.linkDate = Date.now();
                    withdrawl.userId = Meteor.userId();
                    LinkedAccountingIssues.insert(withdrawl);
                    idx += 1;
                } else {
                    issues.push(withdrawl);
                    idx += 1;
                }
            }
        }
    });
    // console.log(issues.length  + ' withdrawl ISSUES = ' + JSON.stringify(issues));
    return issues;
};

matchWithdrawalToOwner = function(withdrawl, processes) {
    return _.find(processes, function(process) {
        if (process.management.clientIBAN && process.management.clientIBAN === withdrawl.IBAN) {
            var payToOwnerWith2Decimals;
            if (process.management.rentalFee) {
                payToOwnerWith2Decimals = (getRelevantRentBrutto(process, moment(withdrawl.executionDate, 'DD.MM.YYYY').month()) - process.management.managementCompany.housegeld - process.management.managementFee - process.management.rentalFee);
            } else {
                payToOwnerWith2Decimals = (getRelevantRentBrutto(process, moment(withdrawl.executionDate, 'DD.MM.YYYY').month()) - process.management.managementCompany.housegeld - process.management.managementFee);
            }
            // console.log("withdrawl.sum = " + Utils.roundWithDecimals(withdrawl.sum, 2));
            // console.log("payToOwnerWith2Decimals = " + Utils.roundWithDecimals(payToOwnerWith2Decimals, 2));
            if (Utils.roundWithDecimals(withdrawl.sum, 2) === Utils.roundWithDecimals(payToOwnerWith2Decimals, 2)) {
                return process;
            }
        }
    });
};

matchWithdrawalToManagementCompany = function(withdrawl, processes) {
    var managementCompany,
        payToMgmtWith2Decimals;
    return _.find(processes, function(process) {
        if (process.management.managementCompany) {
            managementCompany = PropertyManagementCompanies.findOne({_id: process.management.managementCompany.details});
        }
        if (process.management.managementCompany && process.management.managementCompany.apartmentIBAN === withdrawl.IBAN) {
            payToMgmtWith2Decimals = process.management.managementCompany.housegeld;
            if (Utils.roundWithDecimals(withdrawl.sum, 2) === Utils.roundWithDecimals(payToMgmtWith2Decimals, 2)) {
                return process;
            }
        } else if (process.management.managementCompany && managementCompany && managementCompany.IBAN === withdrawl.IBAN) {
            payToMgmtWith2Decimals = process.management.managementCompany.housegeld;
            if (Utils.roundWithDecimals(withdrawl.sum, 2) === Utils.roundWithDecimals(payToMgmtWith2Decimals, 2)) {
                if ((process.primaryClient && (withdrawl.purpose.toLowerCase().indexOf(process.primaryClient.firstName.toLowerCase()) != -1 || withdrawl.purpose.toLowerCase().indexOf(process.primaryClient.lastName.toLowerCase()) != -1))||
                    (process.client2 && process.client2.firstName && (withdrawl.purpose.toLowerCase().indexOf(process.client2.firstName.toLowerCase()) != -1 || withdrawl.purpose.toLowerCase().indexOf(process.client2.firstName.toLowerCase()) != -1)) ||
                    (process.client3 && process.client3.firstName && (withdrawl.purpose.toLowerCase().indexOf(process.client3.firstName.toLowerCase()) != -1 || withdrawl.purpose.toLowerCase().indexOf(process.client3.firstName.toLowerCase()) != -1))
                ) {
                    return process;
                }
            }
        }
    });
};

matchWithdrawalToFinanzamt = function(withdrawl, processes) {
    return _.find(processes, function(process) {
        if (process.management.taxNumber && withdrawl.payer === 'FINANZAMT') {
            var payToFinanzamtWith2Decimals = process.management.quarterlyPayment;
            if (Utils.roundWithDecimals(withdrawl.sum, 2) === Utils.roundWithDecimals(payToFinanzamtWith2Decimals, 2)) {
                if (withdrawl.purpose.indexOf(process.management.taxNumber)) {
                    return process;
                }
            }
        }
    });
};

matchDepositToTenant = function(deposit, processes) {
    var match =  _.find(processes, function(process) {
        if (process.management.tenant && (process.management.tenant.tenantIBAN === deposit.IBAN || _.find(process.management.tenant.extraIBANS, function(IBAN){IBAN === deposit.IBAN}))) {
            return process;
        } else if (process.management.secondTenant && (process.management.secondTenant.tenantIBAN === deposit.IBAN || _.find(process.management.secondTenant.extraIBANS, function(IBAN){IBAN === deposit.IBAN}))) {
            return process;
        } else if (process.management.tenant && process.management.tenant.firstName && (deposit.payer === 'Bundesagentur fuer Arbeit-Service-Haus' || deposit.payer.indexOf('Bezirksamt') != -1 || deposit.payer.indexOf('Bezirkskasse') != -1)) {
            var payFromTenantWith2Decimals = getRelevantRentBrutto(process, moment(deposit.executionDate, 'DD.MM.YYYY').month());
            if (Utils.roundWithDecimals(deposit.sum, 2) === Utils.roundWithDecimals(payFromTenantWith2Decimals, 2)) {
                return process;
            } else {
                if ((process.management.tenant.firstName && process.management.tenant.lastName &&(deposit.purpose.toLowerCase().indexOf(process.management.tenant.firstName.toLowerCase()) != -1) && (deposit.purpose.toLowerCase().indexOf(process.management.tenant.lastName.toLowerCase()) != -1)) ||
                    (process.management.secondTenant && process.management.secondTenant.firstName && process.management.secondTenant.lastName && (deposit.purpose.toLowerCase().indexOf(process.management.secondTenant.firstName.toLowerCase()) != -1) && (deposit.purpose.indexOf(process.management.secondTenant.lastName.toLowerCase()) != -1))) {
                    return process;
                }
            }
        }
    });
    return match;
};

getRelevantRentBrutto = function(process, transactionMonth) {
    var rentBrutto,
        actualRentBruttoMonth,
        reveresedBruttos,
        index;
    if (process && process.management.tenant && process.management.tenant.previousRentBruttos) {
        actualRentBruttoMonth = getTransactionMonthAsIndex(process.management.tenant.actualRentBruttoDate);
        if (transactionMonth >= actualRentBruttoMonth + 1) {
            rentBrutto = process.management.tenant.actualRentBrutto;
        } else {
            reveresedBruttos = process.management.tenant.previousRentBruttos.reverse();
            rentBrutto =  _.find(reveresedBruttos, function(previousRentBrutto) {
                if (moment(previousRentBrutto.rentBruttoDate).year() < moment().year()) {
                    index = 0;
                } else {
                    index = getTransactionMonthAsIndex(previousRentBrutto.rentBruttoDate) + 1
                }
                return index <= transactionMonth;
            });
            if (rentBrutto) {
                rentBrutto = rentBrutto.rentBrutto;
            }
        }
    } else if (process.management.tenant) {
        rentBrutto = process.management.tenant.actualRentBrutto;
    } else {
        rentBrutto = 0;
    }
    return rentBrutto;
};

getTransactionMonthAsIndex = function(date) {
    var idx,
        momentDate = moment(date, 'DD.MM.YYYY');
    idx = momentDate.month();
    return idx;
};

findClosestMonth = function(num, arr) {
    var mid;
    var lo = 0;
    var hi = arr.length - 1;
    while (hi - lo > 1) {
        mid = Math.floor ((lo + hi) / 2);
        if (arr[mid] < num) {
            lo = mid;
        } else {
            hi = mid;
        }
    }
    if (num - arr[lo] <= arr[hi] - num) {
        return arr[lo];
    }
    return arr[hi];
};

getTenantCellClass = function(index, value, object) {
    var comperator,
        cellClass,
        value = getTenantCellValue(index, object);
    if (object.management.tenant) {
        comperator = getRelevantRentBrutto(object, index + 1);
    }
    cellClass = getPaymentCellClass(index, value, object, comperator);
    if (object.accounting && object.accounting.years[getYear()].tenantPayments && object.accounting.years[getYear()].tenantPayments[index] && _.find(object.accounting.years[getYear()].tenantPayments[index], function(payment){return payment.isLinked})) {
        cellClass += ' isLinked';
    }
    if (object.accounting && object.accounting.years[getYear()].tenantPayments && object.accounting.years[getYear()].tenantPayments[index] && _.find(object.accounting.years[getYear()].tenantPayments[index], function(payment){return payment.note})) {
        cellClass += ' note';
    }
    return cellClass;
};

getOwnerCellClass = function(index, value, object) {
    var comperator,
        cellClass,
        value = getOwnerCellValue(index, object);
    if (object.management.tenant && object.management.tenant.actualRentBrutto) {
        if (object.management.rentalFee) {
            comperator = (getRelevantRentBrutto(object, index + 1) - object.management.managementCompany.housegeld - object.management.managementFee - object.management.rentalFee);
        } else {
            comperator = (getRelevantRentBrutto(object, index + 1) - object.management.managementCompany.housegeld - object.management.managementFee);
        }
    }
    cellClass = getPaymentCellClass(index, value, object, comperator);
    if (object.accounting && object.accounting.years[getYear()].ownerPayments && object.accounting.years[getYear()].ownerPayments[index] && _.find(object.accounting.years[getYear()].ownerPayments[index], function(payment){return payment.isLinked})) {
        cellClass += ' isLinked';
    }
    if (object.accounting && object.accounting.years[getYear()].ownerPayments && object.accounting.years[getYear()].ownerPayments[index] && _.find(object.accounting.years[getYear()].ownerPayments[index], function(payment){return payment.note})) {
        cellClass += ' note';
    }
    return cellClass;
};

getManagementCellClass = function(index, value, object) {
    var managementCompany, comperator, cellClass,
        value = getManagementCellValue(index, object);
    if (object.management.managementCompany) {
        comperator = object.management.managementCompany.housegeld;
    }
    cellClass = getPaymentCellClass(index, value, object, comperator);
    if (object.accounting && object.accounting.years[getYear()].managementPayments && object.accounting.years[getYear()].managementPayments[index] && _.find(object.accounting.years[getYear()].managementPayments[index], function(payment){return payment.isLinked})) {
        cellClass += ' isLinked';
    }
    if (object.accounting && object.accounting.years[getYear()].managementPayments && object.accounting.years[getYear()].managementPayments[index] && _.find(object.accounting.years[getYear()].managementPayments[index], function(payment){return payment.note})) {
        cellClass += ' note';
    }
    return cellClass;
};

getLandtaxCellClass = function(index, value, object) {
    var cellClass, comperator = object.management.quarterlyPayment,
        value = getLandTaxCellValue(index, object);
    cellClass = getPaymentCellClass(index, value, object, comperator);
    if (object.accounting && object.accounting.years[getYear()].landTaxPayments && object.accounting.years[getYear()].landTaxPayments[index] && _.find(object.accounting.years[getYear()].landTaxPayments[index], function(payment){return payment.isLinked})) {
        cellClass += ' isLinked';
    }
    if (object.accounting && object.accounting.years[getYear()].landTaxPayments && object.accounting.years[getYear()].landTaxPayments[index] && _.find(object.accounting.years[getYear()].landTaxPayments[index], function(payment){return payment.note})) {
        cellClass += ' note';
    }
    return cellClass;
};

getPaymentCellClass = function(index, value, object, comperator) {
    var cellClass,
        currentMonth = moment().month();
    if (index < currentMonth - 1) {
        if (Utils.roundWithDecimals(value, 2) < Utils.roundWithDecimals(comperator + 1, 2) && Utils.roundWithDecimals(value, 2) >Utils.roundWithDecimals(comperator - 1, 2)) {
            cellClass = 'success';
        } else {
            cellClass = 'alert';
        }
    } else if (index === currentMonth -1) {
        if (!value) {
            cellClass = 'alert';
        } else {
            if (Utils.roundWithDecimals(value, 2) < Utils.roundWithDecimals(comperator + 1, 2) && Utils.roundWithDecimals(value, 2) >Utils.roundWithDecimals(comperator - 1, 2)) {
                cellClass = 'success';
            } else {
                cellClass = 'incomplete';
            }
        }
    } else if (index === currentMonth) {
        cellClass = 'current';
    }
    if (value !== 0) {
        cellClass += ' payment';
    }
    cellClass += ' month'+ index;
    return cellClass;
};

getNoteHTML = function(value, note) {
    return new Spacebars.SafeString(value + ' <div class="paymentNote hide">' + note + '</div>');
};

getYear = function() {
    return Session.get('accountingYear');
};

getTenantCellValue = function(index, object) {
    var value;
    if (object.accounting && object.accounting.years[getYear()].tenantPayments && object.accounting.years[getYear()].tenantPayments[index]) {
        value = _.sumBy(object.accounting.years[getYear()].tenantPayments[index], function(payment){
            return payment.value;
        });
    }
    return Utils.roundWithDecimals(value, 2) || 0;
};

getOwnerCellValue = function(index, object) {
    var value;
    if (object.accounting && object.accounting.years[getYear()].ownerPayments && object.accounting.years[getYear()].ownerPayments[index]) {
        value = _.sumBy(object.accounting.years[getYear()].ownerPayments[index], function(payment){
            return payment.value;
        });
    }
    return Utils.roundWithDecimals(value, 2) || 0;
};

getManagementCellValue = function(index, object) {
    var value;
    if (object.accounting && object.accounting.years[getYear()].managementPayments && object.accounting.years[getYear()].managementPayments[index]) {
        value = _.sumBy(object.accounting.years[getYear()].managementPayments[index], function(payment){
            return payment.value;
        });
    }
    return Utils.roundWithDecimals(value, 2) || 0;
};

getLandTaxCellValue = function(index, object) {
    var value;
    if (object.accounting && object.accounting.years[getYear()].landTaxPayments && object.accounting.years[getYear()].landTaxPayments[index]) {
        value = _.sumBy(object.accounting.years[getYear()].landTaxPayments[index], function(payment){
            return payment.value;
        });
    }
    return Utils.roundWithDecimals(value, 2) || 0;
};
