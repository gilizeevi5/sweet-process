import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

Template.PaymentInfoModal.created=function(){
    var self = this;
    Tracker.autorun(function() {
    });
};

Template.PaymentInfoModal.helpers({
    getOwnerName: function() {
        var process = Processes.findOne({_id: Session.get('selectedProcessId')}),
            fullName;
        if (process) {
            fullName = process.primaryClient.firstName + ' ' + process.primaryClient.lastName;
        }
        return fullName;
    },
    getPropertyAddress: function() {
        var process = Processes.findOne({_id: Session.get('selectedProcessId')});
        return process.realty.addressStreet + ' ' + process.realty.addressNumber + ' ' + process.realty.apartmentNumber;
    },
    getTenants: function() {
        var tenants,
            process = Processes.findOne({_id: Session.get('selectedProcessId')});
        tenants = process.management.tenant.firstName + ' ' +  process.management.tenant.lastName;
        if ( process.management.secondTenant) {
            tenants += '& ' + process.management.secondTenant.firstName + ' ' +  process.management.secondTenant.lastName;
        }
        return tenants;
    },
    getPayments: function() {
        return Session.get('paymentInfo');
    }
});

Template.PaymentInfoModal.events({
    'click .js-closeModal' (event, template) {
        $( ".cancel-link-issue" ).trigger( "click" );
        Session.set('selectedProcessId', false);
        $('#paymentInfoModal').modal('hide');
    }
});