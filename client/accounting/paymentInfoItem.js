import _ from 'lodash';
import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

Template.PaymentInfoItem.created=function(){
    var self = this;
    Tracker.autorun(function() {
    });
    Session.set('paymentInfoLinking', false);
    this.isLinking = new ReactiveVar(false);
    this.isSplitting = new ReactiveVar(false);
};

Template.PaymentInfoItem.helpers({
    getIsLinked: function() {
        var isLinkedString = 'No';
        if (this.isLinked) {
            isLinkedString = 'Yes';
        }
        return isLinkedString;
    },
    getIsSplit: function() {
        var isSplitString = 'No';
        if (this.isSplit) {
            isSplitString = 'Yes';
        }
        return isSplitString;
    },
    isLinking: function() {
        return Template.instance().isLinking.get();
    },
    isSplitting: function() {
        return Template.instance().isSplitting.get();
    },
    isSelected: function(value) {
        return moment(Template.instance().data.executionDate, 'DD.MM.YYYY').month() + 1 === value;
    },
    isNotLinkingOrSplitting() {
        var isNotLinkingOrSplitting;
        if (Template.instance().isLinking.get() || Template.instance().isSplitting.get()) {
            isNotLinkingOrSplitting = false;
        } else {
            isNotLinkingOrSplitting = true;
        }
        return isNotLinkingOrSplitting;
    },
    getCollection: function() {
        return Session.get('miniCollection');
    },
    getSettings: function() {
        return {
            rowsPerPage: 10,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return 'Link';
                    },
                    cellClass: function(value, object) {
                        return 'js-link-confirm';
                    }
                }
            ]
        };
    }
});

Template.PaymentInfoItem.events({
    'click .js-link-issue' (event, template) {
        var isLinking=template.isLinking.get();
        template.isLinking.set(!isLinking);
    },
    'click .js-split-issue' (event, template) {
        var isSplitting=template.isSplitting.get();
        template.isSplitting.set(!isSplitting);
    },
    'click .js-cancel-split-issue' (event, template) {
        var isSplitting=template.isSplitting.get();
        template.isSplitting.set(!isSplitting);
    },
    'click .js-confirm-split-issue' (event, template) {
        var process = Processes.findOne({_id: Session.get('selectedProcessId')}),
            direction = $('input:radio[name =\'splitTime\']:checked').val(),
            monthCount = parseInt($('#monthCount').val()),
            data = Template.instance().data,
            startingMonth;
            payment = {
                value: Utils.roundWithDecimals(data.bankTransaction.sum, 2) / monthCount,
                isLinked: true,
                isSplit: true,
                note: data.note,
                paymentDate: data.paymentDate,
                bankTransaction: data.bankTransaction
            },
            yearVar = parseInt(moment(data.bankTransaction.executionDate, 'DD.MM.YYYY').year()) - Constants.defaultAccountingYear;
        if (data.monthIdx !== undefined  && data.monthIdx !== null) {
            startingMonth = data.monthIdx;
        } else {
            startingMonth = moment(data.bankTransaction.executionDate, 'DD.MM.YYYY').month();
        }
        if (direction || monthCount) {
            if (direction === 'future') {
                if (startingMonth + (monthCount - 1) <= 11) {
                    console.log('split ' + direction + ' for ' + monthCount + ' months');
                    switch(data.paymentType) {
                        case 'PaymentFromTenant':
                            payment.paymentType ='PaymentFromTenant';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].tenantPayments) {
                                process.accounting.years[yearVar].tenantPayments = [];
                            }
                            _.remove(process.accounting.years[yearVar].tenantPayments[startingMonth], function(payment){
                                return _.isEqual(payment, data);
                            });
                            for (var i = startingMonth; i < startingMonth + monthCount; i++) {
                                payment.monthIdx = i;
                                if (!process.accounting.years[yearVar].tenantPayments[i]) {
                                    process.accounting.years[yearVar].tenantPayments[i] = [];
                                }
                                process.accounting.years[yearVar].tenantPayments[i].push(payment);
                            }
                            break;
                        case 'PaymentToOwner':
                            payment.paymentType ='PaymentToOwner';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].ownerPayments) {
                                process.accounting.years[yearVar].ownerPayments = [];
                            }
                            _.remove(process.accounting.years[yearVar].ownerPayments[startingMonth], function(payment){
                                return _.isEqual(payment, data);
                            });
                            for (var i = startingMonth; i < startingMonth + monthCount; i++) {
                                payment.monthIdx = i;
                                if (!process.accounting.years[yearVar].ownerPayments[i]) {
                                    process.accounting.years[yearVar].ownerPayments[i] = [];
                                }
                                process.accounting.years[yearVar].ownerPayments[i].push(payment);
                            }
                            break;
                        case 'PaymentToMgmtCompany':
                            payment.paymentType ='PaymentToMgmtCompany';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].managementPayments) {
                                process.accounting.years[yearVar].managementPayments = [];
                            }
                            _.remove(process.accounting.years[yearVar].managementPayments[startingMonth], function(payment){
                                return _.isEqual(payment, data);
                            });
                            for (var i = startingMonth; i < startingMonth + monthCount; i++) {
                                payment.monthIdx = i;
                                if (!process.accounting.years[yearVar].managementPayments[i]) {
                                    process.accounting.years[yearVar].managementPayments[i] = [];
                                }
                                process.accounting.years[yearVar].managementPayments[i].push(payment);
                            }
                            break;
                        case 'PaymentToFinanzamt':
                            payment.paymentType ='PaymentToFinanzamt';
                            payment.period = 'Quarterly';
                            if (!process.accounting.years[yearVar].landTaxPayments) {
                                process.accounting.years[yearVar].landTaxPayments = [];
                            }
                            _.remove(process.accounting.years[yearVar].landTaxPayments[startingMonth], function(payment){
                                return _.isEqual(payment, data);
                            });
                            for (var i = startingMonth; i < startingMonth + monthCount; i++) {
                                payment.monthIdx = i;
                                if (!process.accounting.years[yearVar].landTaxPayments[i]) {
                                    process.accounting.years[yearVar].landTaxPayments[i] = [];
                                }
                                process.accounting.years[yearVar].landTaxPayments[i].push(payment);
                            }
                            break;
                    }
                    Processes.update(process._id, {
                        $set: {
                            'accounting': process.accounting
                        }
                    });
                    Template.instance().isSplitting.set(false);
                    Session.set('selectedProcessId', false);
                    $('#paymentInfoModal').modal('hide');
                } else {
                    alert('The split cannot effect previous or next year');
                }
            } else {
                if (startingMonth - (monthCount - 1) >= 0) {
                    console.log('split ' + direction + ' for ' + monthCount + ' months');
                    switch(data.paymentType) {
                        case 'PaymentFromTenant':
                            payment.paymentType ='PaymentFromTenant';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].tenantPayments) {
                                process.accounting.years[yearVar].tenantPayments = [];
                            }
                            _.remove(process.accounting.years[yearVar].tenantPayments[startingMonth], function(payment){
                                return _.isEqual(payment, data);
                            });
                            for (var i = startingMonth; i > startingMonth - monthCount; i--) {
                                payment.monthIdx = i;
                                if (!process.accounting.years[yearVar].tenantPayments[i]) {
                                    process.accounting.years[yearVar].tenantPayments[i] = [];
                                }
                                process.accounting.years[yearVar].tenantPayments[i].push(payment);
                            }
                            break;
                        case 'PaymentToOwner':
                            payment.paymentType ='PaymentToOwner';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].ownerPayments) {
                                process.accounting.years[yearVar].ownerPayments = [];
                            }
                            _.remove(process.accounting.years[yearVar].ownerPayments[startingMonth], function(payment){
                                return _.isEqual(payment, data);
                            });
                            for (var i = startingMonth; i > startingMonth - monthCount; i--) {
                                payment.monthIdx = i;
                                if (!process.accounting.years[yearVar].ownerPayments[i]) {
                                    process.accounting.years[yearVar].ownerPayments[i] = [];
                                }
                                process.accounting.years[yearVar].ownerPayments[i].push(payment);
                            }
                            break;
                        case 'PaymentToMgmtCompany':
                            payment.paymentType ='PaymentToMgmtCompany';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].managementPayments) {
                                process.accounting.years[yearVar].managementPayments = [];
                            }
                            _.remove(process.accounting.years[yearVar].managementPayments[startingMonth], function(payment){
                                return _.isEqual(payment, data);
                            });
                            for (var i = startingMonth; i > startingMonth - monthCount; i--) {
                                payment.monthIdx = i;
                                if (!process.accounting.years[yearVar].managementPayments[i]) {
                                    process.accounting.years[yearVar].managementPayments[i] = [];
                                }
                                process.accounting.years[yearVar].managementPayments[i].push(payment);
                            }
                            break;
                        case 'PaymentToFinanzamt':
                            payment.paymentType ='PaymentToFinanzamt';
                            payment.period = 'Quarterly';
                            if (!process.accounting.years[yearVar].landTaxPayments) {
                                process.accounting.years[yearVar].landTaxPayments = [];
                            }
                            _.remove(process.accounting.years[yearVar].landTaxPayments[startingMonth], function(payment){
                                return _.isEqual(payment, data);
                            });
                            for (var i = startingMonth; i > startingMonth - monthCount; i--) {
                                payment.monthIdx = i;
                                if (!process.accounting.years[yearVar].landTaxPayments[i]) {
                                    process.accounting.years[yearVar].landTaxPayments[i] = [];
                                }
                                process.accounting.years[yearVar].landTaxPayments[i].push(payment);
                            }
                            break;
                    }
                    Processes.update(process._id, {
                        $set: {
                            'accounting': process.accounting
                        }
                    });
                    Template.instance().isSplitting.set(false);
                    Session.set('selectedProcessId', false);
                    $('#paymentInfoModal').modal('hide');
                } else {
                    alert('The split cannot effect previous or next year');
                }
            }
        } else {
            alert('You have to select the direction of the split for how many months');
        }
    },
    'click .js-cancel-link-issue' (event, template) {
        var isLinking=template.isLinking.get();
        template.isLinking.set(!isLinking);
    },
    'click .reactive-table.paymentInfo tbody tr': function (event) {
        var process = Processes.findOne({'_id': this._id});
        if (event.target.className.includes('js-link-confirm')) {
            var data = Template.instance().data,
                type = $('input:radio[name =\'type\']:checked').val(),
                month = $('#monthSelector').val(),
                note = $('#noteHolder').val(),
                originalProcess,
                payment,
                paymentDate,
                note,
                yearVar;
            if (data) {
                if (type && month) {
                    payment = {
                        value: Utils.roundWithDecimals(data.bankTransaction.sum, 2),
                        isLinked: true
                    };
                    if (moment(data.executionDate, 'DD.MM.YYYY').month(month) === moment(data.bankTransaction.executionDate, 'DD.MM.YYYY')) {
                        paymentDate = moment(data.bankTransaction.executionDate, 'DD.MM.YYYY')
                    } else {
                        paymentDate = moment(data.bankTransaction.executionDate, 'DD.MM.YYYY').month(month);
                    }
                    if (note) {
                        payment.note = note;
                    }
                    payment.date = paymentDate.toISOString();
                    payment.bankTransaction = data.bankTransaction;
                    monthIdx = getTransactionMonthAsIndex(paymentDate);
                    payment.monthIdx = monthIdx;
                    yearVar = parseInt(moment(data.bankTransaction.executionDate, 'DD.MM.YYYY').year()) - Constants.defaultAccountingYear;
                    if (!process.accounting) {
                        process.accounting = {};
                        process.accounting.years = [];
                    }
                    if (!process.accounting.years[yearVar]) {
                        process.accounting.years[yearVar] = {};
                    }
                    switch(type) {
                        case 'tenant':
                            payment.paymentType ='PaymentFromTenant';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].tenantPayments) {
                                process.accounting.years[yearVar].tenantPayments = [];
                            }
                            if (!process.accounting.years[yearVar].tenantPayments[monthIdx]) {
                                process.accounting.years[yearVar].tenantPayments[monthIdx] = [];
                            }
                            process.accounting.years[yearVar].tenantPayments[monthIdx].push(payment);
                            break;
                        case 'owner':
                            payment.paymentType ='PaymentToOwner';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].ownerPayments) {
                                process.accounting.years[yearVar].ownerPayments = [];
                            }
                            if (!process.accounting.years[yearVar].ownerPayments[monthIdx]) {
                                process.accounting.years[yearVar].ownerPayments[monthIdx] = [];
                            }
                            process.accounting.years[yearVar].ownerPayments[monthIdx].push(payment);
                            break;
                        case 'management':
                            payment.paymentType ='PaymentToMgmtCompany';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].managementPayments) {
                                process.accounting.years[yearVar].managementPayments = [];
                            }
                            if (!process.accounting.years[yearVar].managementPayments[monthIdx]) {
                                process.accounting.years[yearVar].managementPayments[monthIdx] = [];
                            }
                            process.accounting.years[yearVar].managementPayments[monthIdx].push(payment);
                            break;
                        case 'landTax':
                            payment.paymentType ='PaymentToFinanzamt';
                            payment.period = 'Quarterly';
                            if (!process.accounting.years[yearVar].landTaxPayments) {
                                process.accounting.years[yearVar].landTaxPayments = [];
                            }
                            if (!process.accounting.years[yearVar].landTaxPayments[monthIdx]) {
                                process.accounting.years[yearVar].landTaxPayments[monthIdx] = [];
                            }
                            process.accounting.years[yearVar].landTaxPayments[monthIdx].push(payment);
                            break;
                    }
                    Processes.update(process._id, {
                        $set: {
                            'accounting': process.accounting
                        }
                    });
                    if (data.monthIdx !== undefined && data.monthIdx !== null) {
                        originalMonthIdx = data.monthIdx;
                    } else {
                        originalMonthIdx = moment(data.bankTransaction.executionDate, 'DD.MM.YYYY').month();
                    }
                    originalProcess = Processes.findOne({_id: Session.get('selectedProcessId')});
                    switch(data.paymentType) {
                        case 'PaymentFromTenant':
                            _.remove(originalProcess.accounting.years[yearVar].tenantPayments[originalMonthIdx], function(payment){
                                return _.isEqual(payment, data);
                            });
                            break;
                        case 'PaymentToOwner':
                            _.remove(originalProcess.accounting.years[yearVar].ownerPayments[originalMonthIdx], function(payment){
                                return _.isEqual(payment, data);
                            });
                            break;
                        case 'PaymentToMgmtCompany':
                            _.remove(originalProcess.accounting.years[yearVar].managementPayments[originalMonthIdx], function(payment){
                                return _.isEqual(payment, data);
                            });
                            break;
                        case 'PaymentToFinanzamt':
                            _.remove(originalProcess.accounting.years[yearVar].landTaxPayments[originalMonthIdx], function(payment){
                                return _.isEqual(payment, data);
                            });
                            break;
                    }
                    Processes.update(originalProcess._id, {
                        $set: {
                            'accounting': originalProcess.accounting
                        }
                    });
                    data.bankTransaction.manualLink = true;
                    data.bankTransaction.linkDate = Date.now();
                    data.bankTransaction.userId = Meteor.userId();
                    linkedIssue = LinkedAccountingIssues.findOne({'executionDate': data.bankTransaction.executionDate, 'payee': data.bankTransaction.payee, 'payer': data.bankTransaction.payer, 'accountNumber': data.bankTransaction.accountNumber, 'IBAN': data.bankTransaction.IBAN, 'sum': data.bankTransaction.sum});
                    if (linkedIssue) {
                        LinkedAccountingIssues.update({'_id': linkedIssue._id},{$set:{'linkDate': data.bankTransaction.linkDate, 'userId': data.bankTransaction.userId}});
                    } else {
                        LinkedAccountingIssues.insert(data);
                    }
                    Template.instance().isLinking.set(false);
                    Session.set('selectedProcessId', false);
                    $('#paymentInfoModal').modal('hide');
                } else {
                    alert('You have to select a type of transaction and a month to link an issue to a process');
                }
            }
        }
    },
    'click .js-delete-issue' (event, template) {
        var process = Processes.findOne({_id: Session.get('selectedProcessId')}),
            yearVar = parseInt(moment(template.data.bankTransaction.executionDate, 'DD.MM.YYYY').year()) - Constants.defaultAccountingYear,
            type = template.data.paymentType,
            monthIndex,
            index,
            linkedAccountingIssue,
            linkedAccountingIssueId;

        switch (type) {
            case 'PaymentFromTenant':
                monthIndex = this.monthIdx || moment(this.bankTransaction.executionDate, 'DD.MM.YYYY').month();
                index = _.findIndex(process.accounting.years[yearVar].tenantPayments[monthIndex], function(item){
                    var isSame;
                    if (item.bankTransaction.IBAN === template.data.bankTransaction.IBAN &&
                        item.bankTransaction.sum === template.data.bankTransaction.sum &&
                        item.bankTransaction.executionDate === template.data.bankTransaction.executionDate &&
                        item.bankTransaction.payee === template.data.bankTransaction.payee &&
                        item.bankTransaction.payer === template.data.bankTransaction.payer
                    ) {
                        isSame = true;
                    }
                    return isSame;
                });
                if (index !== -1) {
                    process.accounting.years[yearVar].tenantPayments[monthIndex].splice(index, 1);
                    console.log('deleted tenantPayments with index of ', index);
                } else {
                    console.warn('unable to find the payment to delete');
                }
                break;
            case 'PaymentToOwner':
                monthIndex = this.monthIdx || moment(this.bankTransaction.executionDate, 'DD.MM.YYYY').month();
                index = _.findIndex(process.accounting.years[yearVar].ownerPayments[monthIndex], function(item){
                    var isSame;
                    if (item.bankTransaction.IBAN === template.data.bankTransaction.IBAN &&
                        item.bankTransaction.sum === template.data.bankTransaction.sum &&
                        item.bankTransaction.executionDate === template.data.bankTransaction.executionDate &&
                        item.bankTransaction.payee === template.data.bankTransaction.payee &&
                        item.bankTransaction.payer === template.data.bankTransaction.payer
                    ) {
                        isSame = true;
                    }
                    return isSame;
                });
                if (index !== -1) {
                    process.accounting.years[yearVar].ownerPayments[monthIndex].splice(index, 1);
                    console.log('deleted ownerPayments with index of ', index);
                } else {
                    console.warn('unable to find the payment to delete');
                }
                break;
            case 'PaymentToMgmtCompany':
                monthIndex = this.monthIdx || moment(this.managementPayments.executionDate, 'DD.MM.YYYY').month();
                index = _.findIndex(process.accounting.years[yearVar].ownerPayments[monthIndex], function(item){
                    var isSame;
                    if (item.bankTransaction.IBAN === template.data.bankTransaction.IBAN &&
                        item.bankTransaction.sum === template.data.bankTransaction.sum &&
                        item.bankTransaction.executionDate === template.data.bankTransaction.executionDate &&
                        item.bankTransaction.payee === template.data.bankTransaction.payee &&
                        item.bankTransaction.payer === template.data.bankTransaction.payer
                    ) {
                        isSame = true;
                    }
                    return isSame;
                });
                if (index !== -1) {
                    process.accounting.years[yearVar].managementPayments[monthIndex].splice(index, 1);
                    console.log('deleted managementPayments with index of ', index);
                } else {
                    console.warn('unable to find the payment to delete');
                }
                break;
            case 'PaymentToFinanzamt':
                monthIndex = this.monthIdx || moment(this.landTaxPayments.executionDate, 'DD.MM.YYYY').month();
                index = _.findIndex(process.accounting.years[yearVar].ownerPayments[monthIndex], function(item){
                    var isSame;
                    if (item.bankTransaction.IBAN === template.data.bankTransaction.IBAN &&
                        item.bankTransaction.sum === template.data.bankTransaction.sum &&
                        item.bankTransaction.executionDate === template.data.bankTransaction.executionDate &&
                        item.bankTransaction.payee === template.data.bankTransaction.payee &&
                        item.bankTransaction.payer === template.data.bankTransaction.payer
                    ) {
                        isSame = true;
                    }
                    return isSame;
                });
                if (index !== -1) {
                    process.accounting.years[yearVar].landTaxPayments[monthIndex].splice(index, 1);
                    console.log('deleted landTaxPayments with index of ', index);
                } else {
                    console.warn('unable to find the payment to delete');
                }
                break;
        }
        linkedAccountingIssue = _.find(LinkedAccountingIssues.find({}).fetch(), function(issue) {
            var isSame;
            if (issue.IBAN === template.data.bankTransaction.IBAN &&
                issue.sum === template.data.bankTransaction.sum &&
                issue.executionDate === template.data.bankTransaction.executionDate &&
                issue.payee === template.data.bankTransaction.payee &&
                issue.payer === template.data.bankTransaction.payer
            ) {
                isSame = true;
            }
            return isSame;
        });
        Processes.update(process._id, {
            $set: {
                'accounting': process.accounting
            }
        });
        if (linkedAccountingIssue) {
            linkedAccountingIssueId = linkedAccountingIssue._id;
            LinkedAccountingIssues.remove({'_id': linkedAccountingIssueId});
            console.log('deleted issue with id ', linkedAccountingIssueId);
        }
        $( ".cancel-link-issue" ).trigger( "click" );
        Session.set('selectedProcessId', false);
        $('#paymentInfoModal').modal('hide');
    },});


getYear = function() {
    return Session.get('accountingYear');
};