import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

Template.TransactionItem.created=function(){
    Tracker.autorun(function() {
    });
    this.isLinking = new ReactiveVar(false);
};

Template.TransactionItem.helpers({
    isLinking: function() {
        return Template.instance().isLinking.get();
    },
    isSelected: function(value) {
        return moment(Template.instance().data.executionDate, 'DD.MM.YYYY').month() + 1 === value;
    },
    getCollection: function() {
        return Session.get('miniCollection');
    },
    getSettings: function() {
        return {
            rowsPerPage: 10,
            showRowCount: true,
            showNavigation: 'always',
            showNavigationRowsPerPage: true,
            fields: [
                {
                    key: 'primaryClient.firstName',
                    label: 'Client name',
                    fn: function (value, object, key) {
                        return object.primaryClient.lastName + ' ' + object.primaryClient.firstName;
                    }
                },
                {
                    key: 'primaryClient.firstName',
                    label: 'First Name',
                    hidden: true
                },
                {
                    key: 'primaryClient.lastName',
                    label: 'Last Name',
                    hidden: true
                },
                {
                    key: 'realty.addressStreet',
                    label: 'Property',
                    fn: function (value, object, key) {
                        return object.realty.addressStreet + ' ' + object.realty.addressNumber + ' ' + object.realty.apartmentNumber;
                    }
                },
                {
                    key: '_id',
                    label: 'Link to process',
                    fn: function (value, object, key) {
                        return 'Link';
                    },
                    cellClass: function(value, object) {
                        return 'js-link-confirm';
                    }
                }
            ]
        };
    }
});

Template.TransactionItem.events({
    'click .js-delete-issue' (event, template) {
        AccountingIssues.remove({_id: this._id});
    },
    'click .js-ignore-issue' (event, template) {
        IgnoredAccountingIssues.insert(this);
        AccountingIssues.remove({_id: this._id});
    },
    'click .js-link-issue' (event, template) {
        var isLinking=template.isLinking.get();
        template.isLinking.set(!isLinking);
    },
    'click .js-cancel-link-issue' (event, template) {
        var isLinking=template.isLinking.get();
        template.isLinking.set(!isLinking);
    },
    'click .reactive-table tbody tr': function (event) {
        var process = this;
        if (event.target.className.includes('js-link-confirm')) {
            var data = Template.instance().data,
                type = $('input:radio[name =\'type\']:checked').val(),
                month = $('#monthSelector').val(),
                note = $('#noteHolder').val(),
                payment,
                paymentDate,
                note,
                yearVar;
            if (data) {
                if (type && month) {
                    payment = {
                        value: Utils.roundWithDecimals(data.sum, 2),
                        isLinked: true
                    };
                    if (moment(data.executionDate, 'DD.MM.YYYY').month(month) === moment(data.executionDate, 'DD.MM.YYYY')) {
                        paymentDate = moment(data.executionDate, 'DD.MM.YYYY')
                    } else {
                        paymentDate = moment(data.executionDate, 'DD.MM.YYYY').month(month);
                    }
                    if (note) {
                        payment.note = note;
                    }
                    payment.date = paymentDate.toISOString();
                    payment.bankTransaction = data;
                    monthIdx = getTransactionMonthAsIndex(paymentDate);
                    yearVar = parseInt(moment(data.executionDate, 'DD.MM.YYYY').year()) - Constants.defaultAccountingYear;
                    if (!process.accounting) {
                        process.accounting = {};
                        process.accounting.years = [];
                    }
                    if (!process.accounting.years[yearVar]) {
                        process.accounting.years[yearVar] = {};
                    }
                    switch(type) {
                        case 'tenant':
                            payment.paymentType ='PaymentFromTenant';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].tenantPayments) {
                                process.accounting.years[yearVar].tenantPayments = [];
                            }
                            if (!process.accounting.years[yearVar].tenantPayments[monthIdx]) {
                                process.accounting.years[yearVar].tenantPayments[monthIdx] = [];
                            }
                            process.accounting.years[yearVar].tenantPayments[monthIdx].push(payment);
                            Processes.update(process._id, {
                                $set: {
                                    'accounting': process.accounting
                                }
                            });
                            break;
                        case 'owner':
                            payment.paymentType ='PaymentToOwner';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].ownerPayments) {
                                process.accounting.years[yearVar].ownerPayments = [];
                            }
                            if (!process.accounting.years[yearVar].ownerPayments[monthIdx]) {
                                process.accounting.years[yearVar].ownerPayments[monthIdx] = [];
                            }
                            process.accounting.years[yearVar].ownerPayments[monthIdx].push(payment);
                            Processes.update(process._id, {
                                $set: {
                                    'accounting': process.accounting
                                }
                            });
                            break;
                        case 'management':
                            payment.paymentType ='PaymentToMgmtCompany';
                            payment.period = 'Monthly';
                            if (!process.accounting.years[yearVar].managementPayments) {
                                process.accounting.years[yearVar].managementPayments = [];
                            }
                            if (!process.accounting.years[yearVar].managementPayments[monthIdx]) {
                                process.accounting.years[yearVar].managementPayments[monthIdx] = [];
                            }
                            process.accounting.years[yearVar].managementPayments[monthIdx].push(payment);
                            Processes.update(process._id, {
                                $set: {
                                    'accounting': process.accounting
                                }
                            });
                            break;
                        case 'landTax':
                            payment.paymentType ='PaymentToFinanzamt';
                            payment.period = 'Quarterly';
                            if (!process.accounting.years[yearVar].landTaxPayments) {
                                process.accounting.years[yearVar].landTaxPayments = [];
                            }
                            if (!process.accounting.years[yearVar].landTaxPayments[monthIdx]) {
                                process.accounting.years[yearVar].landTaxPayments[monthIdx] = [];
                            }
                            process.accounting.years[yearVar].landTaxPayments[monthIdx].push(payment);
                            Processes.update(process._id, {
                                $set: {
                                    'accounting': process.accounting
                                }
                            });
                            break;
                    }
                    data.monthIdx = monthIdx;
                    data.manualLink = true;
                    data.linkDate = Date.now();
                    data.userId = Meteor.userId();
                    LinkedAccountingIssues.insert(data);
                    AccountingIssues.remove({_id: data._id});
                } else {
                    alert('You have to select a type of transaction and a month to link an issue to a process');
                }
            }
        }
    }
});

getYear = function() {
    return Session.get('accountingYear');
};