import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.ConfirmFinanzamt.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);});
};

Template.ConfirmFinanzamt.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep
});

Template.ConfirmFinanzamt.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    }
});

AutoForm.addHooks(['ConfirmFinanzamtForm'], {
    onSuccess: function() {
        var id = FlowRouter.getParam('id');
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 50, Meteor.userId());
        FlowRouter.go('process-list');
    }
});