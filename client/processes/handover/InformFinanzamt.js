import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.InformFinanzamt.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);});
};

Template.InformFinanzamt.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep
});

Template.InformFinanzamt.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    }
});

AutoForm.addHooks(['InformFinanzamtForm'], {
    onSuccess: function() {
        var id = FlowRouter.getParam('id');
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 49, Meteor.userId());
        FlowRouter.go('process-list');
    }
});