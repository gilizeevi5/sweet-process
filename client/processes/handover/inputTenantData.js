import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.InputTenantData.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
    this.isUncomplete = new ReactiveVar(false);
};

Template.InputTenantData.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    isUncomplete: function(){
        return Template.instance().isUncomplete.get();
    }
});

Template.InputTenantData.events({
    'click .js-cancel':function() {
        AutoForm.resetForm('InputTenantDataForm');
        FlowRouter.go('process-list');
    }
});

AutoForm.addHooks(['InputTenantDataForm'], {
    onSuccess: function() {
        var id = FlowRouter.getParam('id');
        this.template.parent().isUncomplete.set(false);
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 42, Meteor.userId());
        FlowRouter.go('process-list');
    },
    onError: function(formType, error) {
        console.log('error = ' + error);
        this.template.parent().isUncomplete.set(true);
    }
});