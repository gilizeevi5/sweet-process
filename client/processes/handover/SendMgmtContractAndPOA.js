import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.SendMgmtContractAndPOA.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
        self.subscribe('configurations');
    });
};

Template.SendMgmtContractAndPOA.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    today: function() {
        return moment().format("YYYY-MM-DD");
    },
    managementFee: function() {
        var id = FlowRouter.getParam('id'),
            conf = Configurations.findOne(),
            doc = Processes.findOne({_id: id});
        if (conf && doc.realty.town.toUpperCase() === "Berlin".toUpperCase()) {
            return conf.managementFee;
        } else {
            return 0;
        }
    }
});

Template.SendMgmtContractAndPOA.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    }
});

AutoForm.addHooks(['SendMgmtContractAndPOAForm'], {
    onSuccess: function() {
        var id = FlowRouter.getParam('id');
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 40, Meteor.userId());
        FlowRouter.go('process-list');
    }
});