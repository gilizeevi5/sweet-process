import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.InputManagementCompanyData.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
        self.subscribe('propertyManagementCompanies');
    });
    this.isUncomplete = new ReactiveVar(false);
};

Template.InputManagementCompanyData.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    isUncomplete: function(){
        return Template.instance().isUncomplete.get();
    }
});

Template.InputManagementCompanyData.events({
    'click .js-cancel':function() {
        AutoForm.resetForm('InputManagementCompanyDataForm');
        FlowRouter.go('process-list');
    }
});

AutoForm.addHooks(['InputManagementCompanyDataForm'], {
    onSuccess: function() {
        var id = FlowRouter.getParam('id');
        this.template.parent().isUncomplete.set(false);
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 46, Meteor.userId());
        FlowRouter.go('process-list');
    },
    onError: function(formType, error) {
        console.log('error = ' + error);
        this.template.parent().isUncomplete.set(true);
    }
});