import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.InputConversionRate.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
};

Template.InputConversionRate.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep
});

Template.InputConversionRate.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    }
});

AutoForm.addHooks(['inputConversionRateForm'], {
    onSuccess: function() {
        var id = FlowRouter.getParam('id');
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 27, Meteor.userId());
        FlowRouter.go('process-list');
    }
});