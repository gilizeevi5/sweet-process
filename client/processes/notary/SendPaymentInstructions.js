import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.SendPaymentInstructions.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
};

Template.SendPaymentInstructions.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    payableToTwoAccounts: function() {
        var formId = AutoForm.getFormId();
        return AutoForm.getFieldValue('notary.payment.payableToTwoAccounts', formId);
    },
    getPaymentAccount1Value: function() {
        var id = FlowRouter.getParam('id'),
            process = Processes.findOne({_id: id}),
            valueToPay,
            formId = AutoForm.getFormId(),
            account2Value = AutoForm.getFieldValue('process.notary.payment.paymentAccount2.value', formId);
        if (process.mortgageRequest.mortgageApprovedSum) {
            valueToPay = process.realty.price - process.mortgageRequest.mortgageApprovedSum;
        } else {
            valueToPay = process.realty.price;
        }
        return valueToPay;
    },
    getPaymentAccount2Value: function() {
        return 0;
    }
});

Template.SendPaymentInstructions.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    }
});

AutoForm.addHooks(['SendPaymentInstructionsForm'], {
    onSuccess: function() {
        var id = FlowRouter.getParam('id');
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 35, Meteor.userId());
        FlowRouter.go('process-list');
    }
});