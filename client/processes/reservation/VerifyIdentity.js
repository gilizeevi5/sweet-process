import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.VerifyIdentity.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
    this.isUncomplete=new ReactiveVar(false);
};

Template.VerifyIdentity.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    getLogItem: function(idx, date) {
        return Constants.steps[idx].label + ' at: ' + date;
    },
    isUncomplete: function(){
        return Template.instance().isUncomplete.get();
    },
});

Template.VerifyIdentity.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    },
    'click .js-submit':function(event,template) {
        if (checkIfComplete()) {
            $('#verifyIdentityForm').submit();
        } else {
            template.isUncomplete.set(true);
        }
    },
    'click .js-uploadPassport': function(event) {
        var id = FlowRouter.getParam('id');
        filepicker.pickAndStore(
            {
                extension: ['.pdf', '.jpeg', '.jpg', 'png'],
                maxSize: '10485760',
                services: ['COMPUTER','DROPBOX','GMAIL'],
                container: 'window'
            },
            {
                location:"S3",
            },
            function(Blobs){
                if (event.currentTarget.dataset.client === '1') {
                    Processes.update(id, {
                        $set: {
                            'primaryClient.identification.passportScanPath': Blobs[0].url,
                            'primaryClient.identification.passportScanFileName': Blobs[0].filename
                        }
                    });
                } else if (event.currentTarget.dataset.client === '2') {
                    Processes.update(id, {
                        $set: {
                            'client2.identification.passportScanPath': Blobs[0].url,
                            'client2.identification.passportScanFileName': Blobs[0].filename
                        }
                    });
                } else if (event.currentTarget.dataset.client === '3') {
                    Processes.update(id, {
                        $set: {
                            'client3.identification.passportScanPath': Blobs[0].url,
                            'client3.identification.passportScanFileName': Blobs[0].filename
                        }
                    });
                }
            },
            function(error){
                 console.log(JSON.stringify(error));
            }
        );
    },
    'click .js-uploadSignature': function(event) {
        var id = FlowRouter.getParam('id');
        filepicker.pickAndStore(
            {
                extension: ['.pdf', '.jpeg', '.jpg', 'png'],
                maxSize: '10485760',
                services: ['COMPUTER','DROPBOX','GMAIL'],
                container: 'window'
            },
            {
                location:"S3",
            },
            function(Blobs){
                if (event.currentTarget.dataset.client === '1') {
                    Processes.update(id, {
                        $set: {
                            'primaryClient.identification.signaturePath': Blobs[0].url,
                            'primaryClient.identification.signatureFileName': Blobs[0].filename
                        }
                    });
                } else if (event.currentTarget.dataset.client === '2') {
                    Processes.update(id, {
                        $set: {
                            'client2.identification.signaturePath': Blobs[0].url,
                            'client2.identification.signatureFileName': Blobs[0].filename
                        }
                    });
                } else if (event.currentTarget.dataset.client === '3') {
                    Processes.update(id, {
                        $set: {
                            'client3.identification.signaturePath': Blobs[0].url,
                            'client3.identification.signatureFileName': Blobs[0].filename
                        }
                    });
                }
            },
            function(error){
                console.log(JSON.stringify(error));
            }
        );
    }
});

AutoForm.addHooks(['verifyIdentityForm'], {
    onSuccess: function() {
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 2, Meteor.userId());
        FlowRouter.go('process-list');
    }
});

function checkIfComplete() {
    var isComplete,
        doc = Processes.findOne({_id: FlowRouter.getParam('id')});
    if (doc.primaryClient.identification && doc.primaryClient.identification.signaturePath && doc.primaryClient.identification.passportScanPath) {
        if (doc.includeClient2) {
            if (doc.client2.identification && doc.client2.identification.signaturePath && doc.client2.identification.passportScanPath) {
                if (doc.includeClient3) {
                    if (doc.client3.identification && doc.client3.identification.signaturePath && doc.client3.identification.passportScanPath) {
                        isComplete = true;
                    } else {
                        isComplete = false;
                    }
                } else {
                    isComplete = true;
                }
            } else {
                isComplete = false;
            }
        } else {
            isComplete = true;
        }
    } else {
        isComplete = false;
    }
    return isComplete;
}