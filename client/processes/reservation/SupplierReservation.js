import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.SupplierReservation.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
    this.isUncomplete=new ReactiveVar(false);
};

Template.SupplierReservation.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    isUncomplete: function(){
        return Template.instance().isUncomplete.get();
    }
});

Template.SupplierReservation.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    },
    'click .js-submit':function(event,template) {
        if (checkIfComplete()) {
            $('#supplierReservationForm').submit();
        } else {
            template.isUncomplete.set(true);
        }
    }
});

AutoForm.addHooks(['supplierReservationForm'], {
    onSuccess: function() {
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 4, Meteor.userId());
        FlowRouter.go('process-list');
    }
});

function checkIfComplete() {
    var isComplete,
        includeClient1 = $("input[name='supplierReservation.includeClient1']"),
        includeClient2 = $("input[name='supplierReservation.includeClient2']"),
        includeClient3 = $("input[name='supplierReservation.includeClient3']");
    if (includeClient1.is(':checked') || includeClient2.is(':checked') || includeClient3.is(':checked')) {
        isComplete = true;
    } else {
        isComplete = false;
    }
    return isComplete;
};