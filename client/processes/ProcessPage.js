import _ from 'lodash';
import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';;

Template.ProcessPage.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
        self.subscribe('configurations');
        self.subscribe('suppliers');
        self.subscribe('mortgageBrokers');
        self.subscribe('notaries');
        self.subscribe('accountLawyers');
        self.subscribe('propertyManagementCompanies');
        self.subscribe('users');
        self.subscribe('linkedAccountingIssues');
        self.subscribe('ignoredAccountingIssues');
    });
    this.isInvalid=new ReactiveVar(false);
    Session.set('isEditing', false);
    Session.set('isInvalid', false);
    Session.set('isAddingNote', false);
};

Template.ProcessPage.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getNotes: function() {
        return process.notes;
    },
    isEditing: function(){
        return Session.get('isEditing');
    },
    isInvalid: function() {
        return Session.get('isInvalid');
    },
    isAddingNote: function() {
        return Session.get('isAddingNote');
    },
    getProcessStep: Constants.getProcessStep,
    getLogItemLabel: function(item) {
        return Constants.steps[item.index].label;
    },
    getFormatedDate: function(item) {
        var formatedDate;
        if (item.date) {
            formatedDate = moment(item.date).format('D MMMM YYYY, HH:mm')
        } else {
            formatedDate = moment(item).format('D MMMM YYYY, HH:mm')
        }
        return formatedDate;
    },
    getUser: function(item) {
        var user = Meteor.users.findOne({_id: item.userId}),
            result;
        if (user) {
            result =  user.emails[0].address;
        } else {
            result = 'unknown';
        }
        return result;
    },
    isEmployee: function(professionType) {
        return isEmployee(professionType);
    },
    startedMortgage: function() {
        var process = Processes.findOne({_id: FlowRouter.getParam('id')});
        if (process.step > 5 && process.mortgageRequirement.requiresMortgage) {
            return true;
        }
    },
    mortgageNotComplete: function() {
        var process = Processes.findOne({_id: FlowRouter.getParam('id')});
        if (process.stepB < Constants.phaseCap[1]) {
            return true;
        }
    },
    startedHandover: function() {
        var process = Processes.findOne({_id: FlowRouter.getParam('id')});
        if (process.management) {
            return true;
        }
    },
    isNotComplete: function() {
        var process = Processes.findOne({_id: FlowRouter.getParam('id')});
        if (process.step > Constants.phaseCap[3]) {
            return false;
        } else {
            return true;
        }
    }
});

Template.ProcessPage.events({
    'click .js-remove':function() {
        var id = FlowRouter.getParam('id');
        if (window.confirm("Are you sure you want to delete this process?")) {
            Processes.remove(id);
            FlowRouter.go('process-list');
        };
    },
    'click .js-set-offline': function() {
        var id = FlowRouter.getParam('id');
        Processes.update(id, {
            $set: {
                'isOnline': false
            }
        });
        console.log('isOnline = ' + doc.isOnline);
    },
    'click .js-set-online': function() {
        var id = FlowRouter.getParam('id');
        Processes.update(id, {
            $set: {
                'isOnline': true
            }
        });
    },
    'click .js-change-step': function() {
        var id = FlowRouter.getParam('id'),
            doc = Processes.findOne({_id: id}),
            currentValue = parseInt($('#stepSelector').val()),
            currentPhase = parseInt(doc.phase),
            log = doc.log,
            cleanedLog = cleanLog(log,currentValue);
        if (currentValue > Constants.phaseCap[0] && currentValue <= Constants.phaseCap[1]) {
            Session.set('isInvalid', true);
            console.log('trying to change to an invalid step');
            return;
        } else {
            Session.set('isInvalid', false);
        }
        if (currentValue <= Constants.phaseCap[0]) {
            currentPhase = 0;
            if (doc.stepB) {
                Processes.update(id, {
                    $unset: {
                        'stepB': '',
                        'phaseB': ''
                    }
                });
            }
        } else if (currentValue <= Constants.phaseCap[1]) {
            currentPhase = 1;
        } else if (currentValue <= Constants.phaseCap[2])  {
            currentPhase = 2;
        } else if (currentValue > Constants.phaseCap[2]) {
            currentPhase = 3;
        }
        Processes.update(id, {
            $set: {
                'step': parseInt(currentValue),
                'phase': currentPhase,
                'log': cleanedLog,
                'lastStepAAdvancedAt': new Date(),
                'isAOverdue': false
            }
        });
        $('#changeStepModal').modal('hide');
    },
    'click .js-change-stepB': function() {
        var id = FlowRouter.getParam('id'),
            doc = Processes.findOne({_id: id}),
            currentValue = parseInt($('#stepSelectorB').val()),
            currentPhase = parseInt(doc.phaseB),
            log = doc.log,
            cleanedLog = cleanLog(log,currentValue);
        if (currentValue <= Constants.phaseCap[0] || currentValue > Constants.phaseCap[1]) {
            Session.set('isInvalid', true);
            console.log('trying to change to an invalid step');
            return;
        } else {
            Session.set('isInvalid', false);
        }
        if (currentValue <= Constants.phaseCap[0]) {
            currentPhase = 0;
        } else if (currentValue <= Constants.phaseCap[1]) {
            currentPhase = 1;
        } else if (currentValue <= Constants.phaseCap[2])  {
            currentPhase = 2;
        }
        Processes.update(id, {
            $set: {
                'stepB': parseInt(currentValue),
                'phaseB': currentPhase,
                'log': cleanedLog,
                'lastStepBAdvancedAt': new Date(),
                'isBOverdue': false
            }
        });
        $('#changeStepBModal').modal('hide');
    },
    'click .js-repeat-step': function(event, template) {
        var step = $(event.currentTarget).data('step'),
            id = FlowRouter.getParam('id');
        Meteor.call('playStep', id, step);
        Alerts.defaultOptions.autoHide = true;
        Alerts.defaultOptions.fadeOut = 1200;
        Alerts.add('Step #' + step + ' was repeated', 'success');
    },
    'click .js-edit':function(event, template) {
        var isEditing = Session.get('isEditing');
        Session.set('isEditing', !isEditing);
    },
    'click .js-add-new-note':function(event, template) {
        var isAddingNote = Session.get('isAddingNote');
        Session.set('isAddingNote', !isAddingNote);
    },
    'click #log .js-cancel'(event,template){
        var isAddingNote = Session.get('isAddingNote');
        Session.set('isAddingNote', !isAddingNote);
    },
    'click #management .js-cancel'(event,template){
        var isAddingNote = Session.get('isAddingNote');
        Session.set('isAddingNote', !isAddingNote);
    },
    'click #log .js-submit-note'(event,template){
        var id = FlowRouter.getParam('id'),
            doc = Processes.findOne({_id: id}),
            body = $('#insertNewNoteForm textarea').val(),
            isAddingNote = Session.get('isAddingNote'),
            notes = doc.notes;

        if (!notes || notes.length === 0) {
            notes = [];
        }
        notes.push({'date': new Date(), 'userId': Meteor.userId(), 'body': body, 'index': notes.length-1});
        Processes.update(id, {
            $set: {
                notes: notes
            }
        });
        Session.set('isAddingNote', !isAddingNote);

    },
    'click #management .js-submit-note'(event,template){
        var id = FlowRouter.getParam('id'),
            doc = Processes.findOne({_id: id}),
            body = $('#insertNewManagementNoteForm textarea').val(),
            isAddingNote = Session.get('isAddingNote'),
            notes = doc.management.notes;

        if (!notes || notes.length === 0) {
            notes = [];
        }
        notes.push({'date': new Date(), 'userId': Meteor.userId(), 'body': body, 'index': notes.length-1});
        Processes.update(id, {
            $set: {
                'management.notes': notes
            }
        });
        Session.set('isAddingNote', !isAddingNote);

    },
    'click #log .js-delete-note': function(event, template) {
        var index = $(event.currentTarget).data('index'),
            id = FlowRouter.getParam('id'),
            doc = Processes.findOne({_id: id}),
            notes = doc.notes;
        notes = _.filter(notes, function(item) {
            return item.index !== index;
        });
        Processes.update(id, {
            $set: {
                notes: notes
            }
        });
    },
    'click #management .js-delete-note': function(event, template) {
        var index = $(event.currentTarget).data('index'),
            id = FlowRouter.getParam('id'),
            doc = Processes.findOne({_id: id}),
            notes = doc.management.notes;
        notes = _.filter(notes, function(item) {
            return item.index !== index;
        });
        Processes.update(id, {
            $set: {
                'management.notes': notes
            }
        });
    },
    'click .js-advance': function(event) {
        var id = FlowRouter.getParam('id');
        AdvanceProcess(Processes.findOne({_id: id}), event);
    },
    'click .js-closeModal': function(event) {
        Session.set('isInvalid', false);
    },
    'click .js-archive': function(event) {
        var id = FlowRouter.getParam('id');
        Processes.update(id, {
            $set: {
                'isArchived': true
            }
        });
    },
    'click .js-unarchive': function(event) {
        var id = FlowRouter.getParam('id');
        Processes.update(id, {
            $set: {
                'isArchived': false
            }
        });
    },
    'click .js-delete-accounting-data': function(event) {
        var id = FlowRouter.getParam('id');
        Meteor.call('deleteAccountingById', id, function(error, result){
            if (error) {
                Alerts.add('ERROR: ' + error.reason, 'danger');
                console.error('ERROR, error = ' + JSON.stringify(error));
            } else {
                Alerts.defaultOptions.autoHide = true;
                Alerts.defaultOptions.fadeOut = 1200;
                Alerts.add('Accounting data was deleted', 'success');
            }
        });
    }
});

cleanLog = function(log, currentStep) {
    return _.filter(log, function(item) {
        if (parseInt(item.index) <= (currentStep - 1)) {
            return item;
        }
    })
};

AutoForm.addHooks(['updatePropertyForm','updateClientsForm','updateMortgageRequirementForm','updateNotaryForm', 'updateHandoverForm'], {
    onSuccess: function() {
        var isEditing = Session.get('isEditing'),
            id = FlowRouter.getParam('id');

        Session.set('isEditing', !isEditing);
        Meteor.call('addRefToRealty', id);
    }
});

AutoForm.addHooks(['insertNewNoteForm'], {
    onSuccess: function() {
        var isAddingNote = Session.get('isAddingNote');
        Session.set('isAddingNote', !isAddingNote);
    }
});