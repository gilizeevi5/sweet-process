import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.UploadEquityReceipt.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
    this.isUncomplete=new ReactiveVar(false);
};

Template.UploadEquityReceipt.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    getLogItem: function(idx, date) {
        return Constants.steps[idx].label + ' at: ' + date;
    },
    isUncomplete: function(){
        return Template.instance().isUncomplete.get();
    },
});

Template.UploadEquityReceipt.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    },
    'click .js-submit':function(event,template) {
        if (checkIfComplete()) {
            $('#uploadEquityReceiptForm').submit();
        } else {
            template.isUncomplete.set(true);
        }
    },
    'click .js-uploadReceipt': function(event) {
        var id = FlowRouter.getParam('id');
        filepicker.pickAndStore(
            {
                extension: ['.pdf', '.jpeg', '.jpg', 'png'],
                maxSize: '10485760',
                services: ['COMPUTER','DROPBOX','GMAIL'],
                container: 'window'
            },
            {
                location:"S3",
            },
            function(Blobs){
                Processes.update(id, {
                    $set: {
                        'mortgageRequest.equityReceipt.receiptPath': Blobs[0].url,
                        'mortgageRequest.equityReceipt.receiptFileName': Blobs[0].filename
                    }
                });
            },
            function(error){
                 console.log(JSON.stringify(error));
            }
        );
    }
});

AutoForm.addHooks(['uploadEquityReceiptForm'], {
    onSuccess: function() {
        Meteor.call('advanceProcess', FlowRouter.getParam('id'),19, Meteor.userId());
        FlowRouter.go('process-list');
    }
});

function checkIfComplete() {
    var isComplete,
        doc = Processes.findOne({_id: FlowRouter.getParam('id')});
    if (doc.mortgageRequest.equityReceipt && doc.mortgageRequest.equityReceipt.receiptPath && doc.mortgageRequest.equityReceipt.receiptFileName) {
        isComplete = true;
    }
    return isComplete;
}