import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Suppliers } from '../../../imports/collections/configuration/Suppliers';

Template.MortgageProcessItem.helpers({
    getSupplierName: function(doc) {
        var supplierId = doc.realty.supplier,
            supplier = Suppliers.findOne({
                _id: supplierId
            });
        if (supplier) {
            return supplier.displayName;
        }
    },
    isNotBerlin: function(doc) {
        if (doc.realty.town.toUpperCase() !== "Berlin".toUpperCase()) {
            return true;
        }
    },
    getProcessStep: Constants.getProcessStep,
    passedDeadline: function() {
        var passedDeadline,
            stepDuration,
            deadline;
        if (this.log && Constants.steps[this.step]) {
            stepDuration = Constants.steps[this.step].deadline;
            if (stepDuration) {
                deadline = moment(this.log[this.log.length-1].date).add(stepDuration,'hours');
                // console.log(deadline.format('D MMM YYYY, HH:mm'));
                if (this.log && deadline.isBefore(moment.now())) {
                    passedDeadline = true;
                } else {
                    passedDeadline = false;
                }
            } else {
                passedDeadline = false;
            }
        } else {
            passedDeadline = false;
        }
        return passedDeadline;
    },
    getProgress: function() {
        return getProgress(this.log)
    },
    isOutputStep: function() {
        return Constants.steps[this.step].isOutput;
    },
    approvedToView: function(step) {
        if (Roles.userIsInRole(Meteor.userId(), ['admin']) || Constants.steps[step].isNormalUser) {
            return true;
        }
    }
});

Template.MortgageProcessItem.events({
    'click .js-advance': function(event) {
        AdvanceProcess(this, event);
    }
});