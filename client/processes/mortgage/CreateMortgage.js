Template.CreateMortgage.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
    this.isUncomplete=new ReactiveVar(false);
};

Template.CreateMortgage.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    isUncomplete: function(){
        return Template.instance().isUncomplete.get();
    }
});

Template.CreateMortgage.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    }
});

AutoForm.addHooks(['createMortgageForm'], {
    onSuccess: function() {
        var id = FlowRouter.getParam('id'),
            doc = Processes.findOne({_id: id});
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 7, Meteor.userId());
        FlowRouter.go('process-list');
    },
    onError: function(formType, error) {
        console.log('validation error = ' + error);
        this.template.parent().isUncomplete.set(true);
    }
});