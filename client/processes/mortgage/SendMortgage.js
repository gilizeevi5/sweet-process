import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.SendMortgage.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
    this.isUncomplete=new ReactiveVar(false);
};

Template.SendMortgage.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    isUncomplete: function(){
        return Template.instance().isUncomplete.get();
    }
});

Template.SendMortgage.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    }
});

AutoForm.addHooks(['sendMortgageForm'], {
    onSuccess: function() {
        var id = FlowRouter.getParam('id'),
            doc = Processes.findOne({_id: id});
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 8, Meteor.userId());
        FlowRouter.go('process-list');
    }
});