import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../../imports/collections/process/Processes';

Template.InputMortgage.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
    });
    this.isUncomplete = new ReactiveVar(false);
};

Template.InputMortgage.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    isEmployee: function(professionType) {
        return isEmployee(professionType);
    },
    isUncomplete: function(){
        return Template.instance().isUncomplete.get();
    },
});

Template.InputMortgage.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    },
    'click .js-sameEmployer':function(event, template) {
        if ($(event.currentTarget).is(':checked')) {
            if (event.currentTarget.name.includes('primaryClient')) {
                if (event.currentTarget.name.includes('payslip')) {
                    if (event.currentTarget.name.includes('payslip2')) {
                        $("input[name='primaryClient.payslip2.employerName']").val($("input[name='primaryClient.payslip1.employerName']").val());
                        $("input[name='primaryClient.payslip2.employerAddress']").val($("input[name='primaryClient.payslip1.employerAddress']").val());
                        $("input[name='primaryClient.payslip2.employerTaxNumber']").val($("input[name='primaryClient.payslip1.employerTaxNumber']").val());
                        $("input[name='primaryClient.payslip2.employedSince']").val($("input[name='primaryClient.payslip1.employedSince']").val());
                    } else {
                        $("input[name='primaryClient.payslip3.employerName']").val($("input[name='primaryClient.payslip2.employerName']").val());
                        $("input[name='primaryClient.payslip3.employerAddress']").val($("input[name='primaryClient.payslip2.employerAddress']").val());
                        $("input[name='primaryClient.payslip3.employerTaxNumber']").val($("input[name='primaryClient.payslip2.employerTaxNumber']").val());
                        $("input[name='primaryClient.payslip3.employedSince']").val($("input[name='primaryClient.payslip2.employedSince']").val());
                    }
                } else {
                    $("input[name='primaryClient.form106Year2.employerName']").val($("input[name='primaryClient.form106Year1.employerName']").val());
                    $("input[name='primaryClient.form106Year2.employerAddress']").val($("input[name='primaryClient.form106Year1.employerAddress']").val());
                    $("input[name='primaryClient.form106Year2.employerTaxNumber']").val($("input[name='primaryClient.form106Year1.employerTaxNumber']").val());
                }
            } else if (event.currentTarget.name.includes('client2')) {
                if (event.currentTarget.name.includes('payslip')) {
                    if (event.currentTarget.name.includes('payslip2')) {
                        $("input[name='client2.payslip2.employerName']").val($("input[name='client2.payslip1.employerName']").val());
                        $("input[name='client2.payslip2.employerAddress']").val($("input[name='client2.payslip1.employerAddress']").val());
                        $("input[name='client2.payslip2.employerTaxNumber']").val($("input[name='client2.payslip1.employerTaxNumber']").val());
                        $("input[name='client2.payslip2.employedSince']").val($("input[name='client2.payslip1.employedSince']").val());
                    } else {
                        $("input[name='client2.payslip3.employerName']").val($("input[name='client2.payslip2.employerName']").val());
                        $("input[name='client2.payslip3.employerAddress']").val($("input[name='client2.payslip2.employerAddress']").val());
                        $("input[name='client2.payslip3.employerTaxNumber']").val($("input[name='client2.payslip2.employerTaxNumber']").val());
                        $("input[name='client2.payslip3.employedSince']").val($("input[name='client2.payslip2.employedSince']").val());
                    }
                } else {
                    $("input[name='client2.form106Year2.employerName']").val($("input[name='client2.form106Year1.employerName']").val());
                    $("input[name='client2.form106Year2.employerAddress']").val($("input[name='client2.form106Year1.employerAddress']").val());
                    $("input[name='client2.form106Year2.employerTaxNumber']").val($("input[name='client2.form106Year1.employerTaxNumber']").val());
                }
            } else if (event.currentTarget.name.includes('client3')) {
                if (event.currentTarget.name.includes('payslip')) {
                    if (event.currentTarget.name.includes('payslip2')) {
                        $("input[name='client3.payslip2.employerName']").val($("input[name='client3.payslip1.employerName']").val());
                        $("input[name='client3.payslip2.employerAddress']").val($("input[name='client3.payslip1.employerAddress']").val());
                        $("input[name='client3.payslip2.employerTaxNumber']").val($("input[name='client3.payslip1.employerTaxNumber']").val());
                        $("input[name='client3.payslip2.employedSince']").val($("input[name='client3.payslip1.employedSince']").val());
                    } else {
                        $("input[name='client3.payslip3.employerName']").val($("input[name='client3.payslip2.employerName']").val());
                        $("input[name='client3.payslip3.employerAddress']").val($("input[name='client3.payslip2.employerAddress']").val());
                        $("input[name='client3.payslip3.employerTaxNumber']").val($("input[name='client3.payslip2.employerTaxNumber']").val());
                        $("input[name='client3.payslip3.employedSince']").val($("input[name='client3.payslip2.employedSince']").val());
                    }
                } else {
                    $("input[name='client3.form106Year2.employerName']").val($("input[name='client3.form106Year1.employerName']").val());
                    $("input[name='client3.form106Year2.employerAddress']").val($("input[name='client3.form106Year1.employerAddress']").val());
                    $("input[name='client3.form106Year2.employerTaxNumber']").val($("input[name='client3.form106Year1.employerTaxNumber']").val());
                }
            }
        }
    }
});

AutoForm.addHooks(['inputMortgageForm'], {
    onSuccess: function() {
        Meteor.call('advanceProcess', FlowRouter.getParam('id'), 6, Meteor.userId());
        FlowRouter.go('process-list');
    },
    onError: function(formType, error) {
        console.log('validation error = ' + error);
        this.template.parent().isUncomplete.set(true);
    }
});

isEmployee = function(professionType) {
    var response;
    if (professionType === 'employed') {
        reponse = true
    } else {
        reponse = false;
    }
    return reponse;
}