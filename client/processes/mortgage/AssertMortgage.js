Template.AssertMortgage.created=function(){
    var self = this,
        id = FlowRouter.getParam('id');
    Tracker.autorun(function() {
        self.subscribe('singleProcess', id);
        self.subscribe('mortgageBrokers');
    });
    this.isUncomplete=new ReactiveVar(false);
};

Template.AssertMortgage.helpers({
    process: function() {
        var id = FlowRouter.getParam('id');
        return Processes.findOne({_id: id});
    },
    getProcessStep: Constants.getProcessStep,
    isUncomplete: function(){
        return Template.instance().isUncomplete.get();
    }
});

Template.AssertMortgage.events({
    'click .js-cancel':function() {
        FlowRouter.go('process-list');
    },
    'click .js-submit':function(event,template) {
        template.isUncomplete.set(false);
        if (checkIfComplete()) {
            $('#assertMortgageForm').submit();
        } else {
            template.isUncomplete.set(true);
        }
    }
});

AutoForm.addHooks(['assertMortgageForm'], {
    onSuccess: function() {
        var id = FlowRouter.getParam('id'),
            doc = Processes.findOne({_id: id});
        if (doc.mortgageRequirement.requiresMortgage) {
            Processes.update(id, {
                $set: {
                    'stepB': (Constants.phaseCap[0] + 1),
                    'phaseB': 1
                }
            });
            Meteor.call('advanceProcess', FlowRouter.getParam('id'), 5, Meteor.userId());
        }
        // Advance process to notary
        Processes.update(id, {
            $set: {
                'step': (Constants.phaseCap[1]),
                'phase': 2
            }
        });
        FlowRouter.go('process-list');
    }
});

function checkIfComplete() {
    var isComplete,
        requiresMortgage = $("input[name='mortgageRequirement.requiresMortgage']")
        includeClient1 = $("input[name='mortgageRequirement.includeClient1']"),
        includeClient2 = $("input[name='mortgageRequirement.includeClient2']"),
        includeClient3 = $("input[name='mortgageRequirement.includeClient3']");
    if (requiresMortgage.is(':checked')) {
        if (includeClient1.is(':checked') || includeClient2.is(':checked') || includeClient3.is(':checked')) {
            isComplete = true;
        } else {
            isComplete = false;
        }
    } else {
        isComplete = true;
    }
    return isComplete;
};