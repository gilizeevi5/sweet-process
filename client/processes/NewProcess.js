import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import { Processes } from '../../imports/collections/process/Processes';
import { ProcessSchema } from '../../imports/collections/process/Processes';

Template.NewProcess.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processes');
        self.subscribe('configurations');
        self.subscribe('suppliers');
        self.subscribe('mortgageBrokers');
        self.subscribe('notaries');
        self.subscribe('accountLawyers');
    });
};

Template.NewProcess.helpers({
    getProcesses() {
        return Processes;
    },
    getSchema() {
        return ProcessSchema;
    },
});

Template.NewProcess.events({

});

SimpleSchema.debug = true;

AutoForm.addHooks('insertProcessForm', {
    onSuccess: function() {
        FlowRouter.go('process-list');
    }
});