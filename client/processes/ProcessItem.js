import { Template } from 'meteor/templating';
import { Suppliers } from '../../imports/collections/configuration/Suppliers';

Template.ProcessItem.helpers({
    getProcessPhase: function(idx) {
        return Constants.phases[idx];
    },
    getProcessStep: Constants.getProcessStep,
    getSupplierName: function(doc) {
        var supplierId = doc.realty.supplier,
            supplier = Suppliers.findOne({
                _id: supplierId
            });
        if (supplier) {
            return supplier.displayName;
        }
    },
    isNotBerlin: function(doc) {
        if (doc.realty.town.toUpperCase() !== "Berlin".toUpperCase()) {
            return true;
        }
    },
    getProgress: function() {
        return getProgress(this.log);
    },
    isOutputStep: function() {
        if (Constants.steps[this.step]) {
            return Constants.steps[this.step].isOutput;
        }
    },
    approvedToView: function(step) {
        if (Roles.userIsInRole(Meteor.userId(), ['admin']) || Constants.steps[step].isNormalUser) {
            return true;
        }
    }
});

Template.ProcessItem.events({
    'click .js-advance': function(event) {
        AdvanceProcess(this, event);
    }
});

AdvanceProcess = function(process, event) {
    var originStep = parseInt($(event.currentTarget).attr('data').split('-')[1]),
        selectedTab = $('#ongoing-tabs li.active a').attr('aria-controls');
    console.log('advance ' + originStep + ' userId = ' + Meteor.userId());
    if (process.stepB && originStep > Constants.phaseCap[0] && originStep < Constants.phaseCap[1]) {
        if (Constants.steps[process.stepB].type === 0) {
            callAdvanceProcess(process._id, originStep, Meteor.userId());
        } else {
            Session.set('lastSelectedTab', selectedTab);
            FlowRouter.go(Constants.steps[process.stepB].target, {id: process._id});
        }
    } else {
        if (Constants.steps[process.step].type === 0) {
            callAdvanceProcess(process._id, originStep, Meteor.userId());
        } else {
            // Send supplier reservation without requiring extra input from user
            if (process.step === 4 && !process.includeClient2) {
                callAdvanceProcess(process._id, originStep, Meteor.userId());
            } else {
                Session.set('lastSelectedTab', selectedTab);
                FlowRouter.go(Constants.steps[process.step].target, {id: process._id});
            }
        }
    }
};

callAdvanceProcess = function(processId, originStep, userId) {
    Meteor.call('advanceProcess', processId, originStep, userId, function(error, result){
        if (error) {
            Alerts.add('ERROR: ' + error.reason, 'danger');
            console.error('ERROR, error = ' + JSON.stringify(error));
        } else {
            Alerts.defaultOptions.autoHide = true;
            Alerts.defaultOptions.fadeOut = 1200;
            Alerts.add('Process advanced successfully', 'success');
        }
    });
};

getProgress = function(log) {
    var progress;
    if (log) {
        progress = Math.round((log.length / Constants.steps.length) * 100);
    }
    return progress;
};