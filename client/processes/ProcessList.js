import _ from 'lodash';
import { Processes } from '../../imports/collections/process/Processes';;
import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';

Template.ProcessList.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('processes');
        self.subscribe('suppliers');
    });
};

Template.ProcessList.rendered = function(){
    if ($('#process-list-button').length == 0) {
        Meteor.setTimeout(function(){
            $('#bs-example-navbar-collapse-1 .active').removeClass('active');
            $('#ongoing-button').addClass('active');
        }, 500);
    } else {
        $('#bs-example-navbar-collapse-1 .active').removeClass('active');
        $('#ongoing-button').addClass('active');
    }
    if (Session.get('lastSelectedTab')) {
        $('#ongoing-tabs a[href="#' + Session.get('lastSelectedTab') +'"]').tab('show');
        delete Session.keys['lastSelectedTab']
    }
};

Template.ProcessList.helpers({
    getReservationProcesses: function() {
        return Processes.find({
            phase: 0
        }, {sort: {'step': 1, 'primaryClient.lastName': 1}});
    },
    getOverdueReservationsCount: function() {
        var overdueProcesses = Processes.find({
            phase: 0,
            isAOverdue: true
        }).fetch();
        return overdueProcesses.length;
    },
    getReservationsCount: function() {
        var processes = Processes.find({
            phase: 0
        }).fetch();
        return processes.length;
    },
    getMortgageProcesses: function() {
        var mortgages = Processes.find({
            phaseB: 1
        }, {sort: {'step': 1, 'primaryClient.lastName': 1}}).fetch();
        return _.filter(mortgages, function(process){
            return process.stepB < Constants.phaseCap[1];
        });
    },
    getOverdueMortgagesCount: function() {
        var overdueProcesses = Processes.find({
            phaseB: 1,
            isBOverdue: true
        }).fetch();
        return _.filter(overdueProcesses, function(process){
            return process.stepB < Constants.phaseCap[1];
        }).length;
    },
    getMortgageCount: function() {
        var processes = Processes.find({
            phaseB: 1
        }).fetch();
        return _.filter(processes, function(process){
            return process.stepB < Constants.phaseCap[1];
        }).length;
    },
    getNotaryProcesses: function() {
        return Processes.find({
            phase: 2
        }, {sort: {'step': 1, 'primaryClient.lastName': 1}});
    },
    getOverdueNotariesCount: function() {
        var overdueProcesses = Processes.find({
            phase: 2,
            isAOverdue: true
        }).fetch();
        return overdueProcesses.length;
    },
    getNotariesCount: function() {
        var processes = Processes.find({
            phase: 2
        }).fetch();
        return processes.length;
    },
    getHandoversProcesses: function() {
        return Processes.find({
            phase: 3
        }, {sort: {'step': 1, 'primaryClient.lastName': 1}});
    },
    getOverdueHandoversCount: function() {
        var overdueProcesses = Processes.find({
                phase: 3,
                isAOverdue: true
            }).fetch();
        return overdueProcesses.length;
    },
    getHandoversCount: function() {
        var processes = Processes.find({
            phase: 3
        }).fetch();
        return processes.length;
    }
});