import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';
import _ from 'lodash';

Template.Steps.created=function(){
    var self = this;
    Tracker.autorun(function() {
    });
};

Template.Steps.helpers({
    getReservationSteps: function() {
        var steps = [],
            index;
        steps = Constants.steps;
        steps = _.filter(steps, function(step) {
            index = _.indexOf(steps, step);
            if (index >= 0 && index <= Constants.phaseCap[0]) {
                return step;
            }
        });
        return steps;
    },
    getMortgageSteps: function() {
        var steps = [],
            index;
        steps = Constants.steps;
        steps = _.filter(steps, function(step) {
            index = _.indexOf(steps, step);
            if (index >= 0 && index > Constants.phaseCap[0] && index < Constants.phaseCap[1]) {
                return step;
            }
        });
        return steps;
    },
    getNotarySteps: function() {
        var steps = [],
            index;
        steps = Constants.steps;
        steps = _.filter(steps, function(step) {
            index = _.indexOf(steps, step);
            if (index >= 0 && index >= Constants.phaseCap[1] && index < Constants.phaseCap[2]) {
                return step;
            }
        });
        return steps;
    },
    getManagementSteps: function() {
        var steps = [],
            index;
        steps = Constants.steps;
        steps = _.filter(steps, function(step) {
            index = _.indexOf(steps, step);
            if (index >= 0 && index >= Constants.phaseCap[2]) {
                return step;
            }
        });
        return steps;
    }
});

Template.Steps.events({
});

Template.Steps.rendered = function(){
    if ($('#steps-button').length === 0) {
        Meteor.setTimeout(function(){
            $('.active').removeClass('active');
            $('#steps-button').addClass('active');
        }, 500);
    } else {
        $('.active').removeClass('active');
        $('#steps-button').addClass('active');
    }
};

Template.Step.helpers({
    getStepIndex: function(step) {
       return index = _.indexOf(Constants.steps, step);
   },
    isApprovalStep: function() {
       if (this.type === 0) {
           return true
       }
    }
});