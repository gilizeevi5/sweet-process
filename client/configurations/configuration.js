import { Tracker } from 'meteor/tracker';
import { Template } from 'meteor/templating';

Template.Configuration.created=function(){
    var self = this;
    Tracker.autorun(function() {
        self.subscribe('configurations');
        self.subscribe('suppliers');
        self.subscribe('mortgageBrokers');
        self.subscribe('notaries');
        self.subscribe('accountLawyers');
        self.subscribe('propertyManagementCompanies');
        self.subscribe('users');
    });
    this.isAddingSupplier=new ReactiveVar(false);
    this.isAddingNotary=new ReactiveVar(false);
    this.isAddingBroker=new ReactiveVar(false);
    this.isAddingLawyer=new ReactiveVar(false);
    this.isAddingUser=new ReactiveVar(false);
    this.isAddingPropertyManagement=new ReactiveVar(false);
};

Template.Configuration.helpers({
    getConfigurationDoc: function() {
        var doc =Configurations.findOne();
        return doc;
    },
    isAddingSupplier: function(){
        return Template.instance().isAddingSupplier.get();
    },
    isAddingNotary: function(){
        return Template.instance().isAddingNotary.get();
    },
    isAddingBroker: function(){
        return Template.instance().isAddingBroker.get();
    },
    isAddingLawyer: function(){
        return Template.instance().isAddingLawyer.get();
    },
    isAddingUser: function() {
        return Template.instance().isAddingUser.get();
    },
    isAddingPropertyManagement: function() {
        return Template.instance().isAddingPropertyManagement.get();
    },
    getSuppliers: function() {
        return Suppliers.find({});
    },
    getNotaries: function() {
        return Notaries.find({});
    },
    getBrokers: function() {
        return MortgageBrokers.find({});
    },
    getAccountLawyers: function() {
        return AccountLawyers.find({});
    },
    getUsers: function() {
        return Meteor.users.find({});
    },
    getPropertyManagementCompanies: function() {
        return PropertyManagementCompanies.find({});
    }
});

Template.Configuration.events({
    'click .js-add-new-supplier'(event,template){
        var isAddingSupplier=template.isAddingSupplier.get();
        template.isAddingSupplier.set(!isAddingSupplier);
    },
    'submit #insertSupplierForm'(event,template){
        var isAddingSupplier=template.isAddingSupplier.get();
        template.isAddingSupplier.set(!isAddingSupplier);
    },
    'click #insertSupplierForm .cancel'(event,template){
        var isAddingSupplier=template.isAddingSupplier.get();
        template.isAddingSupplier.set(!isAddingSupplier);
    },
    'click .js-add-new-notary'(event,template){
        console.log('add new notary');
        var isAddingNotary=template.isAddingNotary.get();
        template.isAddingNotary.set(!isAddingNotary);
    },
    'submit #insertNotaryForm'(event,template){
        var isAddingNotary=template.isAddingNotary.get();
        template.isAddingNotary.set(!isAddingNotary);
    },
    'click #insertNotaryForm .cancel'(event,template){
        var isAddingNotary=template.isAddingNotary.get();
        template.isAddingNotary.set(!isAddingNotary);
    },
    'click .js-add-new-broker'(event,template){
        var isAddingBroker=template.isAddingBroker.get();
        template.isAddingBroker.set(!isAddingBroker);
    },
    'submit #insertBrokerForm'(event,template){
        var isAddingBroker=template.isAddingBroker.get();
        template.isAddingBroker.set(!isAddingBroker);
    },
    'click #insertBrokerForm .cancel'(event,template){
        var isAddingBroker=template.isAddingBroker.get();
        template.isAddingBroker.set(!isAddingBroker);
    },
    'click .js-add-new-lawyer'(event,template){
        var isAddingLawyer=template.isAddingLawyer.get();
        template.isAddingLawyer.set(!isAddingLawyer);
    },
    'submit #insertLawyerForm'(event,template){
        var isAddingLawyer=template.isAddingLawyer.get();
        template.isAddingLawyer.set(!isAddingLawyer);
    },
    'click #insertLawyerForm .cancel'(event,template){
        var isAddingLawyer=template.isAddingLawyer.get();
        template.isAddingLawyer.set(!isAddingLawyer);
    },
    'click .js-add-new-propertyManagement'(event,template){
        var isAddingPropertyManagement=template.isAddingPropertyManagement.get();
        template.isAddingPropertyManagement.set(!isAddingPropertyManagement);
    },
    'submit #insertPropertyManagementForm'(event,template){
        var isAddingPropertyManagement=template.isAddingPropertyManagement.get();
        template.isAddingPropertyManagement.set(!isAddingPropertyManagement);
    },
    'click #insertPropertyManagementForm .cancel'(event,template){
        var isAddingPropertyManagement=template.isAddingPropertyManagement.get();
        template.isAddingPropertyManagement.set(!isAddingPropertyManagement);
    },
    'click .js-add-new-user'(event,template){
        var isAddingUser=template.isAddingUser.get();
        template.isAddingUser.set(!isAddingUser);
    },
    'click #newUserForm .cancel'(event,template){
        var isAddingUser=template.isAddingUser.get();
        template.isAddingUser.set(!isAddingUser);
    },
    'click .js-submit-new-user'(event,template){
        var email = $('#newUserForm #userEmail input').val(),
            password = $('#newUserForm #userPassword input').val();
        if (validateEmail(email)) {
            if (password.length > 5) {
                try {
                    Meteor.call('createNewUserAccount', email, password);
                    $('#newUserForm #userEmail input').val('');
                    $('#newUserForm #userPassword input').val('');
                    $('#newUserForm #userEmail .help-block').text('');
                    $('#newUserForm #userPassword .help-block').text('');
                    template.isAddingUser.set(false);
                } catch(error) {
                    alert('error = ' + error);
                }
            } else {
                console.log('Password must include at least 6 characters');
                $('#newUserForm #userPassword .help-block').text('Password must include at least 6 characters');
            }
        } else {
            console.log('Please provide a correct email');
            $('#newUserForm #userEmail .help-block').text('Please provide a correct email').addClass('has-danger');
        }
    },
    'click .js-reset-accounting'(event,template){
        var confiremd = confirm('Are you sure you want to delete all accounting data?');
        if (confiremd) {
            Meteor.call('clearAllAccountingRelatedData');
        }
    },
});

Template.Configuration.rendered = function(){
    if ($('#configuration-button').length === 0) {
        Meteor.setTimeout(function(){
            $('.active').removeClass('active');
            $('#configuration-button').addClass('active');
        }, 500);
    } else {
        $('.active').removeClass('active');
        $('#configuration-button').addClass('active');
    }
};


Template.Supplier.events({
    'click .js-delete-button'(){
        Suppliers.remove(this._id);
    },
    'click .js-edit-button'(event,template){
        var isEditing=template.isEditing.get();
        template.isEditing.set(!isEditing);
    },
    'click .js-cancel-button'(event,template){
        template.isEditing.set(false);
    },
    'click .js-update-button'(event,template){
        template.$("#updateSupplierForm").submit()   ;
    },
    'submit #updateSupplierForm'(event,template){
        template.isEditing.set(false);
    },
});
Template.Notary.events({
    'click .js-delete-button'(){
        Notaries.remove(this._id);
    },
    'click .js-edit-button'(event,template){
        var isEditing=template.isEditing.get();
        template.isEditing.set(!isEditing);
    },
    'click .js-cancel-button'(event,template){
        template.isEditing.set(false);
    },
    'click .js-update-button'(event,template){
        template.$("#updateNotaryForm").submit()   ;
    },
    'submit #updateNotaryForm'(event,template){
        template.isEditing.set(false);
    },
});
Template.Broker.events({
    'click .js-delete-button'(){
        MortgageBrokers.remove(this._id);
    },
    'click .js-edit-button'(event,template){
        var isEditing=template.isEditing.get();
        template.isEditing.set(!isEditing);
    },
    'click .js-cancel-button'(event,template){
        template.isEditing.set(false);
    },
    'click .js-update-button'(event,template){
        template.$("#updateBrokerForm").submit()   ;
    },
    'submit #updateBrokerForm'(event,template){
        template.isEditing.set(false);
    },
});
Template.Lawyer.events({
    'click .js-delete-button'(){
        AccountLawyers.remove(this._id);
    },
    'click .js-edit-button'(event,template){
        var isEditing=template.isEditing.get();
        template.isEditing.set(!isEditing);
    },
    'click .js-cancel-button'(event,template){
        template.isEditing.set(false);
    },
    'click .js-update-button'(event,template){
        template.$("#updateLawyerForm").submit()   ;
    },
    'submit #updateLawyerForm'(event,template){
        template.isEditing.set(false);
    },
});

Template.PropertyManagement.events({
    'click .js-delete-button'(){
        PropertyManagementCompanies.remove(this._id);
    },
    'click .js-edit-button'(event,template){
        var isEditing=template.isEditing.get();
        template.isEditing.set(!isEditing);
    },
    'click .js-cancel-button'(event,template){
        template.isEditing.set(false);
    },
    'click .js-update-button'(event,template){
        template.$("#updatePropertyManagementForm").submit()   ;
    },
    'submit #updatePropertyManagementForm'(event,template){
        template.isEditing.set(false);
    },
});

Template.User.helpers({
    getUserEmail: function(userId) {
        var email = Meteor.users.findOne({'_id': userId}).emails[0].address;
        return email;
    },
    getUserRole: function(userId) {
        var role = 'Regular user';
        if (Roles.userIsInRole(userId, ['admin'])) {
            role = 'Admin user';
        }
        return role;
    },
    isAdmin: function(userId){
        return Roles.userIsInRole(userId, ['admin']);
    }
});

Template.User.events({
    'click .js-demote-user-button'(event,template){
        Meteor.call('demoteUser', this._id);
    },
    'click .js-promote-user-button'(event,template){
        Meteor.call('promoteUser', this._id);
    },
    'click .js-delete-user-button'(event,template){
        if (window.confirm('Are you sure you want to delete the user?')) {
                Meteor.call('deleteUser', this._id);
        }
    }
});

Template.Supplier.created = function() {
    this.isEditing=new ReactiveVar(false);
};

Template.Supplier.helpers({
    isEditing: function(){
        return Template.instance().isEditing.get();
    }
});

Template.Notary.created = function() {
    this.isEditing=new ReactiveVar(false);
};

Template.Notary.helpers({
    isEditing: function(){
        return Template.instance().isEditing.get();
    }
});

Template.Broker.created = function() {
    this.isEditing=new ReactiveVar(false);
};

Template.Broker.helpers({
    isEditing: function(){
        return Template.instance().isEditing.get();
    }
});

Template.Lawyer.created = function() {
    this.isEditing=new ReactiveVar(false);
};

Template.Lawyer.helpers({
    isEditing: function(){
        return Template.instance().isEditing.get();
    }
});

Template.PropertyManagement.created = function() {
    this.isEditing=new ReactiveVar(false);
};

Template.PropertyManagement.helpers({
    isEditing: function(){
        return Template.instance().isEditing.get();
    }
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}