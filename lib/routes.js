Accounts.onLogin(function(event){
    if (event.type === 'password') {
        FlowRouter.go('process-list');
    }
});

Accounts.onLogout(function(){
    window.localStorage.removeItem('Meteor.loginToken');
    window.localStorage.removeItem('Meteor.loginTokenExpires');
    window.localStorage.removeItem('Meteor.userId');
    Accounts.connection.setUserId(null);
    FlowRouter.go('home');
});

FlowRouter.triggers.enter([function(context, redirect){
    Alerts.removeSeen();
    if (!Meteor.userId()) {
        FlowRouter.go('home');
    }
}]);

FlowRouter.route('/', {
   name: 'home',
   action() {
       if (Meteor.userId()) {
           FlowRouter.go('process-list');
       }
       BlazeLayout.render('HomeLayout');
   }
});

FlowRouter.route('/process-list', {
    name: 'process-list',
    action() {
        BlazeLayout.render('MainLayout', {main: 'ProcessList'});
    }
});

FlowRouter.route('/process/:id', {
    name: 'process',
    action() {
        BlazeLayout.render('MainLayout', {main: 'ProcessPage'});
    }
});

FlowRouter.route('/process/:id/verifyIdentity', {
    name: 'verifyIdentity',
    action() {
        BlazeLayout.render('MainLayout', {main: 'VerifyIdentity'});
    }
});

FlowRouter.route('/process/:id/supplierReservation', {
    name: 'supplierReservation',
    action() {
        BlazeLayout.render('MainLayout', {main: 'SupplierReservation'});
    }
});

FlowRouter.route('/process/:id/assertMortgage', {
    name: 'assertMortgage',
    action() {
        BlazeLayout.render('MainLayout', {main: 'AssertMortgage'});
    }
});

FlowRouter.route('/process/:id/inputMortgage', {
    name: 'inputMortgage',
    action() {
        BlazeLayout.render('MainLayout', {main: 'InputMortgage'});
    }
});

FlowRouter.route('/process/:id/createMortgage', {
    name: 'createMortgage',
    action() {
        BlazeLayout.render('MainLayout', {main: 'CreateMortgage'});
    }
});

FlowRouter.route('/process/:id/sendMortgage', {
    name: 'sendMortgage',
    action() {
        BlazeLayout.render('MainLayout', {main: 'SendMortgage'});
    }
});

FlowRouter.route('/process/:id/inputMortgageSum', {
    name: 'inputMortgageSum',
    action() {
        BlazeLayout.render('MainLayout', {main: 'InputMortgageSum'});
    }
});

FlowRouter.route('/process/:id/scheduleMortgageSignature', {
    name: 'scheduleMortgageSignature',
    action() {
        BlazeLayout.render('MainLayout', {main: 'ScheduleMortgageSignature'});
    }
});

FlowRouter.route('/process/:id/suggestNotaryDates', {
    name: 'suggestNotaryDates',
    action() {
        BlazeLayout.render('MainLayout', {main: 'SuggestNotaryDates'});
    }
});

FlowRouter.route('/process/:id/selectNotaryDate', {
    name: 'selectNotaryDate',
    action() {
        BlazeLayout.render('MainLayout', {main: 'SelectNotaryDate'});
    }
});

FlowRouter.route('/process/:id/inputConversionRate', {
    name: 'inputConversionRate',
    action() {
        BlazeLayout.render('MainLayout', {main: 'InputConversionRate'});
    }
});

FlowRouter.route('/process/:id/inputAccountDetails', {
    name: 'inputAccountDetails',
    action() {
        BlazeLayout.render('MainLayout', {main: 'InputAccountDetails'});
    }
});

FlowRouter.route('/process/:id/sendPaymentInstructions', {
    name: 'sendPaymentInstructions',
    action() {
        BlazeLayout.render('MainLayout', {main: 'SendPaymentInstructions'});
    }
});

FlowRouter.route('/process/:id/assertSeparateNotaryRequirement', {
    name: 'assertSeparateNotaryRequirement',
    action() {
        BlazeLayout.render('MainLayout', {main: 'AssertSeparateNotaryRequirement'});
    }
});

FlowRouter.route('/process/:id/suggestMortgageNotaryDates', {
    name: 'suggestMortgageNotaryDates',
    action() {
        BlazeLayout.render('MainLayout', {main: 'SuggestMortgageNotaryDates'});
    }
});

FlowRouter.route('/process/:id/selectMortgageNotaryDate', {
    name: 'selectMortgageNotaryDate',
    action() {
        BlazeLayout.render('MainLayout', {main: 'SelectMortgageNotaryDate'});
    }
});

FlowRouter.route('/process/:id/uploadMortgage', {
    name: 'uploadMortgage',
    action() {
        BlazeLayout.render('MainLayout', {main: 'UploadMortgage'});
    }
});

FlowRouter.route('/process/:id/uploadEquityReceipt', {
    name: 'uploadEquityReceipt',
    action() {
        BlazeLayout.render('MainLayout', {main: 'UploadEquityReceipt'});
    }
});

FlowRouter.route('/process/:id/sendMgmtContractAndPOA', {
    name: 'sendMgmtContractAndPOA',
    action() {
        BlazeLayout.render('MainLayout', {main: 'SendMgmtContractAndPOA'});
    }
});

FlowRouter.route('/process/:id/uploadMgmtContractAndPOA', {
    name: 'uploadMgmtContractAndPOA',
    action() {
        BlazeLayout.render('MainLayout', {main: 'UploadMgmtContractAndPOA'});
    }
});

FlowRouter.route('/process/:id/inputTenantData', {
    name: 'inputTenantData',
    action() {
        BlazeLayout.render('MainLayout', {main: 'InputTenantData'});
    }
});

FlowRouter.route('/process/:id/introToManagementCompany', {
    name: 'introToManagementCompany',
    action() {
        BlazeLayout.render('MainLayout', {main: 'IntroToManagementCompany'});
    }
});

FlowRouter.route('/process/:id/inputManagementCompanyData', {
    name: 'inputManagementCompanyData',
    action() {
        BlazeLayout.render('MainLayout', {main: 'InputManagementCompanyData'});
    }
});

FlowRouter.route('/process/:id/firstRentalPayment', {
    name: 'firstRentalPayment',
    action() {
        BlazeLayout.render('MainLayout', {main: 'FirstRentalPayment'});
    }
});

FlowRouter.route('/process/:id/notifyOnRentalsToBePayed', {
    name: 'notifyOnRentalsToBePayed',
    action() {
        BlazeLayout.render('MainLayout', {main: 'NotifyOnRentalsToBePayed'});
    }
});

FlowRouter.route('/process/:id/informFinanzamt', {
    name: 'informFinanzamt',
    action() {
        BlazeLayout.render('MainLayout', {main: 'InformFinanzamt'});
    }
});

FlowRouter.route('/process/:id/confirmFinanzamt', {
    name: 'confirmFinanzamt',
    action() {
        BlazeLayout.render('MainLayout', {main: 'ConfirmFinanzamt'});
    }
});

FlowRouter.route('/new-process', {
    name: 'new-process',
    action() {
        BlazeLayout.render('MainLayout', {main: 'NewProcess'});
    }
});

FlowRouter.route('/configuration', {
    name: 'configuration',
    action() {
        BlazeLayout.render('MainLayout', {main: 'Configuration'});
    }
});


FlowRouter.route('/management', {
    name: 'management',
    action() {
        BlazeLayout.render('MainLayout', {main: 'Management'});
    }
});

FlowRouter.route('/process/:id/managementActions/', {
    name: 'managementActions',
    action() {
        BlazeLayout.render('MainLayout', {main: 'ManagementActions'});
    }
});

FlowRouter.route('/process/:id/managementActions/firstReminder', {
    name: 'managementActions-firstReminder',
    action() {
        BlazeLayout.render('MainLayout', {main: 'FirstReminderAction'});
    }
});

FlowRouter.route('/process/:id/managementActions/secondReminder', {
    name: 'managementActions-secondReminder',
    action() {
        BlazeLayout.render('MainLayout', {main: 'SecondReminderAction'});
    }
});

FlowRouter.route('/process/:id/managementActions/rentIncrease', {
    name: 'managementActions-rentIncrease',
    action() {
        BlazeLayout.render('MainLayout', {main: 'RentIncreaseAction'});
    }
});

FlowRouter.route('/reports', {
    name: 'reports',
    action() {
        BlazeLayout.render('MainLayout', {main: 'Reports'});
    }
});

FlowRouter.route('/reports/process-status-report', {
    name: 'process-status-report',
    action() {
        BlazeLayout.render('MainLayout', {main: 'ProcessStatusReport'});
    }
});

FlowRouter.route('/reports/step-status-report', {
    name: 'step-status-report',
    action() {
        BlazeLayout.render('MainLayout', {main: 'StepStatusReport'});
    }
});

FlowRouter.route('/reports/figures-upload-report', {
    name: 'figures-upload-report',
    action() {
        BlazeLayout.render('MainLayout', {main: 'FiguresUploadReport'});
    }
});

FlowRouter.route('/reports/missing-management-info-report', {
    name: 'missing-management-info-report',
    action() {
        BlazeLayout.render('MainLayout', {main: 'MissingManagementInfoReport'});
    }
});

FlowRouter.route('/reports/missing-tax-info-report', {
    name: 'missing-tax-info-report',
    action() {
        BlazeLayout.render('MainLayout', {main: 'MissingTaxInfoReport'});
    }
});

FlowRouter.route('/reports/furnished-rentals', {
    name: 'furnished-rentals',
    action() {
        BlazeLayout.render('MainLayout', {main: 'FurnishedRentals'});
    }
});

FlowRouter.route('/reports/ignoredAccountingIssues', {
    name: 'ignoredAccountingIssues',
    action() {
        BlazeLayout.render('MainLayout', {main: 'IgnoredAccountingIssuesReport'});
    }
});

FlowRouter.route('/reports/building', {
    name: 'building-report',
    action() {
        BlazeLayout.render('MainLayout', {main: 'BuildingsReport'});
    }
});

FlowRouter.route('/reports/archivedProcesses', {
    name: 'archived-processes',
    action() {
        BlazeLayout.render('MainLayout', {main: 'ArchivedProcesses'});
    }
});

FlowRouter.route('/reports/clientEmails', {
    name: 'client-emails',
    action() {
        BlazeLayout.render('MainLayout', {main: 'ClientEmails'});
    }
});

FlowRouter.route('/steps', {
    name: 'steps',
    action() {
        BlazeLayout.render('MainLayout', {main: 'Steps'});
    }
});

FlowRouter.route('/accounting', {
    name: 'accounting',
    action() {
        BlazeLayout.render('MainLayout', {main: 'Accounting'});
    }
});