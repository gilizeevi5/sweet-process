Constants = {
    steps: [
        /* - 01 - */ {label: 'Send reservation to client', type: 0, isOutput: true, isNormalUser: true, deadline: 1},
        /* - 01 - */ {label: 'Confirm client signed reservation', type: 0, isNormalUser: false, isOutput: true, deadline: 1 },
        /* - 02 - */ {label: 'Verify client identity', type: 1, target: 'verifyIdentity', isNormalUser: true, deadline: 1 },
        /* - 03 - */ {label: 'Confirm deposit payed', type: 0, isNormalUser: false, deadline: 7 },
        /* - 04 - */ {label: 'Send supplier reservation', type: 1, target: 'supplierReservation', isOutput: true, isNormalUser: false, deadline: 1 },
        /* - 05 - */ {label: 'Ask for mortgage information', type: 1, target: 'assertMortgage', isOutput: true, isNormalUser: false, deadline: 1 },
        /* - 06 - */ {label: 'Input mortgage details', type: 1, target: 'inputMortgage', isOutput: true, isNormalUser: true, deadline: 5 },
        /* - 07 - */ {label: 'Create mortgage request', type: 1, target: 'createMortgage', isNormalUser: true, deadline: 1 },
        /* - 08 - */ {label: 'Send mortgage request to broker', type: 1, target: 'sendMortgage', isOutput: true, isNormalUser: false, deadline: 1 },
        /* - 09 - */ {label: 'Update mortgage approved sum', type: 1, target: 'inputMortgageSum', isOutput: true, isNormalUser: false, deadline: 7 },
        /* - 10 - */ {label: 'Schedule mortgage signature', type: 1, target: 'scheduleMortgageSignature', isOutput: true, isNormalUser: false, deadline: 7 },
        /* - 11 - */ {label: 'Confirm mortgage signed and uploaded', type: 1, target: 'uploadMortgage', isNormalUser: true, isNotDone: false, isOutput: true, deadline: 7 },
        /* - 12 - */ {label: 'Send mortgage fee invoice', type: 0, isOutput: true, isNormalUser: false, deadline: 1 },
        /* - 13 - */ {label: 'Confirm mortgage returned to broker', type: 0, isNormalUser: false, deadline: 7 },
        /* - 14 - */ {label: 'Confirm mortgage fee payment', type: 0, isNormalUser: false, deadline: 7 },
        /* - 15 - */ {label: 'Requires separate notary meeting', type: 1, target: 'assertSeparateNotaryRequirement', isNotDone: false, isNormalUser: false, deadline: 1 },
        /* - 16 - */ {label: 'Suggest mortgage notary dates', type:1, target: 'suggestMortgageNotaryDates', isNotDone: false, isNormalUser: true, isOutput: true, deadline: 5 },
        /* - 17 - */ {label: 'Schedule mortgage notary', type: 1, target: 'selectMortgageNotaryDate', isNotDone: false, isNormalUser: true, isOutput: true, deadline: 3 },
        /* - 18 - */ {label: 'Confirm mortgage notarized', type: 0, isNotDone: false, isNormalUser: true, deadline: 14 },
        /* - 19 - */ {label: 'Instruct broker to pay mortgage', type: 1, target: 'uploadEquityReceipt', isOutput: true, isNotDone: false, isNormalUser: false, deadline: 14 },
        /* - 20 - */ {label: 'Confirm mortgage payed', type: 0, isNormalUser: false, deadline: 7 },
        /* - 21 - */ {label: 'Confirm receipt of contract', type: 0, isNormalUser: false, deadline: 10 },
        /* - 22 - */ {label: 'Confirm receipt of POA', type: 0, isOutput: true, isNormalUser: false, deadline: 10 },
        /* - 23 - */ {label: 'Confirm POA signed', type: 0, isNormalUser: false, deadline: 14 },
        /* - 24 - */ {label: 'Suggest notarization dates', type: 1, target: 'suggestNotaryDates', isOutput: true, isNormalUser: false, deadline: 3 },
        /* - 25 - */ {label: 'Schedule notarization', type: 1, isOutput: true, target: 'selectNotaryDate', isNormalUser: false, deadline: 3 },
        /* - 26 - */ {label: 'Confirm contract notarized', type: 0, isNormalUser: false, deadline: 14 },
        /* - 27 - */ {label: 'Send broker fee invoice', type: 1, target: 'inputConversionRate', isOutput: true, isNormalUser: false, deadline: 2 },
        /* - 28 - */ {label: 'Send escrow documents to client', type: 0, isOutput: true, isNotDone: false, isNormalUser: false, deadline: 2 },
        /* - 29 - */ {label: 'Confirm receipt of escrow documents', type: 0, isNormalUser: false, deadline: 5 },
        /* - 30 - */ {label: 'Send signed contracts to lawyer', type: 0, isNormalUser: false, deadline: 2 },
        /* - 31 - */ {label: 'Receive escrow account details', type: 1, target: 'inputAccountDetails', isNotDone: false, isNormalUser: false, deadline: 4 },
        /* - 32 - */ {label: 'Confirm broker fee payment', type: 0, isNormalUser: false, deadline: 7 },
        /* - 33 - */ {label: 'Confirm escrow sum deposit', type: 0, isNormalUser: false, deadline: 7 },
        /* - 34 - */ {label: 'Confirm receipt of maturity letter', type: 0, isNormalUser: false, deadline: 30 },
        /* - 35 - */ {label: 'Send client instructions for payment', type: 1, target: 'sendPaymentInstructions', isOutput: true, isNotDone: false, isNormalUser: false, deadline: 3 },
        /* - 36 - */ {label: 'Request supplier to confirm equity payment', type: 0, isOutput: true, isNotDone: false, isNormalUser: false, deadline: 7 },
        /* - 37 - */ {label: 'Confirm equity payed', type: 0, isOutput: false, isNotDone: false, isNormalUser: false, deadline: 3 },
        /* - 38 - */ {label: 'Confirm supplier internal invoice sent', type: 0, isNormalUser: false, isNotDone: false, isNormalUser: false, deadline: 3 },
        /* - 39 - */ {label: 'Confirm supplier internal invoice payed', type: 0, isNormalUser: false, isNotDone: false, isNormalUser: false, deadline: 7 },
        /* - 40 - */ {label: 'Send management contract and POA for signature', type: 1, target: 'sendMgmtContractAndPOA', isNotDone: false, isOutput: true, isNormalUser: true, deadline: 3 },
        /* - 41 - */ {label: 'Confirm and upload management contract and POA', type: 1, target: 'uploadMgmtContractAndPOA', isNotDone: false, isOutput: true, isNormalUser: true, deadline: 10 },
        /* - 42 - */ {label: 'Input tenant data', type: 1, target: 'inputTenantData', isNotDone: false, isOutput: false, isNormalUser: true, deadline: 5 },
        /* - 43 - */ {label: 'Notify tenant on ownership change', type: 0, isNotDone: false, isOutput: true, isNormalUser: true, deadline: 3 },
        /* - 44 - */ {label: 'Confirm sending ownership change letter to tenant', type: 0, isNotDone: false, isOutput: false, isNormalUser: true, deadline: 2 },
        /* - 45 - */ {label: 'Send introduction to management company', type: 1, target: 'introToManagementCompany', isNotDone: false, isOutput: true, isNormalUser: true, deadline: 2 },
        /* - 46 - */ {label: 'Input management company data', type: 1, target: 'inputManagementCompanyData', isNotDone: false, isOutput: false, isNormalUser: true, deadline: 2 },
        /* - 47 - */ {label: 'Confirm receipt of first rental from client', type: 1, target: 'firstRentalPayment', isNotDone: false, isOutput: false, isNormalUser: true, deadline: 30 },
        /* - 48 - */ {label: 'Notify client on rentals issued to be payed on monthly basis', type: 1, target: 'notifyOnRentalsToBePayed', isNotDone: false, isOutput: true, isNormalUser: true, deadline: 7},
        /* - 49 - */ {label: 'Inform Finanzamt', type: 1, target: 'informFinanzamt', isOutput: true, isNormalUser: true, isNotDone: true, deadline: 3},
        /* - 50 - */ {label: 'Confirm Finanzamt', type: 1, target: 'confirmFinanzamt', isOutput: false, isNormalUser: true, isNotDone: true, deadline: 3},
        /* - 51 - */ {label: 'Process Complete', type: 0, isNotDone: false},
        /* - 52 - */ {label: 'Mortgage Complete', type: 0, isNotDone: false}
    ],
    phases: [
        'Reservation',
        'Mortgage',
        'Notary',
        'Handover'
    ],
    phaseCap: [
        5,21,38,50
    ],
    getProcessStep: function(idx) {
        var stepLabel;
        if (Constants.steps[idx]) {
            stepLabel = Constants.steps[idx].label;
        }
        return stepLabel;
    },
    emails: {
        1: {
            subject: 'המשך תהליך רכישת נכס ב - ',
        },
        2: {
            subject: 'מסמכים דרושים לבקשת משכנתא עבור נכס ב - '
        },
        3: {
            subject: 'Confirmation on mortgage sum - '
        },
        4: {
            subject: ' אישור על מועד חתימת חוזה המשכנתא לנכס ב - '
        },
        5: {
            subject: 'בקשת תשלום עמלת מימון, נכס ב -'
        },
        6: {
            subject: 'אישור על יפוי כוח, נכס ב -'
        },
        7: {
            subject: 'Possible dates for the notary of - '
        },
        8: {
            subject: 'Confirmed date for the notary of - '
        },
        9: {
            subject: 'בקשת תשלום עמלה , נכס ב -'
        },
        10: {
            subject: 'הודעה מהנוטריון על תשלום הון עצמי -'
        },
        11: {
            subject: 'Required confirmation of equity payed for - '
        },
        12: {
            subject: 'Possible dates for the finance contract notary of - '
        },
        13: {
            subject: 'Mortgage deed for - '
        },
        14: {
            subject: 'Maturity letter received for - '
        },
        15: {
            subject: 'SEV - '
        },
        16: {
            subject: 'Grundsteuer - '
        }
    },
    senders: {
        clients: {
            email: 'rina@sweet-home.co.il',
            name: 'Rina Zeevi - Sweethome'
        },
        suppliers: {
            email: 'israel@sweet-home.co.il',
            name: 'Israel Zeevi - Sweethome'
        },
        management: {
            email: 'nurlan@sweet-home.co.il',
            name: 'Nurlan Bekmagambetov Verwaltung - Sweethome'
        },
        system: {
            email: 'rina@sweet-home.co.il',
            name: 'SweetProcess System'
        },
        customerSupport: {
            email: 'annet@sweet-home.co.il',
            name: 'Annet Weiß '
        }
    },
    defaultAccountingYear: 2018,
    defaultAccountingYearIndex: 0
};