/**
 * Created by gilizeevi on 19/04/17.
 */
Utils = {
    formatCurrencies: function(currencyNumber) {
        currencyNumber = lodash.ceil(currencyNumber, [precision=2]);
        return currencyNumber.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    roundWithDecimals: function(number, decimals) {
        var num = number,
            factor = Math.pow(10, decimals);
        if (typeof(number) === "string") {
            num = number.replace(/\./g, '');
            num = num.replace(/,/g, '.');
            num = parseFloat(num);
        }
        num = Math.round(num * factor) / factor;
        return num;
    }
};